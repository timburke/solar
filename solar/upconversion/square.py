#square.py
#An ideal square-response upconverter with controllable absorption peak location and size

import math
import base
import numpy as np

class SquareUpconverter (base.MobileUpconverter):
	r"""
	An upconverter with a square absorption function.
	
	The absorption is defined by:
	
	.. math ::
		:nowrap:
		
		A(\lambda) = \begin{cases}
		A_o & \text{for $| \lambda - \lambda_o | < \frac{w}{2}$} \\
		0 & \text{otherwise}
		\end{cases}
    
    The parameters are defined in the constructor for the upconverter as explained below.
	"""
	
	def __init__(self, peak, max, width, shift):
		base.UpconverterBase.__init__(self)
		
		self.peak = peak
		self.max = max
		self.width = width
		self.shift = peak-shift
	
	#return the absorption response of this upconverter at wavelength x
	def __call__(self, x):
		if math.fabs(x-self.peak) < self.width/2.0:
			return self.max
		
		return 0.0
	
	#Speedup function for processing an entire array with this response function
	#def process_array(self,arr):
	#	c = self.width / 2.35482
	#	invDen = 1.0/(2*c**2.0)
	#			
	#	return self.max * np.exp(-1.0 * np.square(arr-self.peak) * invDen)
