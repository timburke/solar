#base.py
#Base type for all upconverters defining useful common methods
from .. import reference,utilities
from ..sim import simplesim
from ..types import Spectrum,QuantumEfficiency
import numpy as np
import scipy
import scipy.fftpack

#Defines convenience functions which rely on upconverters supplying certain properties and functions
#In particular, subclasses must define:
# - __call__(self, wavelength) which defines the absorption of the upconverter at the given wavelength 
# - self.shift a wavelength defining the absolute antistokes shift.  Upconverted light is assumed to come out 
# 	as a sharp gaussian at this wavelength
class UpconverterBase:
	"""
	Base class for all upconverters.  Defines commmon interfaces for passing in light spectrums and
	obtaining upconverted emission.
	"""
	
	#define these parameters so that sphinx can extract documentation for them
	iqe = None
	emissionHalfwidth = None
	
	def __init__(self):
	
		#: Internal quantum efficiency of upconverter in range [0, 1]
		#: This is defined with 1 meaning 1 upconverted photon for every 2 absorbed photons, consistent
		#: with standard practices in the literature.
		self.iqe = 1 
		
		#: The full width at half maximum (FWHM) of the upconverted light in nm, which is a assumed to be emitted
		#: as a narrow gaussian with this width
		self.emissionHalfwidth = 10
	
	def ideal_emission(self, eqe):
		r"""
		Calculate, for a given EQE, where the optimal spectral location for upconverter emission is.
		Naively, this would be just the spectral location with maximal quantum efficiency, however,
		that doesn't take into account the fact the the emission is not at a single wavelength.  So,
		this function finds the maximum of the correlation function between the emission gaussian
		of the upconverter and the EQE.  
		
		:param eqe: The EQE that should be used to find the ideal anti-stokes shift
		:type eqe: solar.types.QuantumEfficiency
		
		:returns: The wavelength at which upconverted emission should ideally be centered
		:rtype: float
		"""
		oldShift = self.shift
		
		self.shift = 2048.0
		emission = self.emission_gaussian()
		self.shift = oldShift
		
		emissionU = utilities.sample_uniformly(emission, 0., 4096., 4096)
		eqeU = utilities.sample_uniformly(eqe, 0., 4096., 4096)
		
		combinedResponse = utilities.correlate(eqeU, emissionU)
		
		i = utilities.max_index(combinedResponse)
		
		return combinedResponse[i,0]
		
	#convenience vectorized function using absorption function contained in self.__call__()
	def process_array(self,arr):		
		res = np.vectorize(lambda x: self(x))(arr)
		
		return res
		
	#The total number of photons absorbed
	def total_absorption(self, light, **kw):
		r"""
		Calculate the total absorption of this upconverter.  If keyword flux is set, assume
		that light is already specified in a flux
		"""
		
		if hasattr(self, 'process_array'):
			arr = light.copy()
			arr[:,1] = light[:,1]*self.process_array(light[:,0])
		else:
			arr = light.response(self)
		
		#for i in xrange(0, arr.shape[0]):
		#	arr[i,1] = reference.powerToFlux(arr[i,1], arr[i,0])
		
		if "flux" not in kw:
			reference.vToFluxInPlace(arr)
		
		#np.apply_along_axis(reference.toFlux, 1, arr)
		
		return arr.totalpower()
		
	def emission_gaussian(self):
		sigma = self.emissionHalfwidth / 2.35482
		length = 30
		
		emission = np.zeros([length+2,2])
		emission[:,0] = np.linspace(self.shift-2.*sigma, self.shift+2.*sigma, length+2)
		emission[:,1] = reference.unit_gaussian(emission[:,0], self.shift, sigma)
		emission[0,1] = 0.0
		emission[-1,1] = 0.0
		
		#Make sure we have unit total area in this gaussian (we should be close already, but make it exact)
		totalArea = scipy.trapz(emission[:,1], emission[:,0])
		emission[:,1] /= totalArea
		
		return emission
	
	def upconverted_light(self, light, **kw):
		incomingFlux = self.total_absorption(light, **kw)
		
		outgoingFlux = incomingFlux/2.0 * self.iqe #Note that an iqe of 1 means 1 upconverted photon for 2 incoming photons
		
		emission = self.emission_gaussian()
		
		#Now scale by the total photon count and convert back to a power
		emission[:,1] *= outgoingFlux
		
		reference.vToPowerInPlace(emission)
		
		return emission.view(Spectrum)
	
	#Find the ideal (absolute) emission location for a given EQE or bandgap
	def ideal_shift(self, bg=-1.0, eqe=None):

		if eqe is None:
			if bg <= 0.0:
				raise Exception("You must pass either a valid EQE or a bandgap to UpconverterBase.ideal_shift.  bg was: " + str(bg))
			
			eqe = QuantumEfficiency.IdealQE(bg)

			gapWave = reference.ev_to_nm(bg)
			shiftLocation = gapWave - self.emissionHalfwidth*5 #slightly beyond the bandgap
		else:
			shiftLocation = self.ideal_emission(eqe) 
		
		return (eqe, shiftLocation)
	
#Subclasses of MobileUpconverter have to define a central peak location that can be moved about
#this class defines functions for calculating where best to place the upconverter so that we get
#the maximum benefits
class MobileUpconverter(UpconverterBase):

	#prepare the absorption function for fourier convolution
	#we need to invert it through the origin because we want the cross-correlation
	#and that's equivalent to the convolution of f*h(-t), hence we invert through the peak 
	#to be able to use the fft to quickly compute the cross-correlation
	def prepare_absorption_spectrum(self, explicitX=None):
		length = 8192
		oldPeak = self.peak
		self.peak = 4096.0
		
		if explicitX is None:
			a = np.linspace(0., length*1.0, length)
		else:
			assert(explicitX.shape[0] == length)
			a = explicitX
			
		upAbs = self.process_array(a)
		outAbs = scipy.fftpack.fftshift(upAbs)
		
		self.peak = oldPeak
		
		return outAbs
	
	#Fast estimation of the ideal benefits of using this upconverter with a cell of bandgap bg
	def ideal_benefit(self, bg=-1.0, inQE=None, device=None):
		light = Spectrum.AM15G()
		
		if device is None:
			(eqe, shiftLocation) = self.ideal_shift(bg, inQE) 
			totalCurrent = simplesim.photocurrent(light, eqe)
	
		else:
			(eqe, shiftLocation) = self.ideal_shift(eqe=device.back_eqe())
			totalCurrent = simplesim.photocurrent(light, device.front_eqe())
			
		
		#Sample light uniformly, convert it to a photon flux and then to an IQE scaled current
		light = utilities.sample_uniformly(light, 0, 8192.0, 8192)
		reference.vToFluxInPlace(light)
		light[:,1] *= self.iqe/2.0*reference.q
		
		#If no device, chop light at the bandgap
		if device is None:
			gapWave = reference.ev_to_nm(bg)
			light[light[:,0] <= gapWave,1] = 0.0
		else:
			light[:,1] *= device.transmitted_light(light[:,0])
		
		resp = light.copy()
				
		#Setup absorption spectrum
		oldShift = self.shift
		self.shift = shiftLocation

		upAbs = self.prepare_absorption_spectrum(explicitX=light[:,0])
		
		self.shift = oldShift
		
		#Do the correlation in Fourier Space
		upAbsF = scipy.fftpack.fft(upAbs)
		lightF = scipy.fftpack.fft(light[:,1])
		resp[:,1] = np.real(scipy.fftpack.ifft(np.conj(upAbsF)*lightF))
		
		return (resp[0:4096,:], totalCurrent)
	
	def uniform_absorption_spectrum(self):
		oldPeak = self.peak
		self.peak = 2048.0
		
		a = np.linspace(0, 4096.0, 4096) #power of two for efficiency
		upAbs = self.process_array(a)
		self.peak = oldPeak
		
		return upAbs
	
	#Quickly calculate the benefits of this absorption spectrum in its ideal location for ideal
	#solar cells with bandgaps in the range given.
	#For this to work, the upconverter must not have overridden the base upconverted_light method
	def benefits_vs_bg(self, minBG, maxBG, points):
		#prepare the solar spectrum
		light = Spectrum.AM15G()
		light = utilities.sample_uniformly(light, 0, 8192.0, 8192)
		reference.vToFluxInPlace(light)
		light[:,1]*= self.iqe/2.0*reference.q		#this is now a current density
		resp = light.copy()							#preallocate room for our response data
		
		upAbs = self.prepare_absorption_spectrum()
		upAbsF = np.conj(scipy.fftpack.fft(upAbs)) #conjugate so we calculate correlation rather than convolution
		
		#now iterate through the bg's backwards 
		results = np.ndarray([points,3])
		results[:,0] = np.linspace(maxBG, minBG, points) #iterate backwards so we can progressively chop the light
		
		for i in xrange(0, points):
			gapWave = reference.ev_to_nm(results[i,0])
			light[light[:,0] <= gapWave, 1] = 0.0
			
			lightF = scipy.fftpack.fft(light[:,1])
			resp[:,1] = np.real(scipy.fftpack.ifft(lightF*upAbsF))
			
			maxIndex = utilities.max_index(resp)
			results[i,1] = resp[maxIndex, 0]
			results[i,2] = resp[maxIndex, 1]

		return results
		
	#Old reference implementation of ideal_benefit (kept for testing correctness of faster version)
	def idealbenefit_reference(self, bg, inQE=None, points = 200, absolute=False, max=4000., explicitX=None):
		gapWave = reference.ev_to_nm(bg)
		
		(eqe, shiftLocation) = self.ideal_shift(bg, inQE)
				
		light = Spectrum.AM15G()
		totalCurrent = simplesim.photocurrent(light, eqe)
		
		light = utilities.sample_uniformly(light, 0, 8192.0, 8192)
		light = light.view(Spectrum)
		#choppedLight = light[light[:,0] > gapWave]
		
		if explicitX is None:
			a = np.linspace(0.0, max, points)
		else:
			a = explicitX
			
		b = np.zeros_like(a)
				
		oldShift = self.shift
		oldPeak = self.peak
		
		light[light[:,0] <= gapWave, 1] = 0.0
		
		self.shift = shiftLocation
		
		for i in xrange(0, a.shape[0]):
			self.peak = a[i]
			resp = self.upconverted_light(light)
			addCurrent = simplesim.photocurrent(resp, eqe)
			
			if absolute:
				b[i] = addCurrent
			else:
				b[i] = addCurrent/totalCurrent
		
		#Reset ourselves
		self.peak = oldPeak
		self.shift = oldShift
		
		return (a, b, totalCurrent)
	
	def ideallocation(self, bg, eqe=None, **kw):
		(b, totalCurrent) = self.ideal_benefit(bg, eqe, **kw)
		
		foundI = utilities.max_index(b)
		
		return (b[foundI,0], b[foundI,1], totalCurrent)