#empirical.py
#An upconverter with an empirically measured absorption spectrum and adjustable IQE

import math
import base
from solar.types import OpticalSolution
import numpy as np

class EmpiricalUpconverter (base.MobileUpconverter):
	#peak is the location of gaussian peak in nm
	#max is the maximum absorbance at that wavelength
	#halfwidth is the full width at half-maximum
	#shift is the antistokes shift of the upconverter in nm (relative to peak) and positive
	#the OpticalSolution must define bandStart, bandEnd and peak parameters
	def __init__(self, absorber, shift, iqe=1.0):
		base.UpconverterBase.__init__(self)
		
		self.absorber = absorber
		
		self.peak = absorber.peak

		self.shift = self.peak-shift
		self.iqe = iqe
	
	@staticmethod
	def FromReference(identifier, shift, iqe=1.0):
		absorber = OpticalSolution.LoadReference(identifier)
		
		up = EmpiricalUpconverter(absorber, shift, iqe)
		
		return up
	
	#shift this into the space of the absorber's measured absorption
	def __call__(self, x):
		normX = x - (self.peak - self.absorber.peak)
		
		return self.absorber.band_absorption_at(normX)
		
	def process_array(self, arr):
		return self.absorber.band_absorption(arr - (self.peak - self.absorber.peak))