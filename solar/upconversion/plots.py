#plots.py
#Convenience functions for generating common datasets that make nice plots related to upconversion

from gaussian import GaussianUpconverter
from .. import reference
from ..sim import simplesim
from ..types import Spectrum,QuantumEfficiency
import numpy as np


def IdealBenefit(bg, width, eqe=None, points = 200, absolute=False, emission=10, max=4000):
	gapWave = reference.ev_to_nm(bg)
	
	if eqe is None:
		eqe = QuantumEfficiency.IdealQE(bg)
		shiftLocation = gapWave - 100 #100 nm beyond the bandgap
	else:
		shiftLocation = eqe.max() 
		
	up = GaussianUpconverter(1000, 1, width, 100)
	up.emissionHalfwidth = emission
	
	light = Spectrum.AM15G()
	choppedLight = light[light[:,0] > gapWave]
	
	a = np.linspace(gapWave-2*width, max, points)
	b = np.zeros(points)
	
	totalCurrent = simplesim.photocurrent(light, eqe)

	up.shift = shiftLocation
	
	for i in xrange(0,points):
		up.peak = a[i]
		resp = up.upconverted_light(choppedLight)
		addCurrent = simplesim.photocurrent(resp, eqe)
		
		if absolute:
			b[i] = addCurrent
		else:
			b[i] = addCurrent/totalCurrent
	
	return (a, b, totalCurrent)

def IdealLocation(bg, width, eqe=None, **kw):
	(w, b, totalCurrent) = IdealBenefit(bg, width, eqe, **kw)
	
	max = -1.0
	foundI = -1
	for i in xrange(0, b.shape[0]):
		if b[i] > max:
			foundI = i
			max = b[i]
	
	return (w[foundI], b[foundI], totalCurrent)

def BenefitsVsBG(minBG, maxBG, width, **kw):
	a = np.linspace(minBG, maxBG, 100)
	
	b = np.vectorize( lambda x: IdealLocation(x,width, eqe=None, **kw)[1] )(a)
	
	return (a,b)
	
def IdealLocationVsBG(minBG, maxBG, width, bgPoints=100, **kw):
	a = np.linspace(minBG, maxBG, bgPoints)
	
	b = np.vectorize( lambda x: IdealLocation(x,width, **kw)[0] )(a)
	
	return (a,b)