#fom.py 
#Upconverter Figures of Merit

from solar.characterization import ScalarFOM, FigureOfMerit, SpectralResponseFOM
from solar.types import *
from solar.sim.simplesim import photocurrent

from solar import reference

class UpconverterShiftFOM (ScalarFOM):
	def __init__(self):
		ScalarFOM.__init__(self, "Upconverter Antistokes Shift", "eV")
	
	def _calculate(self, device, deps):
		if not hasattr(device, "uc") or device.uc == None:
			raise Exception("Cannot calc Upconverter figure of merit without an Upconverter")
		
		peak = device.uc.peak
		out = device.uc.shift
		
		peak = reference.wavelengthToJoules(peak)/reference.q
		out = reference.wavelengthToJoules(out)/reference.q
		
		self.value = out - peak
	
class UpconverterPeakFOM (ScalarFOM):
	def __init__(self):
		ScalarFOM.__init__(self, "Upconverter Spectral Absorption Peak", "nm")
	
	def _calculate(self, device, deps):
		if not hasattr(device, "uc") or device.uc == None:
			raise Exception("Cannot calc Upconverter figure of merit without an Upconverter")
		
		peak = device.uc.peak
			
		self.value = peak
		
#TODO: Refactor upconverter emission into a separate FOM

class UpconverterEmissionFOM (SpectralResponseFOM):
	def __init__(self):
		SpectralResponseFOM.__init__(self, "Upconverted Emission", "Upconverted Emission", "photons/cm^2/nm")
		self.depends.append("TransmittedLightFOM")
	
	def _calculate(self, device, deps):
		light = deps["TransmittedLightFOM"]
		
		emission = device.uc.upconverted_light(light, flux=True)
		reference.vToFluxInPlace(emission)
		self.value = emission

class UpconverterEmissionReflectionFOM (ScalarFOM):
	def __init__(self):
		ScalarFOM.__init__(self, "Fraction of Upconverted Light Reflected off Back Device Surface", "")
		self.depends.append("UpconverterEmissionFOM")
	
	def _calculate(self, device, deps):
		emit = deps["UpconverterEmissionFOM"].copy()
		
		total = emit.totalpower()
		
		coupling = device.back_coupling(emit[:,0])
		emit[:,1] *= coupling[:,2]
		refl = emit.totalpower()
		
		self.value = refl/total
		
class UpconverterEmissionTransmissionFOM (ScalarFOM):
	def __init__(self):
		ScalarFOM.__init__(self, "Fraction of Upconverted Light Escaping Front Device Surface", "")
		self.depends.append("UpconverterEmissionFOM")
	
	def _calculate(self, device, deps):
		emit = deps["UpconverterEmissionFOM"].copy()
		
		total = emit.totalpower()
		
		coupling = device.back_coupling(emit[:,0])
		emit[:,1] *= coupling[:,3]
		trans = emit.totalpower()
		
		self.value = trans/total

class UpconverterAbsorbedFractionFOM (ScalarFOM):
	def __init__(self):
		ScalarFOM.__init__(self, "Fraction of UC Absorbed Light", "")
		self.depends.append("TransmittedLightFOM")
		
	def _calculate(self, device, deps):
		light = deps["TransmittedLightFOM"]
		
		total = light.totalpower()
		abs = device.uc.total_absorption(light, flux=True)
		
		self.value = abs/total
		
class UpconverterMaxJscImprovementFOM (ScalarFOM):
	r"""
	The maximum Jsc improvement possible for this upconverter in this device configuration
	even with a 100% eqe in the device for all the upconverted light and no coupling losses
	"""
	
	def __init__(self):
		ScalarFOM.__init__(self, "Max Possible Jsc Improvement Due to Upconverter", "mA/cm^2")
		self.depends.append("TransmittedLightFOM")
		self.depends.append("UpconverterEmissionFOM")
		
	def _calculate(self, device, deps):
		self.value = deps["UpconverterEmissionFOM"].totalpower()*reference.q * 1000. 

class UpconverterEmissionCouplingFOM (ScalarFOM):
	r"""
	Return the fraction of upconverted light that is successfully coupled back into the
	device active layer.
	"""
	
	def __init__(self):
		ScalarFOM.__init__(self, "Fraction of Upconverted Light Coupled into Device", "")
		self.depends.append("TransmittedLightFOM")
		
	def _calculate(self, device, deps):
		light = deps["TransmittedLightFOM"]
		
		emission = device.uc.upconverted_light(light, flux=True)
		
		reference.vToFluxInPlace(emission)
		total = emission.totalpower()
		
		emission[:,1] *= device.back_coupling(emission[:,0])[:,1] #get useful device absorption
		
		coupled = emission.totalpower()
		
		self.value = coupled/total
		
class AverageUpconvertedLightIQE (ScalarFOM):
	r"""
	Computes the flux-weighted iqe of the device over the emission range of the up-converter
	to determine the average fraction of upconverted photons *absorbed in the device*
	that are successfully collected as current
	"""
	def __init__(self):
		ScalarFOM.__init__(self, "Average Device IQE for Upconverted Light ", "")
		self.depends.append("TransmittedLightFOM")
		
	def _calculate(self, device, deps):
		light = deps["TransmittedLightFOM"]
		
		emission = device.uc.upconverted_light(light, flux=True)
		
		iqe = device.back_iqe()
		coupling = device.back_coupling(emission[:,0])
		
		emission[:,1] *= coupling[:,1]

		converted = iqe.response(emission).view(Spectrum)
		converted = converted.totalpower()
		
		reference.vToFluxInPlace(emission)
		total = emission.totalpower()
		
		#print "IQE total current:", converted*reference.q*1000
		
		self.value = converted/total
		
class AverageUpconvertedLightEQE (ScalarFOM):
	r"""
	Computes the flux-weighted eqe of the device over the emission range of the up-converter
	to determine the average fraction of upconverted photons incident on the device
	that are successfully collected as current
	"""
	def __init__(self):
		ScalarFOM.__init__(self, "Average Device EQE for Upconverted Light ", "")
		self.depends.append("TransmittedLightFOM")
		
	def _calculate(self, device, deps):
		light = deps["TransmittedLightFOM"]
		
		emission = device.uc.upconverted_light(light, flux=True)
		
		qe = device.back_eqe()
		
		converted = qe.response(emission).view(Spectrum)
		converted = converted.totalpower()
		
		reference.vToFluxInPlace(emission)
		total = emission.totalpower()
		
		#print "EQE total current:", converted*reference.q*1000
		
		self.value = converted/total
		
class UpconverterInterfaceParasiticLossFOM (ScalarFOM):
	r"""
	Return the fraction of upconverted light that is successfully coupled back into the
	device active layer.
	"""
	
	def __init__(self):
		ScalarFOM.__init__(self, "Fraction of Upconverted Light Parasitically Absorbed", "")
		self.depends.append("TransmittedLightFOM")
		
	def _calculate(self, device, deps):
		light = deps["TransmittedLightFOM"]
		
		emission = device.uc.upconverted_light(light, flux=True)
		
		reference.vToFluxInPlace(emission)
		total = emission.totalpower()
		
		stack = device._optical_stack.reverse()
		
		if stack.layers[0].finite():
			stack.prepend_layer('air')
		
		if stack.layers[-1].finite():
			stack.add_layer('air')
		
		abs = stack.absorption_in_layers(emission[:,0]) #get useful device absorption
		ilayers = stack.tagged_layers("back_interface")
		
		for i in xrange(0, emission.shape[0]):
			loss_frac = 0.0
			for lay in ilayers:
				loss_frac += abs[i, lay+1]
			
			emission[i,1] *= loss_frac
		
		loss = emission.totalpower()
		
		self.value = loss/total

class UpconverterJscImprovementFOM (ScalarFOM):
	def __init__(self):
		ScalarFOM.__init__(self, "Actual Jsc Improvement Due to Upconverter", "mA/cm^2")
		self.depends.append("TransmittedLightFOM")
		
	def _calculate(self, device, deps):
		light = deps["TransmittedLightFOM"]
		
		emission = device.uc.upconverted_light(light, flux=True)
		
		self.value = photocurrent(emission, device.back_eqe())*1000.