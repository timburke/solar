#gaussian.py
#An ideal gaussian upconverter with controllable absorption peak location and size

import math
import base
import numpy as np

class GaussianUpconverter (base.MobileUpconverter):
	r"""
	
	An upconverter model with a Gaussian absorption profile of user-defined width (full width at half maximum). 
	
	The absorption function is given by:

	.. math::
	    
	    A(\lambda) = A_o \exp\left(\frac{-(\lambda - \lambda_o)^2}{2 \sigma^2}\right)

	where :math:`\lambda_o` is given by peak, :math:`\sigma` is related to width and :math:`A_o` is equal to max.
	"""
	
	def __init__(self, peak, max, width, shift):
		"""
		Creating a new GaussianUpconverter
		
		:param peak: the location of the center of the absorption peak in nm
		:type peak: float
		:param max: the maximum absorption value at the peak (must be in [0,1]).
		:type max: float
		:param width: the full width at half maximum of the absorption peak
		:type width: float
		:param shift: the anti-stokes shift in nm.  Upconverted light will appear centered at peak - shift.  This shift should be positive.
		:type shift: float
		"""
		
		base.UpconverterBase.__init__(self)
		
		self.peak = peak
		self.max = max
		self.width = width
		self.shift = peak-shift
	
	#return the absorption response of this upconverter at wavelength x
	def __call__(self, x):
		c = self.width / 2.35482 #convert halfwidth to standard deviation
		
		return self.max * math.exp(-1.0 * (x-self.peak)*(x-self.peak)/(2*(c**2.0)))
	
	#Speedup function for processing an entire array with this response function
	def process_array(self,arr):
		c = self.width / 2.35482
		invDen = 1.0/(2*c**2.0)
				
		return self.max * np.exp(-1.0 * np.square(arr-self.peak) * invDen)
	
	def sweepWidth(self, low, high, points = 10, bg=-1.0, eqe=None,absolute=False):
		a = np.linspace(low, high, points)
		b = np.zeros_like(a)
		
		oldWidth = self.width
		
		for i in xrange(0, a.shape[0]):
			self.width = a[i]
			b[i] = self.ideallocation(bg, eqe, absolute=absolute)[1]
		
		return (a,b)