#study.py
#Supports do in silica studies of device figure of merit changes due to changing
#device parameters in a convenient and consistent fashion.

import numpy as np
from characterization import *

class Experiment:
	def __init__(self, **kwargs):
		self.device = None
		self.tracked_fom = []
		self.initializer = None
		self.iterator = None
		self.capture_initial = False
	
		if "device" in kwargs:
			self.device = kwargs["device"]
		
		if "initial" in kwargs:
			self.capture_initial = True

	def track_figure(self, fom):
		r"""
		Track the changes in the specified figure of merit as this experiment 
		progresses.  If fom is a string, it is assumed to specify the class name
		of a FOM already defined for the device attached to this Experiment.
		If fom is an instant of FigureOfMerit, it is added to the device and then tracked.
		"""
		if isinstance(fom, str):
			if self.device is None:
				raise exception("You must first attach a device to the experiment before tracking figures of merit about that device.")
			
			self.tracked_fom.append(fom)
		else:
			self.device.track_figure(fom)
			self.tracked_fom.append(fom.__class__.__name__)
			
	def _build_results(self, l):
		self.results = np.ndarray([l, len(self.tracked_fom)+1])
	
	def _collect_scalar(self, index, name):
		self.device.figures[name].update(self.device, self.device.figures)
		self.results[index,  self.tracked_fom.index(name)+1] = self.device.figures[name].value
	
	def _collect_figures(self, iter):
		for name in self.tracked_fom:
				if isinstance(self.device.figures[name], ScalarFOM):
					self._collect_scalar(iter, name)
				else:
					print "Ignoring unsupported nonscalar figure of merit: %s" % name
	
	def get_series(self, name):
		fig = np.ndarray([self.results.shape[0], 2])
		fig[:,0] = self.results[:,0]
		fig[:,1] = self.results[:, self.tracked_fom.index(name)+1]
		
		return fig

	def run(self, istart, count):
		if self.iterator is None or self.device is None:
			raise Exception("You must specify a an iterator function and a device for this experiment")

		res = None #x-coord for the first data point (returned from the initializer possibly
		if self.initializer is not None:
			res = self.initializer(self.device)
		
		#offset so that we store results in the right place regardless of whether we capture the initial state
		offset = 0
		
		if self.capture_initial:
			self._build_results(count+1)
			if res is None:
				self.results[0,0] = 0.0
			else:
				self.results[0,0] = res
			
			self._collect_figures(0) #store the initial values for the FOM
			offset = 1
		else:	
			self._build_results(count)
				
		print "Starting Experiment"
		
		for i in xrange(0,count):
			val = self.iterator(self.device, istart+i)
			self.results[i+offset, 0] = val
			
			self._collect_figures(i+offset)
			
		print "Ending Experiment"