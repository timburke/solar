#tensor.py
#Given a function that takes N arguments and N vectors containing the range of
#values that each argument should span, construct an N-tensor containing the function value
#evaluated over the space spanned by the N vector arguments.

import numpy as np

def tensor_eval(f, *args):
	inargs = np.meshgrid(*args, copy=False, indexing='ij')
	return f(*inargs)

def enforce_1d(f):
	if not hasattr(f, 'shape'):
		f = np.array(f)

	if len(f.shape) > 1:
		return f.flatten()

	if len(f.shape) == 0:
		return f.reshape([1,])

	return f

def prepend_array(ar, others):
	shape = [x for x in others.shape]
	shape.insert(0, ar.shape[0])
	shape[-1] += 1

	res = np.zeros(shape)
	res[..., 0] = ar.reshape([ar.shape[0], 1])
	res[..., 1:] = others.reshape([1, others.shape[0], others.shape[1]])

	return res