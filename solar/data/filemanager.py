#filemanager.py
#The portion of the Database class dealing with storing original datafiles
#This is a mixin class so it just defines methods and its purpose is to provide 
#a bit of organization to what will be a very large and complex class (Database)

import shutil, string
import os.path
from solar.data import *
from tables.nodes import filenode
import schema.filemanager
import hashlib
import time

class FileManager (object):
	def create_superblock_entry(self, name, file_entry, id, hash):
		"""
		Create a new superblock entry for this file.
		"""
		
		superblock = self.db.root.filemanager.superblock
		
		entry = superblock.row
		
		entry["file_id"] = id
		entry["file_entry"] = file_entry
		entry["file_name"] = name
		entry["hash"] = hash
		entry["series_id"] = 0
		entry["added_time"] = time.time()
		
		entry.append()
		superblock.flush()
		
	def remove_superblock_entry(self, id):
		"""
		Remove the superblock entry corresponding to the file id given as id
		"""
		
		superblock = self.db.root.filemanager.superblock
		
		foundEntries = [row for row in superblock.where('file_id == %d' % id)]
		
		if len(foundEntries) > 1:
			raise ValueError("Database is corrupted; there is more than one entry with a given file id")
		
		if len(foundEntries) == 0:
			return
		
		superblock.removeRows(foundEntries[0].nrow)
		superblock.flush()
		
	def hash_file(self, file):
		md5 = hashlib.md5()
		for chunk in iter(lambda: file.read(8192), b''): 
			md5.update(chunk)
		
		return md5.hexdigest()
		
	def import_file(self, filename, **kw):
		"""
		Import the file given and store it in the database, returning a unique identifier.
		"""
		
		if not os.path.isfile(filename):
			raise ValueError("{file} is not a regular file".format(file=filename))
		
		realfile = open(filename, "rb")
		guid = self.generate_unique_name(prefix="f_")
		name = os.path.basename(filename) #get the filename
		
		node = filenode.newNode(self.db, where=file_path, name=guid)
		node.attrs.name = name
		
		id = self.get_next_id()
		node.attrs.id = id
		
		#copy the file across
		shutil.copyfileobj(realfile, node)
		realfile.close()
		
		#verify that the hashes agree and add the superblock entry
		with open(filename, "rb") as realfile:
			realhash = self.hash_file(realfile)
		
		node.seek(0)
		storedhash = self.hash_file(node)
				
		if storedhash != realhash:
			raise ValueError("Could not copy file correctly")
		
		self.create_superblock_entry(name, guid, id, realhash)
						
		#set whatever optional metadata we were passed
		for (key, val) in kw.items():
			node._f_setAttr(key, val)
		
		node.close()
			
		#return our newly created file id
		return id

	def delete_file(self, id):
		"""
		Remove the file corresponding to the integer file id passed in.
		"""
		
		loc = self.get_file(id=id)
		
		#Remove the file node and the superblock entry pointing to it
		self.remove_superblock_entry(id)
		self.db.removeNode(where=file_path, name=loc)

	@staticmethod
	def initialize_database(db, **kw):
		"""
		Initialize the filemanager related tables for this database
		"""

		managerNode = db.createGroup("/", 'filemanager', 'Filemanager Related Nodes')
		filesNode = db.createGroup(managerNode, "files", "Original Files")
		controlTable = db.createTable(managerNode, "superblock", schema.filemanager.SuperBlock, "File Accounting Information")
		
	def __init__(self):
		"""
		Setup hooks to allow us to create the tables we need in new databases and register other
		callbacks.
		"""
		
		super(FileManager, self).__init__()
		
	def lookup_file(self, id):
		foundEntries = [row for row in self.db.root.filemanager.superblock.where('file_id == %d' % id)]
		
		if len(foundEntries) == 0:
			raise ValueError("File id not found.")
		elif len(foundEntries) > 1:
			raise ValueError("Database seems corrupted.  There should be only one file with a given ID.")
		
		return foundEntries[0]["file_entry"]
		
	def get_file(self, **kw):
		"""
		Get a file based on certain information.  If we're given a file id, we look it up in the superblock.
		If we're given a file_entry, we return it directly.  If the file does not exist, we raise an exception
		"""
		
		if "id" in kw:
			entry = self.lookup_file(int(kw["id"]))
		elif "path" in kw:
			entry = kw["path"]
		else:
			raise ValueError("You must specify either an id or a path for the file.")
		
		if not self.db.root.filemanager.files._g_checkHasChild(entry):
			raise ValueError("Could not find file with name: %s" % entry)
		
		return self.db.root.filemanager.files._f_getChild(entry)