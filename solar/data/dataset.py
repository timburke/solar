#dataset.py
#Mixin class defining the way the database deals with parsing and storing datasets that
#it understands.  This is the primary function of the database, which is providing a rich
#API for accessing and manipulating experimental data.

from schema.dataset import DataSetList
import data_handlers, file_handlers, storage_handlers
import inspect

class DataSetManager (object):
	@staticmethod
	def initialize_database(db, **kw):
		datasets = db.createGroup("/", 'datasets', 'Parsed Datasets')
		contents = db.createTable(datasets, "contents", DataSetList, "Master DataSet List")
	
	def canonicalize_name(self, name):
		return name.lower()
	
	def import_dataset(self, path, type, **kw):
		#Find the appropriate handlers for this file type
		if self.canonicalize_name(type) not in self.data_handlers:
			raise ValueError("Unknown data type: %s" % str(type))
		
		data_handler = self.data_handlers[self.canonicalize_name(type)]
		file_handler = self.file_handlers[self.canonicalize_name(data_handler.file_handler()[0])]
		storage_handler = self.storage_handlers[self.canonicalize_name(data_handler.storage_handler())]
		
		file = open(path, "rb")
		kwargs = data_handler.file_handler()[1]
		
		#Send file through the data processing pipeline
		data = file_handler.load_file(file, **kwargs)
		processed_data = data_handler.load_dataset(data)
		
		#Check if we need additional data
		#TODO: FINISH THIS
		
		#check if we want to save the dataset in the database or just return it formatted to the user
		if "nosave" in kw and kw["nosave"] == True:
			dset = data_handler.retrieve_dataset(processed_data)
			dset.dataset_id = -1
			
			return dset
		
		#Actually save the dataset into the db
		identifier = storage_handler.store_dataset(self, processed_data)
		file.close()

		#Store the original file
		file_id = self.import_file(path)
		
		#now create the contents entry for this dataset
		id = self.create_contents_entry(identifier, handler=data_handler, file_id=file_id)

		dset = data_handler.retrieve_dataset(processed_data)
		
		dset.dataset_id = id
		
		return dset
	
	def retrieve_dataset(self, id):
		"""
		Retrieve a dataset corresponding to the id given and convert it to its native
		PyaSol object for analysis using the mapping provided by the dataset's associated 
		data_handler.
		
		Raises an exception if the dataset cannot be found or something else goes wrong.
		"""
		
		entry = self._find_dataset(id)
		loc = entry["storage_location"]
		handler_name = entry["data_class"]
		
		if handler_name not in self.data_handlers:
			raise ValueError("The requested dataset is of type %s and a handler for that type cannot be found." % handler_name)
		
		handler = self.data_handlers[handler_name]
		storage_handler = self.storage_handlers[handler.storage_handler()]
		
		#use the storage handler to get the dataset based on its location and then the data_handler to
		#wrap it in the right way for analysis
		data = storage_handler.retrieve_dataset(self, loc)
		
		return handler.retrieve_dataset(data)
	
	def _find_dataset(self, id):
		contents = self.db.root.datasets.contents
		
		foundEntries = [row for row in contents.where('dataset_id == %d' % id)]
		
		if len(foundEntries) == 0:
			raise ValueError("Could not find dataset with id=%d" % id)
		
		if len(foundEntries) > 1:
			raise ValueError("Database seems corrupted, there was more than one dataset with the given id.")
		
		return foundEntries[0]
		
	
	def create_contents_entry(self, identifier, handler, file_id=0, series=0):
		"""
		Create an entry for this dataset in the dataset table of contents.
		"""
		
		row = self.db.root.datasets.contents.row
		id = self.get_next_id()

		row["dataset_id"] = id		
		row["file_id"] = file_id
		row["dataset_series"] = series
		
		row["storage_location"] = identifier
		row["data_class"] = self.canonicalize_name(handler.name())
		
		row.append()
		self.db.root.datasets.contents.flush()
		
		return id
		
	def find_classes_in_module(self, module):
		symbols = dir(module)
		
		classes = {}
		
		for symbol in symbols:
			if symbol[0] == '_':
				continue
			
			obj = getattr(module, symbol)
			
			if inspect.isclass(obj) and hasattr(obj, "name"):
				classes[self.canonicalize_name(obj.name())] = obj
		
		return classes
		
	def __init__(self):
		
		#Register handlers to do the actual import processing for the datatype that we know about
		self.data_handlers = self.find_classes_in_module(data_handlers)
		self.file_handlers = self.find_classes_in_module(file_handlers)
		self.storage_handlers = self.find_classes_in_module(storage_handlers)
				
		super(DataSetManager, self).__init__()