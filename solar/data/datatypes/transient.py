#transient.py
#Transient Absorption Dataset

import numpy as np
import matplotlib as plt

class TransientAbsorptionData (np.ndarray):
	"""
	A convenience class for transient absorption datasets acquired using our lab setup.
	
	TODO: get a copy of this file format and finish.
	"""
	
	def __new__(cls, *args, **kwargs):
		return np.ndarray.__new__(cls, *args, **kwargs)
	
	def __array_finalize__(self, obj):
		pass
		
	def wavelength_range(self):
		"""
		Return the smallest and largest wavelength in this set
		"""
		
		return (np.min(self[:,0]), np.max(self[:,0]))
	
	def plot(self):
		"""
		Plot the fractional change in absorption
		"""
		
		plt.plot(self[:,0], self[:,2])