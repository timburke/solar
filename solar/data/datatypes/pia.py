#pia.py

import numpy as np
import matplotlib as plt

class PhotoinducedAbsorptionData (np.ndarray):
	"""
	A convenience class for PIA datasets acquired using our lab setup.
	They are 11 column arrays with the following columns:
	1. Wavelength (nm)
	2. Energy (eV)
	3. -dT/T
	4. dT/T - r
	5. dT/T - theta (deg)
	6. dT (Volts)
	7. dT PL (V)
	8. dT PL theta (deg)
	9. T (V)
	10. Frequency (Hz)
	11. Temperature (K)
	"""
	
	def __new__(cls, *args, **kwargs):
		return np.ndarray.__new__(cls, *args, **kwargs)
	
	def __array_finalize__(self, obj):
		pass
		
	def wavelength_range(self):
		"""
		Return the smallest and largest wavelength in this set
		"""
		
		return (np.min(self[:,0]), np.max(self[:,0]))
	
	def plot(self):
		"""
		Plot the fractional change in absorption
		"""
		
		plt.plot(self[:,0], self[:,2])