#filemanager.py
#Schema for the FileManager portion of the database

from tables import *

class SuperBlock (IsDescription):
	file_id 	= Int64Col()
	series_id	= Int64Col()
	file_entry 	= StringCol(40)
	file_name 	= StringCol(40)
	hash		= StringCol(40)
	added_time 	= Time64Col()