#Schema for keeping track of datasets in the databse

from tables import *

class DataSetList (IsDescription):
	dataset_id			= Int64Col()
	file_id				= Int64Col() #link this dataset to the original file that it came from
	dataset_series 		= Int64Col()
	data_class			= StringCol(40)
	storage_location	= StringCol(40) #an opaque value made available to the storage class indicating where to find this dataset
	