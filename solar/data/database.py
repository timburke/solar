#The basic pytables based database used in this module

import tables
import inspect
import uuid
import string

from schema.control import *
from solar.data import current_database_version

#mixin classes defining different aspects of our behavior
from filemanager import FileManager
from dataset import DataSetManager

class Database (FileManager, DataSetManager):	
	def generate_unique_name(self, prefix):
		id = uuid.uuid4()
		
		name = prefix + str(id)
		
		return string.replace(name, '-', '_')
	
	@staticmethod
	def initialize_database(db, **kw):
		"""
		Initialize a new empty database with the necessary control structures for proper
		operation.
		"""
		
		#create the administrative section
		admin = db.createGroup("/", 'admin', 'Administrative Information')
		
		#initialize various control values
		admin._v_attrs.last_id = 0
		admin._v_attrs.last_series = 0
		admin._v_attrs.version = current_database_version
		admin._v_attrs.description = "A PyaSol database file."
		
		#iterate through all the mixin classes and call their database init functions
		mixins = inspect.getmro(Database)
		for i in xrange(1, len(mixins)): 
			#start at 1 to skip this class and only iterate over the mixins (check for the attribute to avoid the object base class)
			if hasattr(mixins[i], "initialize_database") and callable(getattr(mixins[i], "initialize_database")):
				getattr(mixins[i], "initialize_database")(db, **kw)
		
		
		if 'owner' in kw:
			admin._v_attrs.owner = str(kw["owner"])
		else:
			admin._v_attrs.owner = "Anonymous"
	
	@staticmethod
	def create(filename, title, **kw):
		"""
		Create a new database for storing experimental data.
		"""
		
		file = tables.openFile(filename, mode='w', title = title)
		
		#Populate the initial control tables
		Database.initialize_database(file)
		
		file.close()
		
		return Database(filename)
		
	def __init__(self, filename):
		self.db = tables.openFile(filename, mode="r+")
		self.admin = self.db.root.admin
		
		super(Database, self).__init__() #allow the mixin classes to do their initialization as well
		
	def close(self):
		"""
		Close the HDF5 database.
		"""
		
		self.db.close()
		
	def get_next_id(self):
		"""
		Get the next sequential id for this database
		"""
		
		id = self.admin._v_attrs.last_id+1
		self.admin._v_attrs.last_id += 1
		
		return id
		
	def get_next_series(self):
		"""
		Get the next series id
		"""
		series = self.admin._v_attrs.last_series+1
		self.admin._v_attrs.last_series += 1
		
		return series