#The data module defines a database for storing experimental datasets, searching for them
#and working with them easily

current_database_version = 100 #packed major.minor.patch with two digits per (00.01.00)
file_path = "/filemanager/files"
admin_path = "/admin"