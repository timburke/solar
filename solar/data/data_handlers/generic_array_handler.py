#generic_array_handler.py
#data handler for generic numpy array datasets

from solar.types import QuantumEfficiency
from base_data_handler import BaseDataHandler

class GenericArrayHandler (BaseDataHandler):
	_name = "GenericArrayHandler"
	_params = {"delimiter": ",", "skip_header": 1}
	
	@classmethod
	def name(cls):
		return cls._name
	
	@staticmethod
	def storage_handler():
		return "Array"
	
	@classmethod
	def file_handler(cls):
		return ("Spreadsheet", cls._params)
	
	@staticmethod
	def load_dataset(dataset):
		"""
		Given the output of out designated file handler, process the data into a form acceptable
		to our storage handler
		"""
				
		return dataset
	
	@classmethod
	def retrieve_dataset(cls, dataset):
		"""
		Given the output of our designated storage handler, wrap the results into an object 
		amenable to analysis and return it to the user.  Note that this function is also called
		with the results of load_dataset.  So the return value of load_dataset should be indistinguishable
		from the return value of this class's storage handler.
		"""
		
		return dataset.view(cls._type)
	
	@staticmethod
	def store_dataset(dataset):
		"""
		Given an object of the type that retrieve_dataset returns, convert it to a form that can
		be passed to the storage_handler for storing in the database.
		"""
		
		return dataset