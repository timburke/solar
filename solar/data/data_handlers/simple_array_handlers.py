#simple_array_handlers

from generic_array_handler import GenericArrayHandler

from solar.types import QuantumEfficiency
from solar.data.datatypes.pia import PhotoinducedAbsorptionData

#Defined array handlers
class QuantumEfficiencyHandler (GenericArrayHandler):
	_name = "QuantumEfficiency"
	_type = QuantumEfficiency

class PIAHandler (GenericArrayHandler):
	_name = "PhotoinducedAbsorption"
	_type = PhotoinducedAbsorptionData
	_params = {"delimiter": "\t", "skip_header": 1}
	_questions = {
		#Format
		#variable name (type 	required 	question text)
		"sample-type": (str, 	False, 		"What type of sample was this experiment performed on?")
	}