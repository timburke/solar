#base_data_handler.py

class BaseDataHandler:
	"""
	Base class for data handlers defining convenience functions that are commonly needed.
	"""
	
	@classmethod
	def requires_additional_data(cls):
		"""
		Return true if this data handler requires user interaction to obtain metadata
		about the datasets that it processes.  By default we check if the data handler 
		class defines a _questions attribute to answer this question.
		"""
		
		if hasattr(cls, "_questions"):
			return True
		else:
			return False
	
	@classmethod
	def additional_questions(cls):
		"""
		Return tuples describing the additional data that we need about this dataset type.
		"""
		
		return cls._questions