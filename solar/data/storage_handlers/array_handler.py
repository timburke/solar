#array_handler.py
#This storage handler takes a numpy array and stores it in the database returning the node
#that it created

from tables import *
import numpy as np

class ArrayHandler:
	@staticmethod
	def name():
		return "Array"
	
	@staticmethod
	def store_dataset(database, dataset):
		"""
		Store the python object dataset into the database.  The dataset can come from one of
		two sources: either a file or programmically.  In either case it will have been processed
		through a data_handler that has indicated that this storage_handler knows how to store
		its dataset.  
		
		This function should store the dataset in the database and return an opaque string
		of less than 40 characters that will allow it to retrieve the dataset later.
		"""
		
		name = database.generate_unique_name(prefix="d_")
		
		db = database.db
		
		#TODO: Add atom type?
		node = db.createCArray(db.root.datasets, name, Atom.from_dtype(dataset.dtype), dataset.shape)
		node[:] = dataset[:]
		
		return name
		
	@staticmethod
	def fast_conversion(hdfarray):
		a=np.empty(shape=hdfarray.shape,dtype=hdfarray.dtype)
		a[:]=hdfarray[:]
		return a
	
	@staticmethod
	def retrieve_dataset(database, dataset):
		db = database.db
		
		return ArrayHandler.fast_conversion(db.root.datasets._f_getChild(dataset))