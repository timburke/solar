#spreadsheet_handler.py

import numpy as np

class SpreadsheetHandler:
	@staticmethod
	def name():
		return "Spreadsheet"
	
	@staticmethod
	def load_file(file, **kw):
		"""
		Load the information found in the file at the given path.  File will already be opened
		according to the specifications determined by this class
		Data should be returned in a handler-specific form.  Keyword parameters can be passed from the
		data_handler driving this process to allow for a generic file_handler to handle 
		many different related file types.
		"""
		
		data = np.genfromtxt(file, **kw)
		
		return data