#file_handlers
#The classes listed in this file are all the registered file handlers that are able to take
#a dataset file and parse it into a form amenable to passing to the data_handler

from spreadsheet_handler import SpreadsheetHandler