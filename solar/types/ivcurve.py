#iv_curve.py

import os.path
import numpy as np
import datetime

class IVCurve(np.ndarray):
	def __new__(cls, *args, **kwargs):
		return np.ndarray.__new__(cls, *args, **kwargs)
	
	def __array_finalize__(self, obj):
		pass

	@staticmethod
	def Load(path, attribs=True):
		"""
		Given an IV curve file taken from the McGehee group IV curve software,
		check it and load it.
		"""

		with open(path, "r") as f:
			rawlines = f.readlines()

		lines = [x.rstrip() for x in rawlines]
		header = lines[24]
		if header != 'Voltage\tCurrent1':
			if lines[25] == 'Voltage\tCurrent1':
				skip = 26
			else:				
				raise ValueError("Invalid file type, IV header line was %s" % header)
		else:
			skip = 25

		ivdata = np.genfromtxt(path, skiprows=skip, delimiter="\t").view(IVCurve)

		if attribs:
			ivdata.add_attributes(path)

		return ivdata

	def add_attributes(self, file):
		"""
		Read in the additional information from an iv curve file and save
		it along with the raw IV curve data.
		"""

		with open(file, "r") as f:
			rawlines = f.readlines()

		lines = [x.rstrip() for x in rawlines]
		
		#Time is stored as m/d/y<tab>hh:mm [AM|PM]
		timeline = lines[0]
		self.acquision_time = datetime.datetime.strptime(timeline, "%m/%d/%Y\t%I:%M %p")

		#Acquire figures of merit
		self.voc = float(lines[16][8:])
		self.jsc = -1.0*float(lines[15][13:])
		self.ff = float(lines[18][7:])
		#self.maxpower = -1.0*float(lines[17][18:])

		#TODO: Acquire device area, series and shunt resistances, mpp current
		