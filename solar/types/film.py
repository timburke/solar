#film.py
import numpy as np
import math
from scipy import integrate, interpolate
import solar.reference
import solar.utilities
import solar.fresnel
import os.path
import ConfigParser
from numbers import Number
from copy import deepcopy
from sets import Set
import thinfilm

class OpticalMaterial:
	r"""
	An optically active material with a wavelength dependent complex index of refraction.
	This class is used to help define stacks of thin (or thick) optical layers.
	"""
	
	def __init__(self, n, k, extend=False):
		r"""
		You construct an optical material from the real and complex parts of its refractive index.
		
		:param n: The real part of the index of refraction.  If n is a callable it should be
			a function that returns the index of refraction as a function of wavelength in nm. If n is an 
			array, it will be quadratically interpolated to produce such a function.  If n is a number,
			it will be used for all wavelengths
		:param k: The extinction coefficient.  If k is a callable, it should bea function that
			returns the extinction coefficient as a function of wavelength in nm.  If k is an array,
			it will be quadratically interpolated to produce such a function.  If k is a number, it 
			will be used for all wavelengths.
		:param extend: Whether the first and last data points of the n and k arrays (if they're arrays)
			should be used for all values outside the sampling domain.
		"""
		
		if isinstance(n, np.ndarray):
			if extend == True:
				nMin = 1.0
				nMax = np.inf
				self.n = solar.utilities.interpolate_array(n, kind="linear", extend=True)
			else:
				nMin = np.min(n[:,0])
				nMax = np.max(n[:,0])		
				self.n = solar.utilities.interpolate_array(n, kind="linear")
		elif isinstance(n, np.lib.function_base.vectorize):
			nMin = 1.0
			nMax = np.inf
			self.n = n
		elif hasattr(n, '__call__'):
			nMin = 1.0
			nMax = np.inf
			self.n = np.vectorize(n)
		elif isinstance(n, Number):
			nMin = 1.0
			nMax = np.inf
			self.n  = np.vectorize(lambda x: n)
		else:
			raise Exception("Invalid index of refraction: " + str(n))
			
		if isinstance(k, np.ndarray):
			if extend == True:
				kMin = 1.0
				kMax = np.inf
				self.k = solar.utilities.interpolate_array(k, kind="linear", extend=True)
			else:
				kMin = np.min(k[:,0])
				kMax = np.max(k[:,0])
				self.k = solar.utilities.interpolate_array(k, kind="linear")
		elif isinstance(k, np.lib.function_base.vectorize):
			kMin = 1.0
			kMax = np.inf
			self.k = k
		elif hasattr(k, '__call__'):
			kMin = 1.0
			kMax = np.inf
			self.k = np.vectorize(k)
		elif isinstance(k, Number):
			kMin = 1.0
			kMax = np.inf
			self.k  = np.vectorize(lambda x: k)
		else:
			raise Exception("Invalid extinction coefficient: " + str(k))
			
		#Set the wavelength bounds for which we are valid:
		self.min_valid = max(nMin, kMin)
		self.max_valid = min(nMax, kMax)
		self.tag = None
			
	def refraction(self, wavelengths):
		r"""
		Return the complex refractive index at the given wavelengths
		"""
		
		return self.n(wavelengths) + 1j*self.k(wavelengths)
	
	@staticmethod
	def FromReference(name):
		r"""
		Load a reference material shipped with this package.
		  
		:param name: The name of the reference material in either upper or lowercase.  It will be canonicalized before
			being used to look up the material.
		"""
		
		directory = os.path.join(os.path.dirname(__file__), "..", "reference_data", "optical")
		
		k_name = name.lower() + "_k.csv"
		n_name = name.lower() + "_n.csv"
		
		k_path = os.path.join(directory, k_name)
		n_path = os.path.join(directory, n_name)
		
		k_arr = np.genfromtxt(k_path, skip_header=1, delimiter=",")
		n_arr = np.genfromtxt(n_path, skip_header=1, delimiter=",")
		
		mat = OpticalMaterial(n_arr, k_arr, extend=True)
		
		return mat

class OpticalFilm:
	def __init__(self, material, thickness):
		r"""
		Thickness must be in nm.
		"""
		
		self.thickness = thickness
		self.material = material
		self.coherent = True
		self.tag = None
	
	def index_matched(self, thickness=np.inf):
		r"""
		Return a new film with the same properties as this one, but with the specified thickness and no extinction.  This is useful for 
		simulating perfect light incoupling by adding a half-space of indexed matched material instead of air for the thin-film simulator
		"""
		
		mat = OpticalMaterial(self.material.n, 0)
		return OpticalFilm(mat, thickness)
	
	def finite(self):
		return np.isfinite(self.thickness)
	
	@staticmethod
	def ARC(center_wave, final_layer, initial_layer=None):
		if initial_layer is None:
			ideal_n = np.sqrt(final_layer.material.n(center_wave))
		else:
			ideal_n = np.sqrt(final_layer.material.n(center_wave) * initial_layer.material.n(center_wave))
		
		wavelength = center_wave * ideal_n
		
		ideal_thick = wavelength / 4.0
		
		return OpticalFilm(OpticalMaterial(ideal_n, 0.0), ideal_thick)

class OpticalStack:
	def __init__(self):
		self.layers = []
		self.solver = thinfilm.ThinFilmSolver()
		
	def _special_layer_air(self):
		material = OpticalMaterial(1.0, 0.0)
		layer = OpticalFilm(material, np.inf)
		
		return layer
	
	def _special_layer_extend(self):
		r"""
		Extend the previous layer into a semi-infinite slab keeping the same index of refraction,
		but with a zero extinction coefficient.
		"""
		
		if len(self.layers) == 0:
			raise Exception("Cannot add a special 'extend' layer to an optical stack that is empty.  There is nothing to extend.")

		last = self.layers[-1]
		
		material = OpticalMaterial(last.n, 0.0)
		
		return OpticalFilm(material, np.inf)
	
	def add_special_layer(self, type):
		method = "_special_layer_" + type
		
		if hasattr(self, method):
			layer = getattr(self, method)()
			self.layers.append(layer)
		else:
			raise Exception("Unknown special layer name given: " + type)
	
	def insert_arc(self, index, center_wave):
		r"""
		Insert a single layer anti-reflection coating (quarter wave type) before the layer given by index i
		"""
		initial = None
		if index != 0:
			initial = self.layers[index-1]
		
		final = self.layers[index]
		
		arc = OpticalFilm.ARC(center_wave, final_layer = final, initial_layer = initial)
		
		self.layers.insert(index, arc)
	
	def make_incoherent(self, index):
		r"""
		Make the layer at the given index incoherent to the thin film solver by adding an index matched layer with
		infinite width.
		"""
				
		self.layers[index].coherent = False
		
	def extract_interface(self, index):
		r"""
		Extract two layers into a separate stack and extend them into semi-infinite half-spaces
		to examine the interface behavior.  The paramater should be the index of the last layer
		to be extracted.  If index is 0, air is assumed to be the first layer, otherwise the previous
		layer is extracted as well.
		"""
		
		new_stack = OpticalStack()
		
		if index >= len(self.layers):
			raise Exception("Invalid index: " + str(index))
					
		if index > 0:
			new_stack.add_layer(self.layers[index-1].index_matched())
		else:
			new_stack.add_layer('air')
			
		new_stack.add_layer(self.layers[index].index_matched())
		
		return new_stack
	
	def add_layer(self, film):
		if isinstance(film, str):
			self.add_special_layer(film)
		else:
			self.layers.append(film)
	
	def prepend_layer(self, film):
		self.add_layer(film)
		
		layer = self.layers.pop()
		self.layers.insert(0, layer)
			
	def build_n_list(self, wavelength):
		def layer_visitor(layer):
			if not layer.coherent:
				return (layer.material.n(wavelength), layer.material.refraction(wavelength))
			else:
				return layer.material.refraction(wavelength)
				
		return solar.utilities.flatten(map(layer_visitor, self.layers))
	
	def build_d_list(self):
		def layer_visitor(layer):
			if not layer.coherent:
				return (np.inf, layer.thickness)
			else:
				return layer.thickness
				
		return solar.utilities.flatten(map(layer_visitor, self.layers))
		
	def validity_range(self):
		minList = map(lambda x: x.material.min_valid, self.layers)
		maxList = map(lambda x: x.material.max_valid, self.layers)
		
		return (max(minList), min(maxList))
		
	def coherent(self):
		for i,layer in enumerate(self.layers):
			if not layer.coherent:
				if not (i == 0 or i == len(self.layers)-1):
					return False
		
		return True
	
	def simulate(self, wave, theta=0.0, pol="s"):
		if self.coherent():
			return solar.fresnel.fresnel_main(pol, self.build_n_list(wave), self.build_d_list(), theta, wave)
		else:
			return solar.fresnel.incoherent_main(pol, self.build_n_list(wave), self.build_d_list(), theta, wave)
		
	def copy(self):
		stack = OpticalStack()
		
		for layer in self.layers:
			stack.add_layer(layer)
			
		return stack
	
	def reverse(self):
		r"""
		Return a new optical stack which is the reverse of the this one, i.e. the last layer is
		first and the first layer is last.
		"""
		
		stack = OpticalStack()
		
		for layer in reversed(self.layers):
			stack.add_layer(layer)
			
		return stack
	
	def remove_layers(self, tag):
		self.layers = [x for x in self.layers if not (x.tag == tag)]
	
	def tag_layers(self, tag, **kw):
		r"""
		Provide a tag to each of the layers in this device so that we can identify things for
		different parts of our stack, i.e. absorption in active device layers vs. parasitic 
		absorption in electrodes etc.
		
		keywords controlling behavior:
		only_untagged: only apply the tag to layers that do not have tags
		only_matching: only apply the tag to layers that have a certain tag
		only_unmatching: only apply the tag to layes that either don't have a tag or have one that doesn't match that given
		
		Only one of these keywords can be given.
		"""
		
		predicate = lambda x: True
		
		if "only_untagged" in kw:
			predicate = lambda x: x.tag == None
		elif "only_matching" in kw:
			predicate = lambda x: x.tag == kw["only_matching"]
		elif "only_unmatching" in kw:
			predicate = lambda x: x.tag != kw["only_unmatching"]
		
		for layer in self.layers:
			if predicate(layer):
				layer.tag = tag
	
	def add_layers(self, stack, **kw):
		r""" 
		if kw is after followed by a defined tag, these layers are added after the last layer
		with the specified tag.  stack should be an OpticalStack
		"""
		tag = None
		index = len(self.layers)
		
		if "after" in kw:
			tag = kw["after"]
			pos = -1
		elif "before" in kw:
			tag = kw["before"]
			pos = 0
			
		if tag is not None:				
			indices = [i for i,x in enumerate(self.layers) if x.tag == tag]
			
			if len(indices) == 0:
				raise Exception("Tried to insert layers relative to a given tag and the tag was not found in the optical stack.")
			
			index = indices[pos] - pos #should give the first index in the list if pos=0 or one + the last index if pos=-1
		
		for layer in stack.layers:
			self.layers.insert(index, layer)
			index += 1
	
	def tagged_layers(self, tag):
		r"""
		return a list of the indices of all layers with a certain tag
		"""
		return Set(map(lambda x: x[0], filter(lambda x: x[1].tag == tag, map(lambda x: (self.layers.index(x), x), self.layers))))
				
	def absorption_in_layers(self, waves):
		r""" return the fraction of light absorbed in each layer of the stack for the given wavelengths.
		The data is returned as a 2D array where the first column is the wavelengths and the subsequent
		columns are the fraction of light absorbed in layer i
		"""
		width = len(self.layers) + 1
		length = len(waves)
		
		output = np.zeros([length, width])
		
		output[:, 0] = waves
		
		for i in xrange(0, length):
			if self.coherent():
				output[i,1:] = solar.fresnel.absorp_in_each_layer(self.simulate(waves[i]))
			else:
				res = solar.utilities.flatten(solar.fresnel.inc_absorp_in_each_layer(self.simulate(waves[i])))
				output[i,1:] = np.array(res)
		
		return output
		
	def absorption_in_layers_fast(self, waves):
		width = len(self.layers) + 1
		length = len(waves)
		
		output = np.zeros([length, width])
		
		output[:, 0] = waves
		
		d = np.array(self.build_d_list(), order='C')
		
		for i in xrange(0, length):
			n_complex = np.array(self.build_n_list(waves[i]), order='C')
			n  = np.real(n_complex)
			k = np.imag(n_complex)
			self.solver.solve(n, k, d, waves[i], 0., thinfilm.SPolarizationState)

			for j in xrange(1, width):
				output[i, j] = self.solver.get_absorption(j-1) #the jth column refers to the j-1th layer
		
		return output
		
	def resolved_absorption(self, wave, points):
		out = points.copy()
		
		#Empty stacks reflects none of the light
		if len(self.layers) == 0:
			out[:] = 0.0
			return out
			
		d_list = self.build_d_list()
		
		for i in xrange(0, points.shape[0]):
			if i == 0:
				dx = points[i]
			else:
				dx = points[i] - points[i-1]
				
			results = self.simulate(wave)
			
			[layer, dist] = solar.fresnel.find_in_structure_with_inf(d_list, points[i])
			
			power = solar.fresnel.position_resolved(layer, dist, results)['absor']*dx
			
			out[i] = solar.reference.powerToFlux(power, wave)
			
		return out
			
	
	def reflection(self, waves):
		r"""
		Return the fraction of incident light reflected off the front surface as a function
		of wavelength for the wavelengths given.
		"""
		
		out = waves.copy()
		
		#Empty stacks reflects none of the light
		if len(self.layers) == 0:
			out[:] = 0.0
			return out
		
		for i in xrange(0, waves.shape[0]):
			out[i] = self.simulate(waves[i])['R']
		
		return out
	
	def absorption(self, waves):
		r"""
		Return the fraction of light absorbed in the optical stack as a function of 
		wavelength for the wavelengths given.  This is calculated as: 1 - (T + R) where
		T and R are the transmitted fraction and reflected fraction, respectively.
		"""
		
		out = waves.copy()
		
		#Empty stacks absorbs none of the light
		if len(self.layers) == 0:
			out[:] = 0.0
			return out
		
		for i in xrange(0, waves.shape[0]):
			res = self.simulate(waves[i])
			
			out[i] = 1.0 - res['T'] - res['R']
		
		return out
	
	def transmission(self, waves, **kw):
		r"""
		Return the fraction of light transmitted through the optical stack as a function of
		wavelength for the wavelengths given.  If you pass the keyword extend=number, transmission
		will be set to number outside of the range of validity of the optical coefficients used
		to define the stack.  Otherwise an exception will be raised on wavelengths outside of the
		validity domain.
		"""
		out = waves.copy()
		
		#Empty stacks transmit all the light
		if len(self.layers) == 0:
			out[:] = 1.0
			return out
		
		domain = self.validity_range()
		extend = False
		if "extend" in kw:
			extend = True
			fill = kw["extend"]
		
		for i in xrange(0, waves.shape[0]):
			if extend and (waves[i] < domain[0] or waves[i] > domain[1]):
				out[i] = fill
			else:
				out[i] = self.simulate(waves[i])['T']
		
		return out
		
	def critical_angles(self, waves):
		r"""
		returns the critical angle for all interfaces in this optical stack.
		"""
		
		angles = np.zeros([waves.shape[0], len(self.layers)])
		angles[:,0] = waves
		
		#speed up, avoid recomputing n
		n1 = self.layers[0].material.n(waves)
		
		for i in xrange(0, len(self.layers)-1):
			n2 = self.layers[i+1].material.n(waves)
			n2[n2>n1] = 0.0
			
			angles[:,i+1] = np.arcsin(n2/n1)
			angles[n2 == 0.0, i+1] = np.pi/2.0 #for cases in which total internal reflection is impossible return theta = 90 degrees
			
			n1 = n2
		
		return angles
		
	def resolved_isotropic_coupling(self, wave):
		r"""
		returns the isotropic coupling parameters for the interface described by this OpticalStack().
		
		returned dictionary contains the average reflectance, transmittance and absorption
		"""
		num = 90
		angles = np.linspace(0, 3.141592635/2., num)
		
		results = np.ndarray([num, 7])
		results[:,0] = angles
		
		for i in xrange(0, num):
			dat = self.simulate(wave, angles[i], "s")
			results[i, 1] = dat['T']
			results[i, 2] = dat['R']
			
			dat = self.simulate(wave, angles[i], "p")
			results[i, 4] = dat['T']
			results[i, 5] = dat['R']
			
		results[:,3] = 1.0 - results[:,1] - results[:,2]
		results[:,6] = 1.0 - results[:,4] - results[:,5]
		
		return results
		
	def isotropic_coupling(self, wave):
		dat = self.resolved_isotropic_coupling(wave)
		
		T = np.sum(dat[:,1]+dat[:,4])/(2.0 * dat.shape[0])
		R = np.sum(dat[:,2]+dat[:,5])/(2.0 * dat.shape[0])
		A = 1.0 - T - R
		
		return {"T": T, "R": R, "A": A}