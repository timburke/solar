#quantum_efficiency.py
import numpy as np
import math
from scipy import integrate, interpolate
import solar.reference
import solar.utilities
import os.path
import ConfigParser

class QuantumEfficiency(np.ndarray):
	def __new__(cls, *args, **kwargs):
		return np.ndarray.__new__(cls, *args, **kwargs)
	
	def __array_finalize__(self, obj):
		pass
		
	@staticmethod
	def Load(filename):
		return np.genfromtxt(filename, skip_header=1, delimiter=",").view(QuantumEfficiency)
		
	@staticmethod
	#cutoff should be in eV
	#this is useful for specifying ideal solar cell behavior
	def IdealQE(cutoff):
		cutoffWave = solar.reference.ev_to_nm(cutoff)
				
		arr = np.zeros([200, 2])
		arr[1:199,0] = np.linspace(0.0,cutoffWave, 198)
		
		arr[0,0] = 0.0
		arr[199,0] = cutoffWave+1.0
		
		arr[:,1] = 1.0
		arr[199,1] = 0.0
		
		qe = arr.view(IdealQuantumEfficiency)
		qe.cutoff = cutoffWave
		
		return qe
	
	@staticmethod
	def ZeroQE():
		arr = np.zeros([2,2])
		arr[0,0] = 100.
		arr[1,0] = 4000.
		
		return arr.view(QuantumEfficiency)
	
	def max(self):
		max = -1.0
		
		for i in xrange(0,self.shape[0]):
			if self[i,1] > max:
				found = self[i,0]
				max = self[i,1]
		
		return found
	
	#input should be a spectrumlike object
	def response(self, input):
		func = interpolate.interp1d(self[:,0], self[:,1],bounds_error=False, fill_value=0.0) #pad out with 0s
		
		processed = np.zeros_like(input)
		processed[:,0] = input[:,0]
		for i in xrange(0,input.shape[0]):
			processed[i,1] = func(input[i,0])*solar.reference.powerToFlux(input[i,1], input[i,0])
		
		return processed
		

#Optimized subclass for unit QEs
#Should not be instantiate separately, but rather comes from the IdealQE static function in QuantumEfficiency
class IdealQuantumEfficiency(QuantumEfficiency):
	#optimized response function
	def response(self, input):
		processed = input[input[:,0] <= self.cutoff]
		
		return solar.reference.vToFlux(processed)