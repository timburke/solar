#solution.py
import numpy as np
import math
from scipy import integrate, interpolate
import solar.reference
import solar.utilities
import os.path
import ConfigParser

#A class for calculating the optical absorption of a species in transparent solution with
#a given concentration and absorption profile
#data is stored as a normalized absorption spectrum with the molar extinction coeff stored separately

#Internal Units for the various quantities:
#molar extinction coeff: molar^-1 cm^-1
class OpticalSolution(np.ndarray):
	def __new__(cls, *args, **kwargs):
		return np.ndarray.__new__(cls, *args, **kwargs)
	
	def __array_finalize__(self, obj):
		pass
	
	#Normalize the absorption so that it returns 1.0 at the given wavelength
	def __normalizeToWavelength(self, wave):
		func = interpolate.interp1d(self[:,0], self[:,1],kind='cubic')
		value = func(wave)
		
		self[:,1] /= value
		
		return value
	
	def __normalizeToMaximum(self):
		max = np.max(self[:,1])
		
		self[:,1] /= max
		
		return max
	
	@staticmethod
	def LoadAbsorption(filename):
		return np.genfromtxt(filename, skip_header=1, delimiter=",").view(OpticalSolution)
		
	@staticmethod
	def LoadAbsorptionAsMolarExtinction(filename):
		soln = OpticalSolution.LoadAbsorption(filename)
		
		extinctionCoeff = soln.__normalizeToMaximum()
		
		soln.extinction = extinctionCoeff
		
		return soln
	
	@staticmethod
	def LoadReference(identifier):
		directory = '/Users/timburke/Documents/Graduate School/Research/Upconversion/Scripts/data/upconverters'
		
		filename = identifier + ".prm"
				
		config = ConfigParser.ConfigParser()
		success = config.read(os.path.join(directory, filename))
		if len(success) != 1:
			raise Exception("Could not find the file named: " + filename)
			
		type = config.get("Information", "Spectrum Type")
		
		if type == "Molar Extinction":
			soln = OpticalSolution.LoadAbsorptionAsMolarExtinction(os.path.join(directory, identifier+".csv"))
		else:
			raise Exception("Unknown spectrum type in reference file: " + type)
			
		soln.concentration = 0.0
		soln.pathLength = 0.0
		soln.bandStart = config.getfloat("Upconversion", "Band Start")
		soln.bandEnd = config.getfloat("Upconversion", "Band End")
		soln.peak = config.getfloat("Upconversion", "Peak")
		
		return soln
	
	def usefulAbsorption(self):
		abs = self.absorptionCurve()
		
		return utilities.definite_integral(abs, self.bandStart, self.bandEnd)
		
	def absorptionCurve(self):
		abs = self.copy()
		
		for i in xrange(0, self.shape[0]):
			absorptionCoeff = self[i,1]*self.extinction*self.concentration*self.pathLength
			absorption = 1.0 - 10**(-1.0 * absorptionCoeff)
			abs[i,1] = absorption
		
		return abs
	
	def absorption(self, input):
		func = interpolate.interp1d(self[:,0], self[:,1],bounds_error=False, fill_value=0.0) #pad out with 0s
		
		processed = np.zeros_like(input)
		processed[:,0] = input[:,0]
		for i in xrange(0,input.shape[0]):
			absorptionCoeff = func(input[i,0])*self.extinction*self.concentration*self.pathLength
			absorption = 1.0 - 10**(-1.0 * absorptionCoeff)
			processed[i,1] = absorption*input[i,1]
		
		return processed
		
	def band_absorption(self, wavelengths):
		r"""
		Return the optical absorption of this solution ats wavelengths specified.  For any wavelength outside the
		absorption band specified in the solution file, 0.0 is returned.  For wavelengths not explicitly
		given in the solution file used to build this OpticalSolution, linear interpolation is used.
		"""
		
		func = interpolate.interp1d(self[:,0], self[:,1],bounds_error=False, fill_value=0.0) #pad out with 0s
		
		processed = np.zeros_like(wavelengths)
		for i in xrange(0,len(wavelengths)):
			if wavelengths[i] < self.bandStart or wavelengths[i] > self.bandEnd:
				processed[i] = 0.0
			else:
				absorptionCoeff = func(wavelengths[i])*self.extinction*self.concentration*self.pathLength
				absorption = 1.0 - 10**(-1.0 * absorptionCoeff)
				processed[i] = absorption
		
		return processed
	
	def band_absorption_at(self, wave):
		r"""
		Return the optical absorption of this solution at wavelength wave.  If wave is outside the
		absorption band specified in the solution file, 0.0 is returned.  For wavelengths not explicitly
		given in the solution file used to build this OpticalSolution, linear interpolation is used.
		"""
		
		if wavelengths[i] < self.bandStart or wavelengths[i] > self.bandEnd:
			return 0.0
		
		func = interpolate.interp1d(self[:,0], self[:,1],bounds_error=False, fill_value=0.0) #pad out with 0s
		
		absorptionCoeff = func(wave)*self.extinction*self.concentration*self.pathLength
		absorption = 1.0 - 10**(-1.0 * absorptionCoeff)
		
		return absorption