#spectrum.py

import numpy as np
import math
from scipy import integrate, interpolate
import solar.reference
import solar.utilities
import os.path
import ConfigParser

class Spectrum (np.ndarray):
	#Need nonstandard subclassing methods to support numpy template construction and viewcasting
	def __new__(cls, *args, **kwargs):
		return np.ndarray.__new__(cls, *args, **kwargs)
	
	def __array_finalize__(self, obj):
		pass
	
	@staticmethod
	def AM15G():
		return solar.reference.am15_tilt().view(Spectrum)
	
	def toAbsolute(self):
		absLight = self.copy()
		
		#first point (first sample to half the distance to the second
		absLight[0,1] = self[0,1]*(self[1,0]-self[0,0])/2.0
		for i in xrange(1,self.shape[0]-1):
			forDist = (self[i+1,0] - self[i,0])/2.0
			backDist = (self[i,0] - self[i-1,0])/2.0
			absLight[i, 1] = self[i,1]*(forDist + backDist)
		
		absLight[self.shape[0]-1, 1] = self[self.shape[0]-1,1]*(self[self.shape[0]-1,0]-self[self.shape[0]-2,0])/2.0
		
		return absLight
	
	#numpoints must be at least 2
	#resample this spectrum trying to preserve as much detail as possible
	def downsample(self, numpoints):
		if numpoints < 2:
			return None
		
		#if we're already small enough, don't do anything
		if self.shape[0] <= numpoints:
			return self
		
		#Find the cumulative integral of abs(grad spectrum)*dx
		cumchange = 0.0
		for i in xrange(1, self.shape[0]):
			cumchange += math.fabs(self[i,1]-self[i-1,1])
		
		spacing = cumchange/(numpoints-1.0)
		
		#Now iterate over the array and place a point everytime the value changes by more than spacing
		
		resampled = np.zeros([numpoints,2])
		func = interpolate.interp1d(self[:,0], self[:,1])
		
		#initialize the first and last points
		resampled[0,0] = self[0,0]
		resampled[0,1] = self[0,1]
		resampled[numpoints-1,0] = self[self.shape[0]-1,0]
		resampled[numpoints-1,1] = self[self.shape[0]-1,1]
		
		#current point in the resampled array
		j = 1
		lastchange = 0.0
		
		for i in xrange(1, self.shape[0]-1):
			lastchange += math.fabs(self[i,1]-self[i-1,1])
			lastLoc = self[i-1,0]
			while lastchange >= spacing:
				loc = lastLoc + (spacing/lastchange)*(self[i,0]-self[i-1,0])
			
				resampled[j, 0] = loc
				resampled[j, 1] = func(loc)
				
				#this is important, we need this so that we always get the right number of points
				lastchange -= spacing #arbitrarily assume we got it exactly right and it was spacing apart so that we get the right number of points (exactly numpoints)
				j += 1
				lastLoc = loc
				assert(j < numpoints)
		
		return resampled.view(Spectrum)
		
	#convenience calculation functions
	#Calculate the power contained between wavelengths waveMin and
	#waveMax in the same units as the spectrum.
	#using trapezoidal integration
	def power(self, waveMin, waveMax):
		length = self.shape[0]
		
		counting = False
		sum = 0.0
		
		func = interpolate.interp1d(self[:,0], self[:,1])
		
		for i in xrange(0,length):
			#if we're already in the range of [waveMin, waveMax]
			if counting is True:
				#if we're below wavemax, add up a whole trapezoid contribution
				if self[i,0] <= waveMax:
					if i>0:
						sum += (self[i,1]+self[i-1,1])/2.0*(self[i,0]-self[i-1,0])
				else:
					#this is the first one above wavemax, so add up a trapezoid contribution up to waveMax
					#we know i>0 because we had to set counting true by passing at least one row
					sum += (self[i-1,1]+func(waveMax))/2.0*(waveMax - self[i-1,0])
					break
		
			#if this is the first entry above the cutoff wavelength
			if counting is False and self[i,0] >= waveMin:
				if self[i,0] > waveMax:
					#The entire limit is contained in this one entry
					#if it's below the first entry, we say 0
					#otherwise it's the value of the spectrum*(wavemax - wavemin)
					if i == 0:
						return 0.0
					else:
						return (func(waveMax)+func(waveMin))/2.0*(waveMax-waveMin)
				
				#otherwise we need to add a partial contribution from waveMin to this wavelength
				sum += self[i,1]*(self[i,0]-waveMin)
				
				counting = True
		
		return sum
		
	def totalpower(self):
		return integrate.trapz(self[:,1], self[:,0])
		
	#transform this spectrum according to a response function f which must be evaluable at each point
	#of the spectrum
	def response(self, f):
		arr = self.copy()
		
		for i in xrange(0, arr.shape[0]):
			arr[i,1] *= f(arr[i,0])
		
		return arr