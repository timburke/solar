#Process result arrays by summing over them in certain ways

import numpy as np
import numpy.ma as ma

def average_for_fate(arr, fate):
	"""
	Average a result only for trials that have a certain fate.
	The form of the input array should be:
	[var 1, ..., var N, trials, results]
	results must be length at least 2 and the first component must be Fate
	"""
	
	orig_shape = list(arr.shape)
	orig_shape[-1] = 1
	
	mask = np.logical_or(arr[...,0] <= fate-0.1, arr[...,0] >= fate+0.1)
	mask = mask.reshape(orig_shape)
	mask = np.repeat(mask, arr.shape[-1], len(arr.shape)-1)
	
	input = ma.array(arr, mask=mask, fill_value=0.0)	
	
	out = ma.sum(input[...,1:],-2)
	
	shape = list(out.shape)
	shape[-1] = 1

	
	out /= ma.sum(input[...,0],-1).reshape(shape)
	
	return out.data

def fate_fraction(arr, fate):
	"""
	Return the fraction of trials that ended in the given fate.
	The array must have the results contained in a separate dimension, even if 
	there is only one
	"""
	
	orig_shape = list(arr.shape)
	orig_shape[-1] = 1
	
	mask = np.logical_or(arr[...,0] <= fate-0.1, arr[...,0] >= fate+0.1)
	mask = mask.reshape(orig_shape)
	
	input = ma.array(arr[...,0], mask=mask, fill_value=0.0)	
	
	return input.count(-1)/float(arr.shape[-2])

def simulated_ta(arr):
	"""
	Return simulated transient absorption transients
	for all the kmc runs contained in array.  Array should
	have only two results: Fate, ElapsedTime in that order.
	"""

	num_trials = arr.shape[-2]

	new_shape = list(arr.shape)
	new_shape[-1] = 2
	new_shape[-2] = num_trials+1

	times = arr[...,1] #results should be fate, elapsed time
	times = np.sort(times, axis=-1) #sort from low to high

	ta = np.zeros(new_shape)
	ta[...,0,0] = 0.0
	ta[...,0,1] = num_trials
	ta[...,1:,0] = times
	ta[...,1:,1] = range(num_trials-1,-1,-1)

	return ta

def simulated_pl(arr):
	"""
	Return simulated photoluminescence transients for the run in arr.  
	Array should have only two results: Fate, ElapsedTime in that order.
	"""

	fate = 1.0 #look only at recombined geminate pairs

	orig_shape = list(arr.shape)
	orig_shape[-1] = 1
	
	mask = np.logical_or(arr[...,0] <= fate-0.1, arr[...,0] >= fate+0.1)
	mask = mask.reshape(orig_shape)

	values = input = ma.array(arr[...,1], mask=mask, fill_value=0.0).compressed()

	bins = int(np.sqrt(len(values)))

	cnts,edges = np.histogram(values, bins=bins)

	mid = (edges[0:-1] + edges[1:])/2.0

	hist = np.ndarray([len(cnts), 2])
	hist[:,0] = mid
	hist[:,1] = cnts

	return hist
