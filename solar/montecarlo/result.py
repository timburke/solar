#result.py
#A numpy array subclass encapsulating a result tensor obtained from an MC run

import numpy as np
import itertools

class ResultTensor(np.ndarray):
    def __new__(cls, *args, **kwargs):
        return np.ndarray.__new__(cls, *args, **kwargs)

    def __array_finalize__(self, obj):
        pass

    def set_results(self, results):
        """
        Define the results that are contained in this tensor.
        results should be a list of tuples with the name and size
        """

        curr_size = 0
        self.results = {}

        for result in results:
            self.results[result[0]] = (result[1], curr_size)   # store the size and offset

            curr_size += result[1]

        self.result_total_length = curr_size

    def set_variable(self, vars):
        self.variables = {}
        for val, key in vars:
            self.variables[key] = np.array(val)

    def set_order(self, order):
        self.order = [x for x in order]

    def set_fixed(self, vars):
        self.fixed = {}
        for val, key in vars:
            self.fixed[key] = np.array(val)

    def result_offset(self, result):
        return self.results[result][1]

    def result_size(self, result):
        return self.results[result][0]

    def result_slice(self, result):
        return np.Slice(self.result_offset(result), self.result_offset(result)+self.result_size(result))

    def _dimension_size(self, dim):
        vals = dim[0]
        if isinstance(vals, int):
            return vals

        return len(vals)

    def _iterate_all(self):
        vars = [xrange(0, self._dimension_size(x)) for x in self.order]

        return itertools.product(*vars)

    def _map_variables(self, comb):
        map = {}

        for i in xrange(0, len(self.order)):
            v = self.order[i][1]
            map[v] = self.order[i][0]

        return map

    def reduce(self, func, result_size):
        #make sure the last dimension is the results and that old_size does not include that dimension
        if self.size == 1:
            new_size = list(self.shape)
            s = list(self.shape)
            s.append(1)
            arr = self.reshape(s)
        else:
            new_size = list(self.shape[:-1])

        combs = self._iterate_all()

        new_size.appen(result_size)
        result = np.ndarray(new_size)

        for comb in combs:
            #function to map comb to values for each of the variables
            #add ability to choose which results to use
            #apply function over each combination

            map = self._map_variables(comb)
            index = list(comb)
            index.append(slice())