#kmc_env.py
#KMCEnvironment
#A class encapsulating a fully or partially configured MonteCarlo Simulation
#instance, prepared from a specified config file and configured using a supplied
#configuration routine

import os.path
from kmc_config import MonteCarloConfigurator
import kineticmc as mc
import imp
import numpy as np

class KMCEnvironment:
	def __init__(self, directory):
		self.dir = directory
		self.configfile = os.path.join(self.dir, "config.json")
		self.configmodule = os.path.join(self.dir, "userconfig.py")
		self.config = MonteCarloConfigurator(self.configfile)
		
		self.set_params = {}
		self.userconfig = None
		
		self._parse_params()
		
	def _parse_params(self):
		self.fixed = 	dict((k, val[1]) for k, val in self.config.get_variables().iteritems() if val[0] == False)
		self.variable = dict((k, val[1]) for k, val in self.config.get_variables().iteritems() if val[0] == True)
	
	def _needed_params(self):
		return [k for k,val in self.variable.iteritems() if k not in self.set_params]
	
	def describe(self):
		print "KMC Environment"
		print "Directory:", os.path.basename(self.dir)
		print "Title:", self.config.get_title()
		print "\nFixed Parameters"
		
		for k,v in self.fixed.iteritems():
			print k + ": " + str(v)
		
		print "\nVariable Parameters"
		for k,v in self.variable.iteritems():
			print k + ": " + str(v)
			
		print "\nRequired Paramters"
		for k in self._needed_params():
			print k
		
	def set(self, key, val):
		self.set_params[key] = val
		
	def clear(self, key):
		try:
			del self.set_params[key]
		except KeyError:
			pass #Don't care if it's not there
	
	def ready(self):
		"""
		Return True if all the needed parameters to configure this MC Instance have been filled with values
		"""
		
		return len(self._needed_params()) == 0
	
	def _import_userconfig(self):
		#if we haven't already,import the userconfig script for this environment and give it the parameters so it can
		#give us back a SimulationParameters object
		if self.userconfig is not None:
			return
				
		self.userconfig = imp.load_source("userconfig", os.path.join(self.dir, "userconfig.py"))		
	
	def _build_instance(self):
		c = mc.SimulationController()
		
		if not self.ready():
			raise ValueError("Not all variable parameters have been set.  Cannot build Monte Carlo Instance")
		
		params = dict(self.fixed.items() + self.set_params.items())
		
		#make sure userconfig is imported
		self._import_userconfig()
		
		(pobj, needed) = self.userconfig.initialize_parameters(params)
		for elem in needed:
			print "Needed parameter not set:", elem
		
		c.create_calc_sim(pobj)
				
		return c
	
	#
	# Analysis Functions (ask questions to our fully configured MC Simulation Instance
	#
	
	def hopping_rates(self, elec_pos, hole_pos, field):
		"""
		Calculate the hopping rates for the electron and hole given their position
		"""
		
		c = self._build_instance()
		c.set_calc_field(field)
		c.set_calc_carrier_pos(elec_pos, hole_pos)
		
		rates = np.ndarray([3,2])
		
		rates = c.calc_hopping_rates()
		
		return rates