#manager.py
#RunManager class
#finds all the monte carlo runs in a given directory, automatically extracting their
#parameters, names and descriptions and allowing the user to easily create a MonteCarloRun
#object for any of them to analyze their results.

import os
import os.path
import json
import analyze
import kmc_config as kmcc
import kmc_env as mce
from solarsettings import SolarSettings
from sets import Set
import solar.utilities

class RunManager (solar.utilities.SSHUtility):
	def __init__(self, dir=None, **kw):
		"""
		Create a RunManager object.  The optional argument dir specifies the directory 
		that should be searched.  If not given, it defaults to kineticmc/runs
		
		optional keywords:
		silent - do not print directories that are ignored for whatever reason
		"""
		
		#Pull settings from stored location
		with SolarSettings() as s:
			if dir is None:
				dir = s.get('rundir')
		
		self.dir = dir
		self._load_runs(**kw)
	
	def sync_runs(self, **kw):
		"""
		Search a remote server for runs not already stored locally and download them.  
		Does not download data files if there are any, just downloads the necessary 
		config files and creates the local directory structure so that list_runs can find 
		these runs and the other montecarlo tools can manage them.
		"""
		
		remote_files = self._exec_remote("ls -l /mnt/glusterfs/timburke/batch | sed 1d | grep -E -o '[-_a-zA-Z0-9]+$'", quiet=True)
		remote_files = remote_files.rstrip().split("\n")
		local_files = os.listdir(self.dir)
		
		new_runs = Set(remote_files).difference(Set(local_files))
		
		#Now download each new run from the cloud
		for run in new_runs:
			if "quiet" not in kw:
				print "Syncing run %s" % run
			
			#FIXME: Hardcoded list of the local and remote file names that we need to sync
			files = [('config.json', 'config.json'), ('driver.py','batch_payload.py'), ('log.conf', 'log.conf'), ('userconfig.py', 'userconfig.py')]
			
			local_dir = os.path.join(self.dir, run)
			os.mkdir(local_dir)
			
			for (local, remote) in files:
				local = os.path.join(local_dir, local)
				remote = os.path.join('/mnt/glusterfs/timburke/batch', run, remote)
				self._get(local, remote)
		
		self._load_runs()
		
	def list_runs(self, **kw):
		r"""
		List the runs that have been found
		optional keywords:
		long - show descriptions
		"""
		
		i = 1
		
		for run in self.runs:
			print "%d. %s - %s" % (i, run["shortname"], run["title"])
			
			if "long" in kw and kw["long"] and "description" in run:
				print run["description"]
				
			i = i+1
	
	def get_run(self, index):
		r"""
		get the indicated run from its index or shortname 
		(n.b. indices start at 1).
		"""
		
		run = self._find_run(index)
					
		return analyze.MonteCarloRun(	os.path.join(run["dir"], "data"),
										config=os.path.join(run["dir"], "config.json"),
										format=run["format"],
										iter_var=run["iter_var"])
	
	def get_environment(self, index):
		"""
		Return an object encapsulating the computational environment used in a given
		Monte Carlo simulation run, allowing questions like hopping probabilities in certain 
		circumstances to be	easily answered.
		"""
		
		run = self._find_run(index)
		
		return mce.KMCEnvironment(run["dir"])
		
	def _find_run(self, index):
		try:
			if isinstance(index, str):
				#search by shortname
				i = map(lambda x: x["shortname"], self.runs).index(index)
				run = self.runs[i]
			else:
				run = self.runs[index-1]
			
			return run
		
		except (IndexError, ValueError):
			print "Could not find specified run %s" % str(index)
			
	def _load_runs(self, **kw):
		self.runs = []
		entries = os.listdir(self.dir)
		
		silent = False
		if "silent" in kw:
			silent = kw["silent"]
		
		dirs = filter(lambda x: os.path.isdir(os.path.join(self.dir, x)), entries)
		
		#Now iterate over the directories and pull the relevant information from them
		for curr in dirs:
			path = os.path.join(self.dir, curr)
			config_path = os.path.join(path, "config.json")
			
			#try to load the config file to extract the information we need
			if not os.path.isfile(config_path):
				if not silent:
					print "Ignoring directory %s because it has no config.json file" % curr
				continue
				
			
			entry = {}
			entry["dir"] = path
			entry["shortname"] = curr
			
			try:
				config = kmcc.MonteCarloConfigurator(config_path)
				entry["format"] = config.get_format()
				entry["iter_var"] = config.get_iteration_variable()[0]
				entry["title"] = config.get_title()
				entry["num_files"] = config.count_expected_files()
			except (KeyError, ValueError) as e:
				if not silent:
					print "Ignoring directory %s because a required config variable was not found" % curr
				continue
			
			desc_path = os.path.join(path, "description.txt")
			
			if os.path.isfile(desc_path):
				with open(desc_path, "r") as f:
					entry["description"] = "".join(f.readlines())
			
			self.runs.append(entry)