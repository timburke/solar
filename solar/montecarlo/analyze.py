#analyze.py

import json
import string
import itertools
import os.path
import glob
import numpy as np
import numbers
import gzip
import hashlib
import re
import datetime
import subprocess
from subprocess import call, check_output
import paramiko
from getpass import getpass
import uuid
import math
import shutil
from .. import utilities
from solarsettings import SolarSettings

import result
import kmc_config

def is_sequence(arg):
    return (not hasattr(arg, "strip") and
        hasattr(arg, "__getitem__") or
        hasattr(arg, "__iter__"))

class MonteCarloRun (utilities.SSHUtility):
    def __init__(self, dir, **kw):
        """
        Create a MonteCarloRun object encapsulating the data contained in the supplied directory.
        The keyword arguments control how the files in dir are interpreted.  Some are required.

        config: a path to the config json file that was used to control this run.  The majority of
        the of simulation parameters are loaded automatically from config

        results: The ResultFunctor objects that are contained in each file so that we can interpret it. 
            This could potentially be extracted from config in the future as well.

        vars: A dictionary mapping variable names to arrays.  This is the heart of the simulation.
            It contains what is changed in each run and is combined with format to produce file names
        """

        self.data_dir = dir

        if "config" not in kw:
            print "Not using a config file is not implemented yet."
            raise ValueError()

        self.configfile = kw["config"]

        self._reset()

    def _reset(self):
        """
        Reload configuration from directory, allowing for updates 
        """

        self.vars = {}
        self.fixed = {}
        self.results = []
        self.file_map = []
        self.shortname = os.path.basename(os.path.normpath(os.path.dirname(self.configfile)))
        self.local_dir = os.path.normpath(os.path.dirname(self.configfile))

        self._parse_config(self.configfile)

        #sort all the variables for easy searching later
        for k, v in self.vars.iteritems():
            v.sort()

        #make sure we have variables for every place in our format string
        f = string.Formatter()
        p = f.parse(self.format_str)

        names = [x[1] for x in p] #extract just the placeholder names

        for name in names:
            if name not in self.vars:
                print "Placeholder found with no matching variable: %s" % name
                raise ValueError()

            if name == self.iter_var:
                print "Format string cannot contain the iteration variable (iter_var=%s)" % kw["iter_var"]

        #Now figure out what files we should have and add them to our file map
        self._parse_files(self.data_dir)

    def calc_result_dimension(self, r):
        width = 0
        for (n, d) in self.results:
            if n in r:
                width += d

        if width > 1:
            return width
        elif width == 0:
            raise ValueError("No results were selected.")
        else:
            return 0

    def _get_result_offset(self, r):
        offset = 0

        for result in self.results:
            if r == result[0]:
                return offset

            offset += result[1]

        raise ValueError("Result %s was not found" % r)

    def _load_file(self, filename):
        """
        Load a numpy array from the file with ungzipping if required
        """

        (base, ext) = os.path.splitext(filename)
        if ext == ".gz":
            file = gzip.open(filename, "rb")
        else:
            file = open(filename, "rb")

        arr = np.load(file)

        file.close()

        return arr

    def _fill_array(self, fixed, variable, curr_index, out, results, fold):
        #recurse until we fill out all the wildcard variables
        if len(variable) != 0:
            my_var = variable[0]  # this is a tuple([values], name)
            new_vars = variable[1:]

            for i in xrange(0, len(my_var[0])):
                new_fixed = list(fixed)
                new_fixed.append((my_var[0][i], my_var[1]))

                new_index = list(curr_index)
                new_index.append(i)

                self._fill_array(new_fixed, new_vars, new_index, out, results, fold)

        else:
            #Basecase, all the indices have values, load in the files for that index and store them in the array possibly folding

            files = self._get_files(fixed)

            out_i = 0

            w = self.calc_result_dimension(results)
            step = self._result_width()

            file_no = 0

            fixed_dict = {key: value for (value, key) in fixed}

            for file in files:
                file_no += 1

                arr = self._load_file(file)
                arr_len = arr.shape[1]

                if fold is not None:
                    if fold[1] == "incremental":
                        next_res = np.ndarray([len(results)])

                        for row in xrange(0, len(self.vars[self.iter_var])):
                            index = list(curr_index)
                            index.append(slice(row, row+1))  # must use a slice because with a basic int, numpy gets confused and reverses the index order somehow

                            for col in xrange(0, arr_len):
                                self._build_result_vector(arr, results, step, w, row, col, out=next_res)

                                out[index] = fold[0](next_res, out[index], self.trials)

                else:
                    #We just fill in all the trials in one go with no folding
                    #If we want all the iter_values then loop over them, otherwise just get the one
                    if self.iter_var in fixed_dict:
                        i = self._find_nearest(self.iter_var, fixed_dict[self.iter_var])

                        index = list(curr_index)
                        index.append(slice(out_i, out_i+arr_len, 1))

                        if w == 0:
                            offset = self._get_result_offset(results[0])
                            out[index] = arr[step*i + offset]
                        else:
                            for j in xrange(0, len(results)):
                                offset = self._get_result_offset(results[j])
                                final_index = list(index)
                                final_index.append(j)

                                out[final_index] = arr[step*i + offset]

                    else:
                        for i in xrange(0, len(self.vars[self.iter_var])):
                            index = list(curr_index)
                            index.append(i)
                            index.append(slice(out_i, out_i+arr_len, 1))

                            if w == 0:
                                offset = self._get_result_offset(results[0])
                                out[index] = arr[step*i + offset]
                            else:
                                for j in xrange(0, len(results)):
                                    offset = self._get_result_offset(results[j])
                                    final_index = list(index)
                                    final_index.append(j)

                                    out[final_index] = arr[step*i + offset]

                    out_i += arr_len

    def _build_result_vector(self, arr, results, step, width, row, col, out):
        if width == 0:
            offset = self._get_result_offset(results[0])
            out[0] = arr[step*row + offset, col]
        else:
            for j in xrange(0, len(results)):
                offset = self._get_result_offset(results[j])

                out[j] = arr[step*row + offset, col]

    def get_results(self, keys, results, fold_t=None, quiet=False):
        """
        Get results corresponding to the values of keys given.  If fold_t is not None, it is used
        to process and combine the results from each trial into a single value.  It should be a tuple
        with signature: (function, type), where type can be "iterative", "at once".

        When type = "at once": fun should take 1 parameter which will be the values for all the trials as
        a 1D array.  It will be called once per parameter combination
        """

        wildcards = 0

        for key, value in keys.iteritems():
            if value == "*":
                wildcards += 1

        dim = wildcards
        if fold_t is None:
            dim += 1  # #we have an extra dimension for the iteration variable (usually the field) unless we're folding that

        dim += 1  # #we need the iter_var as well

        res_dim = self.calc_result_dimension(results)

        #print "We need a %d dimensional result tensor to store this." % (dim+res_dim)
        #print "%d trials" % self.trials

        #build up a shape array
        shape = []
        var_params = []
        fixed_params = []

        for key, value in keys.iteritems():
            if value == "*":
                #ignore variable iter_var since that's implied
                if key != self.iter_var:
                    shape.append(len(self.vars[key]))
                    var_params.append((self.vars[key], key))
            else:
                fixed_params.append((keys[key], key))

        result_order = [x for x in var_params]
        #See if we want all values of the iteration parameter or just some
        if self.iter_var not in map(lambda x: x[1], fixed_params):
            shape.append(len(self.vars[self.iter_var]))  # we need one dimension for the iter_var
            result_order.append((self.vars[self.iter_var], self.iter_var))  # add the iter var to the list so we know what the dim

        if fold_t is None:
            shape.append(self.trials)  # # do we need an extra dim for all the trials
            result_order.append((self.trials, 'Trials'))
            if res_dim > 0:
                result_order.append((res_dim, 'Results'))
                shape.append(res_dim)  # # do we need extra dimension(s) for the multiple results

        if not quiet:
            print "Resulting array shape:", shape
            print "Variable Order:",  map(lambda x: x[1], result_order)

        res_arr = np.ndarray(shape)

        #Build all the combinations of variables that we want
        self._fill_array(fixed_params, var_params, [], res_arr, results, fold_t)

        #Wrap the result in a ResultTensor object
        r_tens = res_arr.view(result.ResultTensor)
        r_tens.set_results([(r, self.calc_result_dimension(r)) for r in results])
        r_tens.set_variable(var_params)
        r_tens.set_order(result_order)
        r_tens.set_fixed(fixed_params)

        return r_tens

    #Private functions used for loading up all the data
    def _result_width(self):
        return reduce(lambda x, y: x+y, map(lambda x: x[1], self.results), 0)

    def _get_files(self, vars):
        """
        Given a list of tuples of variable name value pairs, return the files that contain
        the relevant information on those variables.
        """
        m = {}

        for v, name in vars:
            if name != self.iter_var:
                m[name] = self.vars[name][self._find_nearest(name, v)]  # make sure we have an identical version to what was loaded from config

        for key, files in self.file_map:
            if key == m:
                return files

        print "Could not find files for key", m
        raise ValueError()

    def _find_nearest(self, varname, value):
        """
        Given a potentially floating point value, find the nearest value in our list of known variable values
        and return its index.
        """

        #Have to handle iteration variable specially    
        if varname == self.iter_var:
            varlist = self.iter_values
        else:
            varlist = self.vars[varname]

        if isinstance(varlist[0], numbers.Number):
            found = min(range(len(varlist)), key=lambda i: abs(varlist[i]-value))

            if value > 0.001 and abs(varlist[found] - value)/value > 0.001:
                print "Could not find good match for value in list (found %f, looked for %f)" % (varlist[found], value)
                raise ValueError()

            return found
        else:
            #Nonnumeric types must match exactly
            return varlist.index(value)

    def _parse_files(self, dir):
        #create a list of variables each with its name so we can cartesian product them
        mapped_vars = []

        for key, value in self.vars.iteritems():
            if key != self.iter_var:
                mapped_vars.append([(x, key) for x in value])

        for element in itertools.product(*mapped_vars):
            fmt_vars = {}
            for e in element:
                fmt_vars[e[1]] = e[0]

            files = self._find_files(dir, fmt_vars)
            self.file_map.append((fmt_vars, files))

    def _find_files(self, dir, vars):
        """
        Find all files in the given directory described by format_string when substituted with vars
        """

        filestart = self.format_str.format(**vars)

        filename = filestart + "-*" #allow for MPI produced -n.np.gz names      
        path = os.path.join(dir, filename)
        mpis = set(glob.glob(path))

        filename = filestart + ".*" #allow for non MPI produced name {prefix}.np.gz for example
        path = os.path.join(dir, filename)
        others = set(glob.glob(path))

        return list (others | mpis)

    def _parse_config(self, filename):
        with open(filename, "r") as file:
            config = json.load(file)

        c = kmc_config.MonteCarloConfigurator(filename)
        self.format_str = c.get_format()
        (self.iter_var, self.iter_values) = c.get_iteration_variable()

        if "params" in config:
            for key, value in config["params"].iteritems():
                if key == "trials":
                    self.trials = value                 
                elif is_sequence(value):
                    self.vars[key] = value
                else:
                    self.fixed[key] = value

        #Now load the results from the config file
        if "results" not in config["config"]:
            print "Could not load simulation results from config file"
            raise ValueError("Config file must have results listed so that we know how to parse the result files.")

        results = c.get_result_names()

        self.results = map(lambda x: (x, 1), results) #put it in the expected format

        self.expected_files = c.count_expected_files()

    def clear_remote_data(self):
        remote_dir = '/farmshare/user_data/timburke/batch'
        remote  = os.path.join(remote_dir, self.shortname, "data", "*")

        self._exec_remote('rm -rf %s' % remote)

    def status(self, **kw):
        """
        Check if this run is currently submitted to a compute cluster for processing and if so what its status
        is.

        Returns True if the run is complet and False if the run is incomplete or needs to be updated
        the quiet keyword disables all printing
        """

        quiet = ("quiet" in kw and kw["quiet"])
        verbose = ("verbose" in kw and kw["verbose"])

        files = [('config.json', 'config.json'), ('driver.py','batch_payload.py'), ('log.conf', 'log.conf'), ('userconfig.py', 'userconfig.py')]

        remote_dir = '/farmshare/user_data/timburke/batch'

        if not quiet:
            print "Run %s" % self.shortname

        stati = []

        for file in files:
            local = os.path.join(self.local_dir, file[0])
            remote  = os.path.join(remote_dir, self.shortname, file[1])

            status = self._compare_files(local, remote)

            stati.append(status)

            if verbose:
                print "%s: %s" % (file[0], status)

        if not verbose:
            if reduce(lambda x,y: x and y, [x == "same" for x in stati]):
                print "All input files up to date"
            else:
                print "run files are not synced."

        #Check number of files in data
        num_data = self._get_num_remote_files(os.path.join(remote_dir, self.shortname, "data"))
        num_nodes = self._infer_num_nodes(os.path.join(remote_dir, self.shortname, "data"))

        if num_nodes == -1:
            if not quiet:
                print "No data files present"
            done = False
        elif num_data == self.expected_files*num_nodes:
            if not quiet:
                print "All data files present (%d parameter combinations x %d nodes = %d)" % (self.expected_files, num_nodes, num_data)
            done = True
        elif num_data < self.expected_files*num_nodes:
            if not quiet:
                print "%d of %d data files finished (%d%%)" % (num_data, self.expected_files*num_nodes, int(num_data*100/(self.expected_files*num_nodes)))
            done = False
        else:
            if not quiet:
                print "More data files than there should be.  Configuration mismatch. (found %d, expected %d)" % (num_data, self.expected_files*num_nodes)
            done = False

        file_status = reduce(lambda x,y: x and y, [x == "same" for x in stati])

        return {"files": file_status, "finished": done}

    def start(self, nodes=-1, nocheck=False, quiet=True):
        """
        Start the remote version of this process on the corn cluster.
        Passing -1 for nodes causes the script to check the server's load and 
        choose the maximum appropriate number of nodes based on the number of
        free cores.
        """

        def pq(x):
            if not quiet:
                print x

        #Check if the remote files are ready
        if not nocheck:
            pq("Checking remote status")
            (status, done) = self.status(quiet=True)
            if status == False:
                pq("Remote files not ready")
                return False

        #Clear the remote data directory
        pq("Clearing remote data directory")
        self.clear_remote_data()

        if nodes < 0:
            (idle, cores) = self._get_remote_load()

            nodes = cores-1

            if nodes <= 0:
                pq("Server is overloaded, try a different one.")
                return False

            pq("Using %d cores" % nodes)

        remote_dir = os.path.join("/farmshare/user_data/timburke/batch", self.shortname)
        remote_script = os.path.join(remote_dir, "batch-script.sh")
        hostfile = os.path.join(remote_dir, 'host.txt')
        errorfile = os.path.join(remote_dir, 'out.txt')

        ret = self._exec_commands(['cat /etc/hostname', 'cat /etc/hostname >> %s' % hostfile, 'cd %s' % remote_dir, 'export NSLOTS=%d' % nodes, 'nohup %s > /dev/null 2>&1 &' % remote_script])

        pq(ret)

        return True

    def upload_config(self):
        """
        Send the files needed for this run to the remote server.
        """

        if self.shortname == "" or self.shortname == "*":
            print "Invalid shortname:", self.shortname
            return

        with SolarSettings() as s:
            localdir = s.get('rundir')

        sub_script = os.path.join(localdir, self.shortname, "submit.sh")

        remote_dir = os.path.join('/farmshare/user_data/timburke/batch', self.shortname)

        self._exec_remote("rm -rf %s" % remote_dir)

        out = check_output('/bin/bash "%s"' % sub_script, shell=True)

        #Reload configuration and files
        self._reset()

    def clear_data(self):
        """
        Clear all data files in local data directory
        """

        with SolarSettings() as s:
            localdir = s.get('rundir')

        local = os.path.join(localdir, self.shortname, "data")

        if os.path.isdir(local):
            for file in os.listdir(local):
                full_path = os.path.join(local, file)
                os.unlink(full_path)

    def download_data(self):
        """
        Download all the data files for this run from the server, if it's finished.
        Return an error otherwise.
        """

        (files, finished) = self.status(quiet=True)

        if not finished:
            print "Run is not finished.  Exiting"

        remote_base = '/farmshare/user_data/timburke/batch'
        remote  = os.path.join(remote_base, self.shortname, "data")

        with SolarSettings() as s:
            localdir = s.get('rundir')

        local = os.path.join(localdir, self.shortname, "data")

        if os.path.isdir(local):
            print "Clearing local data directory"
            shutil.rmtree(local)

        os.mkdir(local)

        self._get_directory(local, remote)

        #Reload configuration and files
        self._reset()

    def _compare_files(self, local, remote):
        """
        Check the status the local and remote files and see if they are the same, or which one is newer
        """

        try:
            ret = self._exec_remote('stat "%s" | grep Change' % remote)
        except subprocess.CalledProcessError:
            return "no remote"

        if ret == "":
            return "no remote"

        expr = re.compile('^Change: ([0-9]+)-([0-9]+)-([0-9]+) ([0-9]+):([0-9]+):([0-9]+)')

        match = re.search(expr, ret)

        if match is None:
            print "Could not match compare files RE.  Output was:"
            print ret
            return "error"

        remote_mod = datetime.datetime( int(match.group(1)), int(match.group(2)), int(match.group(3)),
                                        int(match.group(4)), int(match.group(5)), int(match.group(6)))

        remote_hash = self._exec_remote('md5sum "%s"| grep -E -o \'^[a-zA-Z0-9]+\'' % remote)
        remote_hash = remote_hash.rstrip()

        with open(local, "rb") as loc:
            local_hash = hashlib.md5(loc.read()).hexdigest()

        local_mod = datetime.datetime.fromtimestamp(os.path.getmtime(local))

        if local_hash == remote_hash:
            return "same"
        elif local_mod > remote_mod:
            return "local newer"
        else:
            return "remote newer"

    def _get_num_remote_files(self, dir):
        """
        List the number of files in the remote directory given
        """

        ret = self._exec_remote('ls -l "%s" | perl -pe "chomp if eof" | wc -l' % dir)

        return int(ret.rstrip())

    def _infer_num_nodes(self, dir):
        """
        Given a data directory on a remote server, infer the number of nodes that were used to create the files in it from this highest
        -n.npy.gz file ending found.
        """

        #list files, get the node number from each file, remove duplicates, sort and return the highest number
        ret = self._exec_remote("ls -l '%s' | grep -E -o -- '[0-9]+.npy.gz' | grep -E -o -- '[0-9]+' | sort -u | tail -n1" % dir)

        ret = ret.rstrip()

        if ret == "":
            return -1

        return int(ret) + 1

    def _get_remote_load(self, num_cores=8):
        """
        Get the current load on the server and translate that into an effective number of free cores
        based on the known number of cores of the machine.
        """

        val = self._exec_commands(["top -b -n 4 | grep -E -o '([0-9]+.[0-9]+)%id'"])

        lines = val.split("\n")

        #returns the average fraction of CPU time that's idle
        loads = [float(x[:-3]) for x in lines if x != ""]
        loads.sort()

        #throw out high outlier and average
        avg_load = sum(loads[:-1])/(len(loads)-1.0)

        free_cores = int(math.floor(avg_load/100.0*num_cores))

        return (avg_load, free_cores)
