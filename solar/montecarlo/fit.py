#fit.py
#Automatically fit field depedence of a Montecarlo run to a generalized logistic function

import numpy as np
import scipy.optimize as opt


def richards_function(x, *params):
    """
    return richards functon for the independent variables x
    using the parameters given in params:
    A - lower asymptote
    K - upper asymptote
    B - growth rate
    v - affects near which assymptote max growth occurs
    Q - depends on Y(0)
    M - x value of maximum growth if Q=v
    """

    A = params[0]
    K = params[1]
    B = params[2]
    v = params[3]
    Q = params[4]
    M = params[5]

    return A + (K-A) / np.power((1+Q*np.exp(-B*(x-M))), 1.0/v)


def richards_half(params):
    """
    Return the argument for which richards function will be halfway between its two asymptotes
    """

    B = params[2]
    v = params[3]
    Q = params[4]
    M = params[5]

    return np.exp(M)*np.power((np.power(2.0, v)-1.0)/Q, -1.0/B)


def richards_width(params):
    """
    Return the range over which the function goes from 10 to 90 percent of its
    maximum value, i.e. from A + .1(K-A) to A + 0.9(K-A).  The result is returned
    as a difference of log(fields)
    """

    B = params[2]
    v = params[3]
    Q = params[4]

    return (np.log((np.power(10., v) - 1)/Q)-np.log((np.power(10./9., v) - 1)/Q))/B


def fit_field_dependence(fields, prob, suppress=False,rescale=1e3):
    """
    Fit the field depedence of the arrays given as a richards function
    """
    x = np.log(fields)
    lower = prob[0]*rescale
    upper = prob[-1]*rescale
    B = 1.0
    Q = 1.0
    v = 1.0
    M = (x[0] + x[-1]) / 2.0

    guesses = [lower, upper, B, v, Q, M]

    try:
        (params, cov) = opt.curve_fit(richards_function, x, prob*rescale, guesses)
    except RuntimeError:
        if suppress:
            return np.zeros([6])
        else:
            raise

    params[0] /= rescale
    params[1] /= rescale

    return np.array(params)


def fit_error(fields, vals, params):
    """
    Return the L2 norm of the two functions and the max deviation
    """

    x = np.log(fields)
    yact = vals
    ycal = richards_function(x, *params)

    ydiff = np.abs(yact-ycal)
    meanerr = np.sqrt(np.mean(ydiff*ydiff))
    maxerr = max(ydiff)

    return (meanerr, maxerr)
