#transition.py
#A class representing a sparse markov transition matrix describing the passed in monte carlo simulation parameters

import enum
import math
import kineticmc as kmc
import numpy as np
from scipy.sparse import coo_matrix
from bisect import bisect_left
from scipy.sparse.linalg import gmres, spsolve


def sorted_indices(seq):
    #http://stackoverflow.com/questions/3382352/equivalent-of-numpy-argsort-in-basic-python/3382369#3382369
    #by ubuntu
    return sorted(range(len(seq)), key=seq.__getitem__)


class TransitionMatrix:
    RecombinedState = 0
    FreeState = 1
    ElectronMovedState = 2

    def __init__(self, params):
        self.sim = kmc.SimulationController()

        self.sim.create_calc_sim(params)
        self.sim.set_calc_field([0.0, 0.0, 0.0])

        #Q Matrix (transient state transitions)
        self.rows = []
        self.cols = []
        self.vals = []

        #R Matrix (transient -> absorbing state transitions)
        self.r_rows = []
        self.r_cols = []
        self.r_vals = []

    def add_absorbing_transition(self, starting, final, prob):
        self.r_rows.append(starting)
        self.r_cols.append(final)
        self.r_vals.append(prob)

    def add_transient_transition(self, starting, final, prob):
        self.rows.append(starting)
        self.cols.append(final)
        self.vals.append(prob)

    def fill_nn_transitions(self, i, curr, origin, side, dist2):
        self.sim.set_calc_carrier_pos([0, 0, 0], curr)
        probs = self.sim.calc_hopping_rates()

        n = np.sum(probs)

        if math.isnan(n) or n == 0.0 or n == -0.0:
            print "Invalid n for location (%d, %d, %d): %f" % (curr[0], curr[1], curr[2], n)
        
        probs /= n

        elec_move_prob = np.sum(probs[0, ...])

        remove_recomb = None

        for ax in xrange(0, 3):
            for d in xrange(0, 2):
                curr[ax] += 1 - 2*d

                sep2 = curr[0]*curr[0] + curr[1]*curr[1] + curr[2]*curr[2]
                if sep2 > dist2:
                    self.add_absorbing_transition(i, TransitionMatrix.FreeState, probs[1, ax, d])
                elif curr[0] == 0 and curr[1] == 0 and curr[2] == 0:
                    self.add_absorbing_transition(i, TransitionMatrix.RecombinedState, 2*probs[1, ax, d])
                    remove_recomb = (ax,d)
                else:
                    self.add_transient_transition(i, enum.map_to_index(curr, origin, side), -probs[1, ax, d])

                curr[ax] -= 1 - 2*d

        if remove_recomb is not None:
            elec_move_prob -= probs[0,remove_recomb[0], 1-remove_recomb[1]]

        self.add_absorbing_transition(i, TransitionMatrix.ElectronMovedState, elec_move_prob)

    def build_fast_hole(self, size):
        """
        Build a transition matrix describing the evolution of the system when
        only the hole can move.
        size is the distance at which we should consider the electron and
            hole free.
        """

        max_dist2 = size*size
        side = 2*size+1
        origin = [-size, -size, -size]

        self.side = side
        self.origin = origin

        states = enum.create_cube_iterator(origin, side)

        for i, coord in states:
            sep2 = coord[0]*coord[0] + coord[1]*coord[1] + coord[2]*coord[2]

            if sep2 > max_dist2:
                continue

            if coord[0] == 0 and coord[1] == 0 and coord[2] == 0:
                continue

            self.fill_nn_transitions(i, coord, origin, side, max_dist2)
            self.add_transient_transition(i, i, 1.0)

        self._prune_matrices()
        #self._check_matrix()
        self._build_sparse()

    def _check_matrix(self):
        """
        Ensure that the matrix has entries that all add up to 1
        """

        curr_row = self.rows[0]
        row_sum = self.vals[0]

        transient_sums = {}

        for i in xrange(1, len(self.rows)):
            if self.rows[i] != curr_row:
                #Start a new row
                transient_sums[curr_row] = row_sum

                curr_row = self.rows[i]
                row_sum = 0.0

            row_sum += self.vals[i]

        for key,val in transient_sums.iteritems():
            if abs(1.0 - val) > 0.001:
                loc = enum.map_to_loc(self.row_map[key], self.origin, self.side)
                print "(%d, %d, %d): %f" % (loc[0], loc[1], loc[2], val)

    def _prune_matrices(self):
        """
        Renumber the states in the q matrix to begin sequentially
        from 0 while maintaining linkages between matrices
        """

        self.row_map = range(0, len(self.rows))

        col_sorted = sorted(enumerate(self.cols), key=lambda x: x[1])

        col_map = [x[1] for x in col_sorted]

        i = -1
        last = -1

        for row_in in xrange(0, len(self.rows)):
            old = self.rows[row_in]
            if old != last:
                i += 1
                self.row_map[i] = old
                last = old

            #Remap columns that this row is connected to
            found = bisect_left(col_map, old)

            while found < len(col_sorted) and col_sorted[found][1] == old:
                self.cols[col_sorted[found][0]] = i

                found += 1

            #Remap row index in absorbing states that this row is connected to
            found = bisect_left(self.r_rows, old)
            while found < len(self.r_rows) and self.r_rows[found] == old:
                self.r_rows[found] = i

                found += 1

            self.rows[row_in] = i

        self.num_states = i+1

    #def check_matrix()

    def build_extended(self):
        vals = [x for x in self.vals]
        rows = [x for x in self.rows]
        cols = [x for x in self.cols]

        dim = max(rows)

        
        ext_mat = coo_matrix((self.vals, (self.rows, self.cols)), shape=(self.num_states, self.num_states)).tocsr()

    def _build_sparse(self):
        self.n_matrix = coo_matrix((self.vals, (self.rows, self.cols)), shape=(self.num_states, self.num_states)).tocsr()
        self.r_matrix = coo_matrix((self.r_vals, (self.r_rows, self.r_cols)), shape=(self.num_states, 3)).tocsc()
        
    def solve_probabilities(self):
        rec = self.r_matrix[:, TransitionMatrix.RecombinedState].todense()
        esc = self.r_matrix[:, TransitionMatrix.FreeState].todense()
        mov = self.r_matrix[:, TransitionMatrix.ElectronMovedState].todense()

        rec_a = spsolve(self.n_matrix, rec).reshape([self.num_states])
        esc_a = spsolve(self.n_matrix, esc).reshape([self.num_states])
        mov_a = spsolve(self.n_matrix, mov).reshape([self.num_states])

        return rec_a, esc_a, mov_a

    def solve_recomb_prob(self):
        R = self.r_matrix[:, TransitionMatrix.RecombinedState].todense()
        res = gmres(self.n_matrix, R)

        return res

    def get_result(self, loc):
        red_loc = [loc[0] - self.origin[0], loc[1] - self.origin[1], loc[2] - self.origin[2]]

        return self.result[red_loc[0], red_loc[1], red_loc[2], :]

    def create_matrix(self):
        rec_a, esc_a, mov_a = self.solve_probabilities()

        results = np.zeros([self.side, self.side, self.side, 3])

        for i in xrange(0, self.num_states):
            loc = enum.map_to_loc(self.row_map[i], [0, 0, 0], self.side)

            results[loc[0], loc[1], loc[2], :] = [esc_a[i], rec_a[i], mov_a[i]]

        self.result = results

        return results
