#sparse.py
#Routines for loading Markov Chain matrices and vectors into numpy format for inspection

import struct
import numpy as np

from scipy.sparse import csr_matrix

def load_matrix(filename):
	with open(filename, "rb") as f:
		headerdata = f.read(40)

		header = struct.unpack_from("<QQQQQ", headerdata)

		if header[0] != 0x5858DADAEEFF0011:
			raise ValueError("Invalid Magic in Sparse Matrix File: %u" % header[0])

		valsize = header[1]
		indsize = header[2]

		if valsize != 8 or indsize != 4:
			raise ValueError("Invalid element sizes in Sparse Matrix File: (value: %u, index: %u)" % (valsize, indsize))

		dim = header[3]
		nelems = header[4]

		cols = np.fromfile(f, dtype=np.int32, count=nelems)
		vals = np.fromfile(f, dtype=np.float64, count=nelems)
		rowptrs = np.fromfile(f, dtype=np.int32, count=(dim+1))

		return csr_matrix((vals, cols, rowptrs))
