#analysis.py
#Various analysis routines for interpreting markov chain results

import enum
import numpy as np
import numpy.ma as ma

def world_process_nn(origin, sides, data, combiner, state="free", orbit="hole"):
	"""
	Iterate over all nearest neighbor starting positions in a 
	system, and process the various results using
	"""

	move_hole = False

	if orbit == 'hole':
		move_hole = True

	world_dim = [x for x in sides]

	world = np.zeros(world_dim)
	world_mask = np.ndarray(world_dim, dtype=np.bool_)

	for stat in enum.cube(origin, sides):
		conds = []
		for orb in enum.neighbors(stat):

			if move_hole:
				res = data.get(stat, orb, state)
			else:
				res = data.get(orb, stat, state)

			if res is not None:
				conds.append(res)

		if len(conds) > 0:
			comb = combiner(conds)
			world[stat[0]-origin[0], stat[1]-origin[1], stat[2]-origin[2]] = comb
			world_mask[stat[0]-origin[0], stat[1]-origin[1], stat[2]-origin[2]] = False
		else:
			world_mask[stat[0]-origin[0], stat[1]-origin[1], stat[2]-origin[2]] = True

	return ma.array(world, mask=world_mask)

def world_iterate_nn(origin, sides, data):
	for stat in enum.cube(origin, sides):
		for orb in enum.neighbors(stat):
			index = data.map.map_state(stat, orb)

			if index == -1:
				continue

			yield (index, (stat, orb))