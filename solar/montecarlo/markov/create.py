#routines for creating markov transition matrices

import kineticmc as kmc
import os.path
import os
import numpy as np
import json

class MarkovChainCreator:
	def __init__(self):
		self.params = kmc.SimulationParameters()
		self.params.field = 0.0
		self.params.field_type = kmc.kIsotropic
		self.params.dialectric_const = 4.0
		self.params.lattice_const = 8.0 #Angstroms
		self.params.temp = 300 #Kelvin

		self.params.use_simd = True
		self.params.trials = 1000
		self.params.preallocate = True
		
		self.params.periodic_size.append(41)
		self.params.periodic_size.append(41)
		self.params.periodic_size.append(41)

		self.bounds = ([-20, 20],[-20,20],[-20,20])


		self.sim = None
		self.chain = None

		self.rules = []
		self.states = {}
		self.sols = {}

	def set_world(self, world):
		self.params.world_spec = world

	def set_mob(self, elec=None, hole=None):
		if elec is not None:
			self.params.elec_mob = elec
 
		if hole is not None:
			self.params.hole_mob = hole

	def set_lifetime(self, lifetime):
		self.params.recomb_life = lifetime

	def set_bounds(self, xb, yb, zb): 
		self.bounds = (xb, yb, zb)

	def create_chain(self):
		self.sim = kmc.SimulationController()

		self.sim.create_calc_sim(self.params)
		self.sim.set_calc_field([0.0, 0.0, 0.0])

		self.chain = kmc.MarkovChain()
		self.chain.set_simulator(self.sim)
		self.chain.set_bounds(self.bounds[0], self.bounds[1], self.bounds[2])

	def create_absorbing(self, name):
		if self.chain is None:
			return None

		self.states[name] = self.chain.create_absorbing_state(name)
		
		return self.states[name]

	def add_rule(self, rule):
		self.rules.append(rule)

	def clear_rules(self):
		self.rules = []

	def generate(self, elec, hole, extended = False):
		if len(self.rules) == 0:
			print "No rules specified for Markov chain, not generating since it would loop forever"
			return
 
		for rule in self.rules:
			self.chain.add_pruning_rule(rule)

		self.chain.build(elec, hole, extended)

	def map(self, elec, hole):
		return self.chain.map_state(elec, hole)

	def _create_dir(self, dir):
		parent = os.path.dirname(dir)

		if os.path.isdir(dir):
			return

		if not os.path.isdir(dir) and os.path.isdir(parent):
			os.mkdir(dir)
			return

		raise ValueError("Invalid path whose parent dir does not exist")

	def save_matlab(self, dir, prefix):
		self._create_dir(dir)

		path = os.path.join(dir, prefix)

		chain_p = path + "-chain.mat"
		self.chain.save_matlab(chain_p)

		for name, state in self.states.iteritems():
			state_p = path + "-" + name + ".vec"

			self.chain.save_absorbing_matlab(state, state_p)

	def load_solutions(self, dir, prefix):
		path = os.path.join(dir, prefix)

		self.sols.clear()

		for name, state in self.states.iteritems():
			state_p = path + "-" + name + ".ans"

			self.sols[name] = np.loadtxt(state_p)

	def results(self, elec=None, hole=None, off=-1):
		offset = off

		if elec is not None and hole is not None:
			offset = self.map(elec, hole)

		#Check if that location was touched in the chain
		if offset == -1:
			return None

		out = {}

		for name,sol in self.sols.iteritems():
			out[name] = sol[offset]

		return out

	def reset(self):
		self.chain = None
		self.sim = None
		self.states = {}
		self.rules = []
		self.bounds = ([-20, 20],[-20,20],[-20,20])
		self.params.recomb_life = 1.0