#dataset.py

import kineticmc as kmc
import numpy as np
from numpy import ma
import os.path
from scipy.sparse import csr_matrix

class MarkovDataSet:
	def __init__(self, mapfile, statefiles, names, matrix):
		"""
		Create an object around the solved Markov Chain specified in the given files.
		states should be a list of DenseVector files.
		"""

		self.map = kmc.StateList(mapfile)
		self.states = {names[i]: np.load(statefiles[i]) for i in xrange(0, len(statefiles))}
		self.mat_file = matrix

	@classmethod
	def FromDir(cls, parent, name, statenames):
		mapname = "q%s_states.map" % name
		matfile = os.path.join(parent, "q%s.mat" % name)

		states = [os.path.join(parent, "q%s_%ssol.npy" % (name, state)) for state in statenames]

		return MarkovDataSet(os.path.join(parent, mapname), states, statenames, matrix=matfile)

	def get(self, elec, hole, name):
		ind = self.map.map_state(elec, hole)

		if ind == -1:
			return None
			
		return self.states[name][ind]

	def state_exists(self, elec, hole):
		nd = self.map.map_state(elec, hole)

		if ind == -1:
			return False

		return True

	def iterate(self, name, filter=None, map=None):
		for i in xrange(0, self.map.len()):
			val = self.states[name][i]
			s = self.map.map_index(i)

			if filter is not None and filter(s, val, self) == False:
				continue

			if map:
				yield map(s, val, self)
			else:
				yield (s, val)

	def load_matrix(self):
		chain = kmc.RealSparseMatrix()
		res = chain.load(self.mat_file)

		if res != kmc.kNoError:
			raise ValueError("Could not load chain from file %s: Error %d" % (self.mat_file, res))

		qcols = chain.np_cols(chain.num_elems())
		qrows = chain.np_rowptrs(chain.rows()+1)
		qvals = chain.np_values(chain.num_elems())

		self.matrix = csr_matrix((qvals,qcols,qrows),shape=(chain.rows(),chain.rows()))

		del chain