#enumerate.py
import kineticmc as kmc
import itertools

def cube(origin, sides):
    """
    Create a generator that iterates over position in a 3D cube with smallest (x,y,z) values given by origin
    """

    if len(origin) != 3:
        raise ValueError('origin must be 3-tuple')

    if isinstance(sides, int):
        sides = [sides]*3
    elif len(sides) != 3:
        raise ValueError('Sides should be an integer or a list with 3 elements')

    iters = [xrange(origin[i], origin[i]+sides[i]) for i in xrange(0,3)]

    return itertools.product(*iters)

def neighbors(start):
    """
    Iterate over all of the neighbors of this position, not including it
    """

    if len(start) != 3:
        raise ValueError('origin must be 3-tuple')

    for ax in xrange(0,3):
        for d in xrange(-1,2,2):
            ret = [start[i] for i in xrange(0,3)]

            ret[ax] += d
            yield ret

def neighbors_periodic(start, N):
    """
    Iterate over all of the neighbors of this position, not including it
    """

    if len(start) != 3:
        raise ValueError('origin must be 3-tuple')

    for ax in xrange(0,3):
        for d in xrange(-1,2,2):
            ret = [start[i] for i in xrange(0,3)]

            ret[ax] += d

            ret[ax] %= N
            yield ret