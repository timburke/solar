#escape.py

import kineticmc as kmc
from create import MarkovChainCreator
import os.path
import json
import numpy as np

def save_run_info(folder, params, states):
    """
    Save a json file with the information needed to reconstruct this run
    """

    d = params.copy()
    d["states"] = states

    with open(os.path.join(folder, 'info.json'), "w") as f:
        json.dump(d, f)

def get_solutions(folder):
    info_path = os.path.join(folder, 'info.json')

    with open(info_path, "r") as f:
        info = json.load(f)

    chain = MarkovChainCreator()
    chain.states = {n:None for n in info["absorbing"]}
    chain.load_solutions(folder, info["prefix"])

    states = info["states"]

    res = {}

    for key,val in states.iteritems():
        res[key] = chain.results(off=val)

    return res

def save_trilayer_escape(world, mob, lifetime, name, parent=None, lattice=8):
    #Changeable Parameters
    recomb_life = lifetime
    mob_ratio = mob
    sim_length = 18

    if parent is None:
        parent = '/Users/timburke/Documents/Graduate School/Research/Monte Carlo/markov/results'

    direc = os.path.join(parent, name)

    #Create Simulation World
    p3ht = kmc.PureMaterialSpec(-4.72, -3.09)
    pcbm = kmc.PureMaterialSpec(-5.98, -3.77)
    #mixed = kmc.PureMaterialSpec(-4.72-widening, -3.77+widening)

    #world = kmc.TrilayerSpec(p3ht, pcbm, mixed, mixed_width)

    #Create mobility values
    elec_mob = kmc.IsotropicMobilityGenerator(0.0)
    hole_mob = kmc.IsotropicMobilityGenerator(mob_ratio)

    #Create Simulation World
    #p3ht = kmc.PureMaterialSpec(-4.72, -3.09)
    #pcbm = kmc.PureMaterialSpec(-5.98, -3.77)
    #mixed = kmc.PureMaterialSpec(-4.72-widening, -3.77+widening)
    #world = kmc.TrilayerSpec(p3ht, pcbm, mixed, mixed_width)

    #Create mobility values
    elec_mob = kmc.IsotropicMobilityGenerator(0.0)
    hole_mob = kmc.IsotropicMobilityGenerator(mob_ratio)

    chain = MarkovChainCreator()

    chain.set_mob(elec_mob, hole_mob)
    chain.set_lifetime(recomb_life)
    chain.set_world(world)
    chain.set_bounds([-20,20],[-20,20], [-20,20])
    chain.params.lattice_const = lattice

    #Create the chain 
    chain.create_chain()

    state = chain.create_absorbing("elec_hopped")
    recomb = chain.create_absorbing("neighbor")
    free = chain.create_absorbing("free")

    rule = kmc.MaximumHopsPruningRule(0,-1,state)
    nn_rule = kmc.SpecificSeparationPruningRule(1, recomb)
    free_rule = kmc.MaxSeparationPruningRule(sim_length, free)
    unlikely_rule = kmc.UnlikelyHopPruningRule(state, 0.00001)

    chain.add_rule(nn_rule)
    chain.add_rule(rule)
    chain.add_rule(free_rule)
    chain.add_rule(unlikely_rule)

    chain.generate([0,0,0], [-2,0,0], False)
    chain.save_matlab(direc, 'escape')

    params = {'lifetime':recomb_life, 'hole_mob':mob_ratio, 'prefix':'escape'}

    params["absorbing"] = ['elec_hopped', 'neighbor', 'free']

    states = {'direct': chain.map([0,0,0],[-2,0,0]),
                'side1': chain.map([0,0,0],[-1,-1,0]),
                'side2': chain.map([0,0,0],[-1,1,0]),
                'side3': chain.map([0,0,0],[-1,0,-1]),
                'side4': chain.map([0,0,0],[-1,0,1])
            }

    save_run_info(direc, params, states)
