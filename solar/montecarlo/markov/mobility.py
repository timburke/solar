#mobility.py
#Given a trace of particle's mean square displacement vs time,
#calculate the mobility spectrum.

from scipy.fftpack import fft, fftshift, fftfreq
import numpy as np


def calculate_mobility(disp2, timestep, run, delay=0, lattice=1e-7, T=300):
	"""
	Give a uniformly sampled square displacement signal, calculate the 
	mobility spectrum using Kubo's Linear Response formula.  Calculates
	the spectrum for the sample from [delay*timestep, (delay+run)*timestep).
	Default lattice parameter setting gives a value in cm^2/Vs for displacements
	in nanometers.
	""" 

	end = int(delay + run)
	start = int(delay)
	kT = 8.6173324e-5*T

	spectrum = fft(disp2[start:end]*timestep*lattice*lattice)
	freqs = fftfreq(end-start, d=timestep)

	spectrum = fftshift(spectrum)
	freqs = fftshift(freqs)

	compl_mobility = -1.0*(4.0*3.1415926535*3.1415926535)*freqs*freqs/(2.0*kT) * spectrum

	result = np.zeros([spectrum.shape[0], 3])

	result[:,0] = freqs
	result[:,1] = np.real(compl_mobility)
	result[:,2] = np.imag(compl_mobility)

	return result