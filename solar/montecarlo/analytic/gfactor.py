#gfactor.py
#Calculate the g-factor as an upper bound on splitting efficiency for Marcus Theory
#and the Miller-Abrahams mobility model.

import numpy as np
from solar.reference import q

class AnalyticalMarcusTheory:
    def __init__(self):
        self.reorg_e = 0.25 #reorganization energy in electron volts
        self.lattice = 1    #lattice constant in nm
        self.temp    = 300  #temperature in Kelvin
        self.eps_r   = 4    #relative dielectric constant

    def lowfield_gfactor(self):
        """
        Calculate the low-field g-factor for Marcus Theory given the parameter values
        in this object
        """

        eps = 8.85418782e-12
        kb = 8.6173324e-5
        
        binding = q/(4*np.pi*self.eps_r*self.eps*self.lattice*kb*self.temp) #only 1 q to get it in eV

        denom = 4*self.reorg_e*kb*self.temp
        away = exp((binding/2.0 + self.reorg_e)**2/denom)
        side = exp((binding/2.0*(2.0-sqrt(2.0)) + self.reorg_e)**2/denom)

        return away + 4*side