#trace.py
#Read in time and position traces from a directory and create traces
#with the particle separations as a function of time

import os.path
from glob import glob
import numpy as np
import matplotlib.pyplot as plt

class KMCTrajectory:
    def __init__(self, trajfile, timefile):
        (elocs, hlocs) = self._read_traj(trajfile)
        delays = self._read_times(timefile)
        seps = self._convert_to_dist(elocs, hlocs)

        ls = len(seps)
        lt = len(delays)

        self.traj = np.zeros([ls, 2])
        self.traj[1:,0] = np.cumsum(delays)
        self.traj[:, 1] = np.array(seps)
        #elf.traj[:, 2] = np.array(elocs)
        #self.traj[:, 3] = np.array(hlocs)

    def _read_traj(self, filename):
        elocs = []
        hlocs = []
        with open(filename, "r") as f:
            while f.readline() != '':
                unused = f.readline()
                elecline = f.readline().rstrip()
                holeline = f.readline().rstrip()
                elec = elecline.split(' ')
                hole = holeline.split(' ')
                
                eloc = map(lambda x: float(x), elec[1:])
                hloc = map(lambda x: float(x), hole[1:])
                
                elocs.append(eloc)
                hlocs.append(hloc)
        
        return (elocs, hlocs)

    def _read_times(self, filename):
        with open(filename, "r") as f:
            delays = map(lambda x: float(x.rstrip()), f.readlines())

        return delays

    def _convert_to_dist(self, elocs, hlocs):
        l = len(elocs)
        dists = np.zeros([l])
        
        for i in xrange(0,l):
            d2 = 0
            for j in xrange(0,3):
                d2 += (elocs[i][j]-hlocs[i][j])**2
            
            dists[i] = np.sqrt(d2)
        
        return dists

    def plot(self, dir=None, name=None, timescale=False):
        fig = plt.figure(figsize=(10,2.3))
        plt.ylim(0,14)
        plt.subplots_adjust(bottom=.3)
        #xlim(0,2)

        if timescale:
            plt.xlabel('Time (ns)', fontsize=14)
            plt.plot(self.traj[:,0], self.traj[:,1]/10.0)
        else:
            plt.xlabel("Simulation Step", fontsize=14)
            plt.plot(self.traj[:,1]/10.0)
        
        plt.ylabel('Separation (nm)',fontsize=14)

        if dir is not None and name is not None:
            plt.savefig(os.path.join(dir, name),transparent=True)

class TrajectoryList:
    def __init__(self, directory):
        """
        Create a list of all the trajectories in a directory
        """
        trajpath = os.path.join(directory, 'traj-*')
        timepath = os.path.join(directory, 'time-*')

        trajs = glob(trajpath)
        times = glob(timepath)

        #Sort both by number from low to high
        trajs.sort(key=get_suffix)
        times.sort(key=get_suffix)

        if len(trajs) != len(times):
            raise ValueError("Directory did not contain matching time and trajectory files")

        self.traces = []

        for i in xrange(0, len(trajs)):
            self.traces.append(KMCTrajectory(trajs[i],times[i]))

def get_suffix(name):
    basename,ext = os.path.splitext(os.path.basename(name))
    alpha, num = basename.split('-')
    return int(num)