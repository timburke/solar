#hist.py

import variablesetter
import matplotlib.pylab as plt
import numpy as np
import numpy.ma as ma
import statsmodels.api as sm # recommended import according to the docs

class MeanDistancePlot (variablesetter.VariableSetter):
	"""
	Plot the mean distance traveled by electrons and holes that ultimately recombine 
	as well as their maximum separation as a function of the remaining free variable 
	in the simulation.
	"""
	
	def __init__(self, **kwarg):		
		kwarg["one_free"] = True
		
		if "logplot" in kwarg:
			self.logplot = kwarg["logplot"]
		else:
			self.logplot = False
			
		if "units" in kwarg:
			self.x_units = kwarg["units"]
		else:
			self.x_units = None
		
		if "connected" in kwarg:
			self.connected = kwarg["connected"]
		else:
			self.connected = True
		
		if "error" in kwarg:
			self.error = kwarg["error"]
		else:
			self.error = False
		
		super(MeanDistancePlot, self).__init__(**kwarg)
	
	def set(self, param, value):
		self.params[param] = value
	
	def _post_match(self, run):
		return super(MeanDistancePlot, self)._post_match(run)
	
	def _run(self, run):
		#if our 1 free param isn't the iteration variable, we need to add it explicitly
		#since it goes into picking the right files to load from
		if self.free_param != run.iter_var:
			params = self.params.copy()
			params[self.free_param] = '*'
		else:
			params = self.params
			
		mask = run.get_results(params, ['Simulation Result'])
		dist = run.get_results(params, ['Max Electron Distance', 'Max Hole Distance', 'Max Separation'])
		
		style = '-'
		
		if not self.connected:
			style = 'o'
			
		if self.error:
			pltfun = lambda x,y,err: plt.errorbar(x, y, yerr=err, fmt=style)
		else:
			pltfun = lambda x,y,err: plt.plot(x, y, style)
		
		if self.logplot:
			fixaxes = lambda: plt.semilogx()
		else:
			fixaxes = lambda: 1.0
		
		#Create a mask removing all the instances where the particles escaped
		mask = (mask == 0.0)
		
		masked_dist = ma.array(dist, mask=np.repeat(mask[...,np.newaxis], 3,axis=-1))
		
		#average over all the trials leaving the other columns untouched
		means = masked_dist.mean(axis=-2)
		counts = masked_dist.count(axis=-2)
		
		fig = plt.figure(figsize=(12,8))
		ax = plt.subplot(3,1,1)
		pltfun(run.vars[self.free_param], means[:,0]/10.0, means[:,0]/10.0/np.sqrt(counts[:,0]))
		fixaxes()
		plt.title("Mean Election Displacement")
		ax.set_xticklabels([])
		
		ax = plt.subplot(3,1,2)
		pltfun(run.vars[self.free_param], means[:,1]/10.0, means[:,2]/10.0/np.sqrt(counts[:,2]))
		fixaxes()
		plt.title("Mean Hole Displacement")
		ax.set_xticklabels([])
		
		plt.subplot(3,1,3)
		pltfun(run.vars[self.free_param], means[:,2]/10.0, means[:,2]/10.0/np.sqrt(counts[:,2]))
		fixaxes()
		plt.title("Mean Particle Separation")
		
		xlabel = self.free_param
		if self.x_units is not None:
			xlabel += " (%s)" % str(self.x_units)
			
		fig.text(0.5,0.04, xlabel, ha='center', va='center', fontsize='18')
		fig.text(0.02, 0.5, 'Distance (nm)', ha='center', va='center', rotation='vertical', fontsize='18')
		
		plt.subplots_adjust(left=0.07, right=0.98, top=0.9, bottom=0.1)
		
		return means