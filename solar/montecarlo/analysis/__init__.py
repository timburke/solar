#This subpackage contains canned analysis features that can be applied to Monte Carlo
#runs easily.  Each analysis can establish the kind of parameters and results that
#must be available for it to be run.

from hist import DistancePlot
from meandist import MeanDistancePlot
from plot import FieldPlot
from sigmoid import SigmoidFitter

known_analyses = {}


def register_analyzer(name, type):
    global known_analyses
    
    known_analyses[name] = type

def get_analyzer(name, *args, **kw):
    global known_analyses
    
    if name in known_analyses:
        return known_analyses[name](*args, **kw)
    
    raise ValueError('Unknown analysis type: %s' % name)
