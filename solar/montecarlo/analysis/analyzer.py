#analyzer.py

class Analyzer (object):
	def __init__(self, **kw):
		self.required_results = []
		self.required_params = []
		
	def _matches(self, run):
		"""
		Make sure that the passed in run has the necessary results and parameters
		to use this analyzer.
		"""
		res = set(self.required_results)
		par = set(self.required_params)
		
		run_res = set([x[0] for x in run.results])
		run_par = set(run.vars.keys()) | set(run.fixed.keys())
		
		if res <= run_res and par <= run_par:
			if hasattr(self, "_post_match"):
				return self._post_match(run)
			else:
				return True
		
		return False
	
	def _post_match(self, run):
		"""
		Base case for post_match chain.  All subclasses that override this will end up 
		creating a chain of conditions with this one at the base.
		"""
		
		return True
		
	def run(self, run):
		"""
		Run the analysis specified on the run passed in if the run's preconditions
		match against the run
		"""
		
		if self._matches(run):
			return self._run(run)