#variablesetter.py

import analyzer
import matplotlib.pylab as plt
import numpy as np


class VariableSetter (analyzer.Analyzer):
    """
    Plot the field dependence of a Monte Carlo run given set values of the variable
    parameters for the run.

    kw:
    allow_extra - allow setting unneeded parameters without throwing an error
    """
    def __init__(self, **kw):
        super(VariableSetter, self).__init__(**kw)

        self.params = kw.get("params", {})
        self.allow_extra = kw.get("allow_extra", False)
        self.omit_iter = kw.get("omit_iter", False)
        self.one_free = kw.get("one_free", False)
        self.autofill = kw.get("autofill", False)

    def set(self, param, value):
        self.params[param] = value

    def clear(self, param):
        del self.params[param]

    def _post_match(self, run):
        """
        Make sure every param that we've set here is a variable parameter in the MC
        run unless we're allowing extra parameters and make sure that every required
        variable parameter in the run has a set value.
        """

        needed = set(run.vars.keys())
        avail = set(self.params.keys())

        #Omit the iteration variable if we don't need it to be set
        if self.omit_iter:
            needed.discard(run.iter_var)

        #There was a parameter that was needed that was not found
        #This is an error unless we want a free parameter
        if not needed.issubset(avail):
            if len(needed - avail) == 1 and self.one_free:
                self.free_param = (needed - avail).pop()
            elif not self.autofill:
                for param in needed - avail:
                    print "Needed variable parameter %s not set" % param
                return False

        #There were extra parameters
        if (not avail.issubset(needed)) and not self.allow_extra:
            for param in avail - needed:
                print "Extra variable parameter %s not allowed, use keyword allow_extra to permit this" % param
            return False

        if (not hasattr(self, 'free_param')) and self.one_free:
            print "Must leave one free parameter for this analysis, no free parameters found."
            return False

        return super(VariableSetter, self)._post_match(run)

    def _get_vars(self, run):
        """
        Take the parameters that have been set, as well as the ones from the run if autofill is set
        """

        params = self.params.copy()

        needed = set(run.vars.keys())
        avail = set(self.params.keys())

        needed.discard(run.iter_var)

        if self.autofill:
            print "Autofilling remaining parameters"
            for p in needed - avail:
                params[p] = '*'
                print p

        return params
