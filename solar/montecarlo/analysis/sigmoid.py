##sigmoid.py

import variablesetter
import numpy as np
from .. import fit


class SigmoidFitter (variablesetter.VariableSetter):
    """
    Fit the field dependence of every variable combination in this run
    to a sigmoidal function and return an array of the sigmoid parameters
    """

    def __init__(self, **kwargs):
        kwargs['omit_iter'] = True
        kwargs['autofill'] = True

        self.suppress = kwargs.get('suppress', False)

        super(SigmoidFitter, self).__init__(**kwargs)

    def _post_match(self, run):
        if 'Simulation Result' not in [x[0] for x in run.results]:
            return False

        return super(SigmoidFitter, self)._post_match(run)

    def _run(self, run):
        results = 1.0 - np.mean(run.get_results(self._get_vars(run), ['Simulation Result']), axis=-1)

        #Keep all the permutation dimensions, but replace the field axis with one containing spots for our
        #parameters
        shp = list(results.shape)
        shp[-1] = 10
        shp = tuple(shp)

        out = np.zeros(shp)

        fitter = lambda x: fit.fit_field_dependence(run.iter_values, x, suppress=self.suppress)
        out[..., 0:6] = np.apply_along_axis(fitter, -1, results)

        calc = lambda x: np.array([fit.richards_width(x), fit.richards_half(x)])

        if len(shp) > 1:
            out[..., 6:8] = np.apply_along_axis(calc, -1, out[..., 0:6])
        else:
            out[..., 6:8] = calc(out[..., 0:6])

        #TODO: Calculate errors as well

        return out
