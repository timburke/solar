#hist.py

import variablesetter
import matplotlib.pylab as plt
import numpy as np
import numpy.ma as ma
import statsmodels.api as sm # recommended import according to the docs

class DistancePlot (variablesetter.VariableSetter):
	"""
	Plot a histogram of the maximum distance traveled by each particle that eventually recombines
	as a function of the other parameters as needed.
	"""
	
	def __init__(self, **kwarg):		
		super(DistancePlot, self).__init__(**kwarg)
	
	def set(self, param, value):
		self.params[param] = value
	
	def _post_match(self, run):
		return super(DistancePlot, self)._post_match(run)
	
	def _run(self, run):
		mask = run.get_results(self.params, ['Simulation Result'])
		dist = run.get_results(self.params, ['Max Electron Distance', 'Max Hole Distance', 'Max Separation'])
		
		#Create a mask 		
		mask = (mask == 0.0)
		
		masked_dist = ma.array(dist, mask=np.repeat(mask[...,np.newaxis], 3,axis=-1))
		
		elec_dist 	= masked_dist[...,0].compressed()
		hole_dist 	= masked_dist[...,1].compressed()
		sep 		= masked_dist[...,2].compressed()
		
		ecdf_elec = sm.distributions.ECDF(elec_dist)
		ecdf_hole = sm.distributions.ECDF(hole_dist)
		ecdf_sep = sm.distributions.ECDF(sep)
		
		#Create dashboard figure
		fig = plt.figure(figsize=(18,12))
		plt.subplot(2,3,1)
		
		#Top left, elec dist histogram
		plt.hist(elec_dist/10.0, bins=np.sqrt(elec_dist.shape[0]))
		plt.text(0.95, 0.9, 'Mean: %.1f nm' % np.mean(elec_dist/10.0), horizontalalignment='right', transform=plt.subplot(2,3,1).transAxes)
		plt.text(0.95, 0.85, 'Std Dev: %.1f nm' % np.std(elec_dist/10.0), horizontalalignment='right', transform=plt.subplot(2,3,1).transAxes)
		plt.title("Electron Max. Displacement")
		plt.xlabel("Distance (nm)")
		plt.ylabel("Occurances")
		
		#Top Middle, hole dist histogram
		plt.subplot(2,3,2)
		plt.hist(hole_dist/10.0, bins=np.sqrt(hole_dist.shape[0]))
		plt.text(0.95, 0.9, 'Mean: %.1f nm' % np.mean(hole_dist/10.0), horizontalalignment='right', transform=plt.subplot(2,3,2).transAxes)
		plt.text(0.95, 0.85, 'Std Dev: %.1f nm' % np.std(hole_dist/10.0), horizontalalignment='right', transform=plt.subplot(2,3,2).transAxes)
		plt.title("Hole Max. Displacement")
		plt.xlabel("Distance (nm)")
		plt.ylabel("Occurances")

		#Top Right, separation histogram
		plt.subplot(2,3,3)
		plt.hist(sep/10.0, bins=np.sqrt(sep.shape[0]))
		plt.text(0.95, 0.9, 'Mean: %.1f nm' % np.mean(sep/10.0), horizontalalignment='right', transform=plt.subplot(2,3,3).transAxes)
		plt.text(0.95, 0.85, 'Std Dev: %.1f nm' % np.std(sep/10.0), horizontalalignment='right', transform=plt.subplot(2,3,3).transAxes)
		plt.title("Max. Separation")
		plt.xlabel("Distance (nm)")
		plt.ylabel("Occurances")
		
		#Now plot continuous distribution functions
		plt.subplot(2,3,4)
		plt.plot(np.linspace(min(elec_dist), max(elec_dist), 200)/10.0, 1.0 - ecdf_elec(np.linspace(min(elec_dist), max(elec_dist), 200)))
		plt.title("Survivor Function")
		plt.xlabel("Distance (nm)")
		plt.ylabel("Probability to reach dispacement")
		
		plt.subplot(2,3,5)
		plt.plot(np.linspace(min(hole_dist), max(hole_dist), 200)/10.0, 1.0 - ecdf_hole(np.linspace(min(hole_dist), max(hole_dist), 200)))
		plt.title("Survivor Function")
		plt.xlabel("Distance (nm)")
		plt.ylabel("Probability to reach dispacement")
		
		plt.subplot(2,3,6)
		plt.plot(np.linspace(min(sep), max(sep), 200)/10.0, 1.0 - ecdf_sep(np.linspace(min(sep), max(sep), 200)))
		plt.title("Survivor Function")
		plt.xlabel("Distance (nm)")
		plt.ylabel("Probability to reach dispacement")
		
		return masked_dist