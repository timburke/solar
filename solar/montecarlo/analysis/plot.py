#plot.py

import variablesetter
import matplotlib.pylab as plt
import numpy as np


class FieldPlot (variablesetter.VariableSetter):
    """
    Plot the field dependence of a Monte Carlo run given set values of the variable
    parameters for the run.
    """
    def __init__(self, **kwarg):
        kwarg['omit_iter'] = True

        super(FieldPlot, self).__init__(**kwarg)

    def _post_match(self, run):
        if 'Simulation Result' not in [x[0] for x in run.results]:
            return False

        return super(FieldPlot, self)._post_match(run)

    def _run(self, run):
        results = 1.0 - np.mean(run.get_results(self.params, ['Simulation Result']), axis=-1)

        plt.figure()
        plt.semilogx(run.iter_values, results)
        plt.xlabel(run.iter_var)
        plt.ylabel("Dissociation Probability")
