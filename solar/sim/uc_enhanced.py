#uc_enhanced.py
#A solar cell model that combined electro-optical modeling with up-converters to 
#do a combined benefit analysis and allow for optimization

from combined_device import CombinedDevice
from solar import reference, utilities
from solar.types import *
import numpy as np
from numbers import Number

class UCEnhancedDevice(CombinedDevice):
	def __init__(self, dev):
		CombinedDevice.__init__(self, dev)
		self.uc = None
				
	def set_uc(self, upconverter, medium):
		r"""
		Attach an upconverter to the back of this device contained in an optical medium
		described by the OpticalMaterial medium.  It is assumed to be non-absorptive and
		semi-infite.
		
		medium can be either an optical material or just a number which is assumed to be
		the real part of the index of refraction.
		"""
		
		if isinstance(medium, Number):
			mat = OpticalMaterial(medium, 0.0)
		elif isinstance(medium, OpticalMaterial):
			mat = medium
		else:
			raise Exception("medium must be either a number specifying the index of refraction or an instance of OpticalMaterial")
		
		layer = OpticalFilm(mat, np.inf)
		layer.tag = "upconverter"
			
		self._optical_stack.remove_layers("upconverter")
		self.append_layer(layer)
		
		self.uc = upconverter
		
		self.modify()
		
	def set_interface(self, interface_stack):
		r"""
		Add optical layers between the device stack and the upconverter.  This replaces
		the layers that were already there, if there were any.	
		"""
		
		self._optical_stack.remove_layers("back_interface")
		interface_stack.tag_layers("back_interface")
		self._optical_stack.add_layers(interface_stack, after="device")

		self.modify()
	
	def ideal_benefit(self):
		r"""
		Sweep the absorption location of this up-converter to maximize its benefit for this device.
		"""
		
		if self.uc is None:
			raise Exception("Cannot perform up-converter optimization unless an up-converter type is chosen")
		
		(resp, totalCurrent) = self.uc.ideal_benefit(device=self)
	
		return resp
		
	def optimize_uc(self):
		print "Starting Optimization"
		resp = self.ideal_benefit()
		center = self.uc.ideal_emission(self.back_eqe())
		
		self.uc.shift = center
		self.uc.peak = resp[utilities.max_index(resp), 0]
		
		self.modify()
		print "Ending Optimization"