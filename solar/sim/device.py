#device.py
import numpy as np
import solar.reference
import solar.types
from scipy import integrate
from simplesim import photocurrent
import matplotlib.pylab as pl
from solar.fom import *

class SimulatedDevice:
	r"""
	The base class for all simulated devices in this framework.  It allows front and back
	illumination and subclasses implement optical propagation.  The base of the simulation
	can be either EQE curves, in which case the device can only be used to simulate the 
	voltage condition under which the EQE was taken, or it can be backed by either the 
	simulation programs WxAmps or PC1D and the EQEs will be generated for the conditions
	specified automatically by those programs.
	
	Subclasses must implement the function calculate_eqes() which is used to calculate the
	EQEs for this device upon both front and back illumination at the set bias voltage
	"""
	
	def __init__(self, name, path="(none)"):
		r"""
		Create a new device with the given name and file path.
		
		:param name: The name of this device, for posterity
		:param path: The file path for the device
		"""
		
		self.name = name
		self.path = path
		self._modified = True
		self._voltage = 0.0
		self._frontEQE = None
		self._backEQE = None
		self._frontIQE = None
		self._backIQE = None
		self.figures = {}
		self.stamp = 0
		
	def modify(self):
		self._modified = True
		self.stamp += 1
		
	def set_voltage(self, voltage):
		r"""
		Set the bias voltage at which the EQEs for this device will be calculated.
		"""
		
		if np.allclose(voltage, self._voltage):
			return
			
		else:
			self.modify()
			self._voltage = voltage
	
	def calculate_qes(self):
		raise Exception("You must override this function in a subclass of SimulatedDevice to actually calculate EQEs here.")
	
	def update_qes(self):
		if self._modified:
			self.modify()
			self.calculate_qes()
			self._modified = False
	
	def front_eqe(self):
		self.update_qes()
		
		return self._frontEQE
		
	def front_iqe(self):
		self.update_qes()
		
		if self._frontIQE is None:
			raise Exception("This device does not support calculating front internal quantum efficiencies.")
		
		return self._frontIQE
	
	def back_iqe(self):
		self.update_qes()
		
		if self._backIQE is None:
			raise Exception("This device does not support calculating back internal quantum efficiencies.")
		
		return self._backIQE
	
	def back_eqe(self):
		self.update_qes()
		
		return self._backEQE
	
	#Convenience function to plot via matplotlib the eqes of this device
	def plot(self, **kwargs):
		legendEntries = []
		
		self.update_qes()
		
		if (not "front" in kwargs) or kwargs["front"] == True:
			pl.plot(self._frontEQE[:,0], self._frontEQE[:,1])
			legendEntries.append("Front EQE")
		if "back" in kwargs and kwargs["back"] == True:
			pl.plot(self._backEQE[:,0], self._backEQE[:,1])
			legendEntries.append("Back EQE")
		
		pl.title(self.name)	
		pl.legend(legendEntries)
	
	def transmitted_light(self, waves):
		r"""
		Calculate the fraction of light at the given wavelengths that are transmitted through this device
		"""
		
		if hasattr(self, "_transmission_function"):
			return self._transmission_function(waves)
		else:
			raise Exception("This device type does not support calculating transmitted light")
	
	def reflected_light(self, waves):
		r"""
		Calculate the fraction of light at the given wavelengths that is reflected from the front interface of this device
		"""
		
		if hasattr(self, "_reflection_function"):
			return self._reflection_function(waves)
		else:
			raise Exception("This device type does not support calculating reflected light.")
		
	def current(self, frontLight = None, backLight = None, voltage=None):
		r"""
		Calculate the photocurrent produced by the device at the specified bias voltage
		when illuminated from the front with frontLight and from the back with backLight.
		
		If frontLight is not specified, AM1.5G is used.  If backLight is not specified, 
		no back illumination is used.  If voltage is not specified, the currently set
		voltage is used.
		
		:param frontLight: Spectrum object containing the illumination on the device from in front
		:param backLight: Spectrum object containing the illumination on the device from behind
		"""
		
		if voltage is not None:
			self.set_voltage(voltage)
			
		if frontLight is None:
			frontLight = solar.types.Spectrum.AM15G()
		
		front_eqe = self.front_eqe()
		back_eqe = self.back_eqe()
		
		photoCurrent = photocurrent(frontLight, front_eqe)
		
		if backLight is not None:
			photoCurrent += photocurrent(backLight, back_eqe)
		
		return photoCurrent
	
	def track_figure(self, fom):
		for dep in fom.depends:
			if dep not in self.figures:
				if dep not in globals():
					raise Exception("Could not add Figure of Merit because it depends on another FOM that was not previously added and could not be found", fom, dep)
				
				#print "Adding hidden dependency %s automatically" % dep
				new_dep = globals()[dep]()
				new_dep.hide()
				self.track_figure(new_dep)
		
		self.figures[fom.__class__.__name__] = fom
	
	def update_figures(self):
		for fom in self.figures.values():
			fom.update(self, self.figures)
			
	def hide_figures(self):
		for fom in self.figures.values():
			fom.hide()
	
	def show_figure(self, figure):
		if figure in self.figures:
			self.figures[figure].show()
	
	def display_figures(self):
		self.update_figures()
		
		for fom in self.figures.values():
			if not fom.hidden:
				fom.display()