#excite.py
#Excitation parameters for controlling pc1d nodes

from solar.types import Spectrum
import PC1DGrid

#Create a PC1D light object containing the AM15G solar spectrum
def OneSun():
	spectrum = Spectrum.AM15G()
	s = spectrum.downsample(200) #downsample to MAXWAVELENGTHS supported by pc1d
	
	light = PC1DGrid.Protocol.ArbitraryLight()
	light.type = PC1DGrid.Protocol.LightType.kArbitrary
	light.position = PC1DGrid.Protocol.LightPosition.kFrontPosition
	light.enabled = True
	light.startingIntensity = 0.1 #.1 W/cm^2
	light.endingIntensity = 0.1
	
	light.wavelengths = s[:,0]
	light.intensities = s[:,1]
	
	return light

def EQELight():
	light = PC1DGrid.Protocol.MonochromaticLight()
	
	light.type = PC1DGrid.Protocol.LightType.kMonochromatic
	light.position = PC1DGrid.Protocol.LightPosition.kFrontPosition
	light.enabled = True
	light.startingIntensity = 0.001
	light.endingIntensity = 0.001
	
	light.startingWavelength = 300
	light.endingWavelength = 1200
	
	return light

def SweepEQE(front = True):
	light  = EQELight()
	
	if front == False:
		light.position = PC1DGrid.Protocol.LightPosition.kBackPosition
	
	baseCircuit = PC1DGrid.Protocol.Circuit()
	colCircuit = PC1DGrid.Protocol.Circuit()
	
	#base circuit
	baseCircuit.arbitrary = False
	baseCircuit.units = PC1DGrid.Protocol.ResistanceUnits.kResistivity
	baseCircuit.startingVoltage = 0.0
	baseCircuit.endingVoltage = 0.0
	baseCircuit.resistance = 0.0 #important that this be zero so that we take points at the voltage specified rather than that minus the voltage drop across the base circuit resistance
	
	#collector circuit
	colCircuit.units = PC1DGrid.Protocol.ResistanceUnits.kResistivity
	colCircuit.startingVoltage = 0.0
	colCircuit.endingVoltage = 0.0
	colCircuit.resistance = 0.0
		
	excite = PC1DGrid.Protocol.TransientExcitation()
	
	excite.baseCircuit = baseCircuit
	excite.collectorCircuit = colCircuit
	excite.type = PC1DGrid.Protocol.ExcitationType.kTransient
	excite.frontLight = light
	
	#So the backLight is not none
	excite.backLight = PC1DGrid.Protocol.MonochromaticLight()
	excite.backLight.type = PC1DGrid.Protocol.LightType.kMonochromatic #not enabled by default
	
	excite.temperature = 298;
	excite.numSteps = 90
	excite.stepLength = 1
	excite.firstStepLength = 1e-9
	
	return excite

def UpconverterLight(inLight, converter):
	light = PC1DGrid.Protocol.ArbitraryLight()
	light.type = PC1DGrid.Protocol.LightType.kArbitrary
	light.position = PC1DGrid.Protocol.LightPosition.kBackPosition
	light.enabled = True
	
	out = converter.upconvertedLight(inLight)
	power = out.totalpower()
	
	out = out.downsample(200)
	
	#PC1D normalizes the spectrum, so we need to pass in the power separately
	light.startingIntensity = power
	light.endingIntensity = power
	
	light.wavelengths = out[:,0]
	light.intensities = out[:,1]
	
	return light

def SteadyCurrent(voltage, priLight, secLight=None):
	baseCircuit = PC1DGrid.Protocol.Circuit()
	colCircuit = PC1DGrid.Protocol.Circuit()
	
	#base circuit
	baseCircuit.arbitrary = False
	baseCircuit.units = PC1DGrid.Protocol.ResistanceUnits.kResistivity
	baseCircuit.startingVoltage = voltage
	baseCircuit.endingVoltage = voltage
	baseCircuit.resistance = 0.0
	
	#collector circuit
	colCircuit.units = PC1DGrid.Protocol.ResistanceUnits.kResistivity
	colCircuit.startingVoltage = 0.0
	colCircuit.endingVoltage = 0.0
	colCircuit.resistance = 0.0
	
	excite = PC1DGrid.Protocol.SteadyStateExcitation()
	
	excite.baseCircuit = baseCircuit
	excite.collectorCircuit = colCircuit
	excite.type = PC1DGrid.Protocol.ExcitationType.kSteadyState
	excite.frontLight = priLight
	if not secLight:
		excite.backLight = PC1DGrid.Protocol.MonochromaticLight()
		excite.backLight.type = PC1DGrid.Protocol.LightType.kMonochromatic #not enabled by default
	else:
		excite.backLight = secLight
		
	excite.temperature = 298;
	
	return excite
	
def SweepIV(priLight, secLight=None):
	baseCircuit = PC1DGrid.Protocol.Circuit()
	colCircuit = PC1DGrid.Protocol.Circuit()
	
	#base circuit
	baseCircuit.arbitrary = False
	baseCircuit.units = PC1DGrid.Protocol.ResistanceUnits.kResistivity
	baseCircuit.startingVoltage = -0.1
	baseCircuit.endingVoltage = 0.8
	baseCircuit.resistance = 0.0 #important that this be zero so that we take points at the voltage specified rather than that minus the voltage drop across the base circuit resistance
	
	#collector circuit
	colCircuit.units = PC1DGrid.Protocol.ResistanceUnits.kResistivity
	colCircuit.startingVoltage = 0.0
	colCircuit.endingVoltage = 0.0
	colCircuit.resistance = 0.0
		
	excite = PC1DGrid.Protocol.TransientExcitation()
	
	excite.baseCircuit = baseCircuit
	excite.collectorCircuit = colCircuit
	excite.type = PC1DGrid.Protocol.ExcitationType.kTransient
	excite.frontLight = priLight
	if not secLight:
		excite.backLight = PC1DGrid.Protocol.MonochromaticLight()
		excite.backLight.type = PC1DGrid.Protocol.LightType.kMonochromatic #not enabled by default
	else:
		excite.backLight = secLight
	excite.temperature = 298;
	excite.numSteps = 32
	excite.stepLength = 1
	excite.firstStepLength = 1e-9
	
	return excite