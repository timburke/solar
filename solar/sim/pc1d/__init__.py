import traceback, Ice
import solar.sim
from os import path

#get directory of slice files relative to cwd
modDir = path.dirname(__file__)
sliceDir = path.join(modDir, "../../slice/pc1d")

#Initialize all of the protocol details
Ice.loadSlice("--all -I\"%s\" \"%s\"/protocol.ice" % (sliceDir, sliceDir))

import PC1DGrid

controller = None
initialized = False

def initialize(host, port, **kw):
	global initialized, controller
	
	if initialized:
		if "silent" not in kw:
			print "Already initialized pc1d_grid and initialize was called again, doing nothing."
		return
	
	timeout = 10000
	if "timeout" in kw:
		timeout = kw["timeout"]
	
	base = solar.sim.ice.stringToProxy("NodeController:tcp -h %s -p %d -t %d" % (host, port, timeout)) 
	controller = PC1DGrid.Control.NodeControllerPrx.checkedCast(base)
	
	if not controller:
		raise RuntimeError("Could not connect to PC1D node running on host:%s, port:%d" % (host, port)) 

	initialized = True
	
#FIXME: Make a way to get rid of Node objects on the server once we're done with them
def getNode():
	global controller
	
	return controller.createNode()