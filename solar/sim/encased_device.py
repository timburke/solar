#encased_device.py

from device import SimulatedDevice
from solar.types import OpticalStack, QuantumEfficiency
from solar import utilities
import numpy as np
from scipy import interpolate

class EncasedDevice (SimulatedDevice):
	def __init__(self, baseDevice, front_stack, back_stack):
		r"""
		Create an EncasedDevice from a simple device defining the EQE for the actual
		solar cell and two OpticalStack objects defining the optical films placed
		in front of and behind the device.  The EQE of the base device is modified by
		the transmission of the front and back stacks of optical films to get the new 
		EQEs for the combined device.
		"""
		
		SimulatedDevice.__init__(self, baseDevice.name, baseDevice.path)
		
		self.baseDevice = baseDevice
		self.front_stack = front_stack
		self.back_stack = back_stack
		
		self._transmission_function = self._build_transmission()
	
	def _build_transmission(self, spacing = 5.):
		waves = np.linspace(400., 4000., (4000. - 400.)/spacing)
		trans = np.ndarray([waves.shape[0], 2])
		trans[:,0] = waves
		
		transFront = self.front_stack.transmission(waves, extend=0.0)
		transBack = self.back_stack.transmission(waves, extend=0.0)
		transDevice = self.baseDevice.transmitted_light(waves)
		
		trans[:,1] = transFront*transBack*transDevice
		
		return utilities.interpolate_array(trans, extend=True)
		
	
	def _modify_eqe(self, eqe, stack, spacing=1.0):
		minX = min(eqe[:,0])
		minX = max(minX, 1.)
		maxX = max(eqe[:,0])
		maxX = min(maxX, 4000.)
		
		waves = np.linspace(minX, maxX, int((maxX-minX)/spacing))
		trans = stack.transmission(waves, extend=0.0) #allow for any wavelength to be used
		
		out = np.ndarray([len(waves), 2])
		
		func = interpolate.interp1d(eqe[:,0], eqe[:,1], bounds_error=False, fill_value=0.0)
		out[:,0] = waves
		out[:,1] = np.vectorize(lambda i: func(waves[i])*trans[i])(xrange(0, len(waves)))
		return out.view(QuantumEfficiency)
	
	def calculate_qes(self):
		self._frontEQE = self._modify_eqe(self.baseDevice.front_eqe(), self.front_stack)
		self._backEQE = self._modify_eqe(self.baseDevice.back_eqe(), self.back_stack.reverse())