import sys, traceback, Ice
from pc1d import PC1DGrid
import os
import csv

class OpaqueDevice:
	def __loadArrayFile(self, filename):
		file = PC1DGrid.Protocol.ExternalFile()
		file.x = []
		file.y = []
		with open(filename, "rb") as f:
			reader = csv.reader(f, delimiter="\t", skipinitialspace=True)
			
			for row in reader:
				file.x.append(float(row[0]))
				file.y.append(float(row[1]))
		
		return file
		
	def __loadDevice(self, filename):
		device = PC1DGrid.Protocol.SimpleDevice()
		
		bytes = []
		
		with open(filename, "rb") as f:
			bytes = f.read()
		
		device.deviceFile = bytes
		
		return device
	
	def __init__(self, deviceFile, otherFiles):
		self.device = self.__loadDevice(deviceFile)
		
		self.device.externalFiles = {}
		
		for file in otherFiles:
			name = os.path.basename(file)
			
			self.device.externalFiles[name] = self.__loadArrayFile(file)
			
	#convert this instance to a PC1DGrid protocol instance that can go on the wire
	def toWire(self):
		return self.device