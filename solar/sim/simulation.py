#simulation.py

#Basic types used to specify devices to be simulated by all the solvers supported by
#this package to allow for uniformly passing parameters and describing devices.

import numpy as np

class SimulationParameters:
	def __init__(self, optical, illumination):
		self.optical = OpticalParameters(optical)
		self.electrical = None
		self.illumination = illumination

class OpticalParameters:
	InternalModel = 1
	ExternalModel = 2
	
	ConstantReflectance = 3
	NonConstantReflectance = 4
	
	r'''
	Superclass for optical parameters being passed to simulations
	'''
	def __init__(self, params):
		self.model = params.model_type()
		self.reflectance = params.reflectance_type()
		self.back_reflectance = params.back_reflectance()
		self.front_reflectance = params.front_reflectance()	
	
class SimpleOpticalParameters:
	def front_reflectance(self):
		return np.vectorize(lambda x: self.front)
	
	def back_reflectance(self):
		return np.vectorize(lambda x: self.back)
	
	def model_type(self):
		return OpticalParameters.InternalModel
	
	def reflectance_type(self):
		return OpticalParameters.ConstantReflectance
	
	def __init__(self, frontRefl, backRefl):
		self.front = frontRefl
		self.back = backRefl
		
class IlluminationParameters:
	def __init__(self):
		self.dark = True
		self.eqe = False
		self.lights = []
	
	def add_light(self, light, from_front = True):
		self.dark = False
		self.lights.append( (light,from_front))
	
	def take_eqe(self, waveStart, waveEnd, spacing):
		self.eqe = True
		self.eqe_start = waveStart
		self.eqe_end = waveEnd
		self.eqe_spacing = spacing
	
	def absolute_light(self, front):
		lights = map(lambda x: x[0], filter(lambda y: y[1]==front, self.lights))
	
		if len(lights) == 0:
			return None
		elif len(lights) == 1:
			return lights[0].toAbsolute()
		else:
			raise Exception("Combining multiple lights is not yet supported.")