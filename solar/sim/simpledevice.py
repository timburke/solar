from solar.sim import SimulatedDevice
import numpy as np
from scipy import integrate
from solar.types import QuantumEfficiency, IdealQuantumEfficiency
from solar.sim.pc1d import pc1ddevice
from solar.sim.wxamps import WxAmpsDevice
from solar import utilities
from solar import reference
import os
import pylab as pl

#A simple device described by an EQE for light incident from the front and back of the device
class SimpleDevice (SimulatedDevice):
	def __init__(self, frontEQE, backEQE=None, name="Unnamed Device", path="(none)"):
		SimulatedDevice.__init__(self, name, path)

		self._transmission_function = np.vectorize(lambda x: 0.0) #Default to having no light transmitted through the device
		self._frontEQE = frontEQE
		
		if backEQE is not None:
			self._backEQE = backEQE
		else:
			self._backEQE = QuantumEfficiency.ZeroQE()
	
		#If we're given an ideal quantum efficiency, define the transmission function as everything
		#below bandgap.
		if isinstance(frontEQE, IdealQuantumEfficiency):
			cutoff = frontEQE.cutoff
			self._transmission_function = np.vectorize(lambda x: SimpleDevice._ideal_transmission(x, cutoff))
	
	@staticmethod
	def _ideal_transmission(x, cutoff):
		if x <= cutoff:
			return 0.0
		else:
			return 1.0
	
	def calculate_qes(self):
		if not np.allclose(0.0, self._voltage):
			raise Exception("SimpleDevice does not allow for bias voltages other than 0 (i.e. short circuit).  Try using a more advanced simulator class if you need this behavior.")
				
	#You must already have initialized PC1D simulator with a valid node location
	#TODO: Add IQE and optical stack calculation to PC1D
	@staticmethod
	def FromPC1DDevice(deviceFile, otherfiles):
		dev = pc1ddevice.PC1DDevice(deviceFile, otherfiles)
		
		frontEQE = dev.eqe(front=True)
		backEQE = dev.eqe(front=False)
		
		transmittedLight = dev.transmitted_light(front=True)
		
		simpleDev = SimpleDevice(frontEQE, backEQE)
		simpleDev._transmission_function = utilities.interpolate_array(transmittedLight, extend=True)
	
		return simpleDev
		
	@staticmethod
	def FromPC1DDirectory(dir):
		files = os.listdir(dir)
		extensions = [".dev", ".inr", ".abs"]
				
		files = filter(lambda x: os.path.splitext(x)[1] in extensions, files)
		
		otherFiles = []
		deviceFile = None
		
		for file in files:
			if os.path.splitext(file)[1] == ".dev":
				deviceFile = os.path.join(dir, file)
			else:
				otherFiles.append(os.path.join(dir,file))
		
		if deviceFile is None:
			raise ValueError("Device file (.dev) not found in directory specified")
		
		return SimpleDevice.FromPC1DDevice(deviceFile, otherFiles)
		
	#You must have already initialized WxAmps simulator
	@staticmethod
	def FromWxAmpsDevice(deviceFile):
		device = WxAmpsDevice(deviceFile)
		
		device.calculate_qes(False)
		frontEQE = device.get_eqe()
		frontIQE = device.get_iqe()
		
		device.calculate_qes(True)
		backEQE = device.get_eqe()
		backIQE = device.get_iqe()
		
		simpleDev = SimpleDevice(frontEQE, backEQE, name=os.path.basename(deviceFile), path=deviceFile)
		simpleDev._frontIQE = frontIQE
		simpleDev._backIQE = backIQE
		
		#stack = device.optical_stack()
		
		#trans = reference.canonical_wavelengths(array=True)
		#refl = reference.canonical_wavelengths(array=True)
		
		#trans[:,1] = stack.transmission(trans[:,0])
		#refl[:,1] = statck.reflection(refl[:,0])
		
		#simpleDev._transmission_function = utilities.interpolate_array(trans, extend=True)
		#simpleDev._reflection_function = utilities.interpolate_array(refl, extend=True)
		#simpleDev._optical_stack = stack
		
		return simpleDev