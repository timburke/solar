#simplesim.py

import solar.reference
import numpy as np
from scipy import integrate
from solar.types import QuantumEfficiency

#Light should be a 2d numpy array with the first column being wavelengths in nm and the second being
#watts/cm^2/nm
def photocurrent(light, eqe):
	"""
	Return an estimate for the short circuit current of the device with the given EQE.
	The current is calculated by integrating the eqe multiplied by the given spectrum.
	
	:param light: the solar spectrum to simulate
	:type light: solar.types.Spectrum
	:param eqe: the EQE of the device to simulate
	:type eqe: solar.types.QuantumEfficiency
	:return: the short circuit current of the device in A/cm^2
	"""
		
	absorbedFlux = eqe.response(light)
		
	return integrate.trapz(absorbedFlux[:,1], absorbedFlux[:,0])*(solar.reference.q)