from solar.sim import SimulatedDevice
from solar import reference
from solar import utilities
from solar.caching import *

import numpy as np

class CombinedDevice(SimulatedDevice):
	r"""
	A simulated device subclass that supports characterizing both optical and electrical properties
	of a given solar cell.  It must be constructed from a SimulatedDevice class that supports:
	front and back iqes
	optical stack
	
	The eqes are determined automatically from the iqes and thin film optical modeling.
	"""
	
	def __init__(self, baseDevice):
		SimulatedDevice.__init__(self, baseDevice.name, baseDevice.path)
		
		#Copy across the relevant properties
		self._frontEQE = baseDevice.front_eqe()
		self._backEQE = baseDevice.back_eqe()
		self._frontIQE = baseDevice.front_iqe()
		self._backIQE = baseDevice.back_iqe()
		
		if not hasattr(baseDevice, "_optical_stack"):
			raise Exception("You can only create a CombinedDevice from a SimulatedDevice that includes an optical stack.")
		
		self._optical_stack = baseDevice._optical_stack.copy()
		self._optical_stack.tag_layers("device") #tag all the device layers so that we can identify parasitic vs non absorption later
	
		self._transmission_function = self._build_transmission()
			
	def _build_transmission(self):
		waves = reference.canonical_wavelengths()
		trans = np.ndarray([waves.shape[0], 2])
		trans[:,0] = waves

		transDevice = self.front_coupling(waves)[:,3]
		
		trans[:,1] = transDevice
		
		return utilities.interpolate_array(trans, extend=True)
	
	def calculate_qes(self):
		#update transmission function as well
		self._transmission_function = self._build_transmission()
				
		front = self.front_coupling(self._frontIQE[:,0])
		back = self.back_coupling(self._backIQE[:,0])
				
		self._frontEQE = self._frontIQE.copy()
		self._backEQE = self._backIQE.copy()
		
		self._frontEQE[:,1] *= front[:,1]
		self._backEQE[:,1] *= back[:,1]
	
	def prepend_layer(self, layer):
		self.modify()
		self._optical_stack.prepend_layer(layer)
	
	def append_layer(self, layer):
		self.modify()
		self._optical_stack.add_layer(layer)
	
	@canonical_spectral_method
	def front_coupling(self, waves):
		r"""
		Calculate the amount of light coming in from the front of the device that is actually coupled into the device
		as a function of wavelength.  If the device does not have a semi-infinite first layer, air is assumed and added
		if the device does not have a semi-infinite back layer, air is again assumed.
		"""
		
		stack = self._optical_stack.copy()
		
		if stack.layers[0].finite():
			stack.prepend_layer('air')
		
		if stack.layers[-1].finite():
			stack.add_layer('air')
		
		res = stack.absorption_in_layers(waves)
		
		refl = res[:,1]
		trans = res[:,-1]
		
		useful = np.zeros_like(refl)
		devLayers = stack.tagged_layers('device')
		
		for l in devLayers:
			useful += res[:, l+1]
				
		parasitic = 1.0 - refl - trans - useful
		
		out = np.ndarray([refl.shape[0], 5])
		
		out[:,0] = waves
		out[:,1] = useful
		out[:,2] = refl
		out[:,3] = trans
		out[:,4] = parasitic
		
		return out		
		
	@canonical_spectral_method
	def back_coupling(self, waves):
		stack = self._optical_stack.reverse()
		
		if stack.layers[0].finite():
			stack.prepend_layer('air')
		
		if stack.layers[-1].finite():
			stack.add_layer('air')
		
		res = stack.absorption_in_layers(waves)
		
		refl = res[:,1]
		trans = res[:,-1]
		
		useful = np.zeros_like(refl)
		devLayers = stack.tagged_layers('device')
		
		for l in devLayers:
			useful += res[:, l+1]
			
		parasitic = 1.0 - refl - trans - useful
		
		out = np.ndarray([refl.shape[0], 5])
		
		out[:,0] = waves
		out[:,1] = useful
		out[:,2] = refl
		out[:,3] = trans
		out[:,4] = parasitic
		
		return out