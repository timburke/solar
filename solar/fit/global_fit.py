#Global Fit class
#Given a data set, perform a robust nonlinear curve fit to a model using the
#either the basin-hopping method or a local minimization method.
#The parameters can be bounded and specified on either a linear or logarithmic
#scale.

import numpy as np
from scipy.optimize import basinhopping, minimize
from scipy.linalg import norm
import sys

class CurveFitter:
	"""
	This object encapsulates routines for fitting data to a nonlinear model in 
	a robust manner using the basin-hopping method to minimize least-squared
	errors between the model prediction and the data.  The data can be resampled
	for uniformity in either x or y, optionally smoothed and the parameters
	to the model can be specified in either linear or logarithmic space.
	"""

	def __init__(self):
		self.params = {}
		self.num_global = 0
		self.num_local = 0

	def load_data(self, *args, **kwargs):
		"""
		Load in a series of data sets that will be fit using this object.  
		resample can be "None" in which case the data is used as is or it can
		be "Uniform Domain" or "Uniform Range"

		resample="None", points=100, copy=True,
		"""

		resample = "None"
		points = 100
		copy = True

		for i in xrange(0, len(args)):
			if args[i].shape[1] != 2:
				raise ValueError("You must pass in a series of Nx2 numpy arrays with x, y values.")

		if resample.lower() not in set(["none", "uniform domain", "uniform range"]):
			raise ValueError("resample must be either [None, Uniform Domain or Uniform Range")

		#Check if we should resample the data
		if resample.lower() != "none":
			raise ValueError("resampling not currently supported")

		if copy:
			self.data = [x.copy() for x in args]
		else:
			self.data = args

	def _next_global_param(self):
		num = self.num_global
		self.num_global += 1
		return num

	def _next_local_param(self):
		num = self.num_local
		self.num_local += 1
		return num

	def set_param(self, name, bounds, guess, type='global', scale='linear'):
		"""
		Add a parameter with the given name and bounds as the next parameter in
		the parameter array.  If type is global then this parameter will use passed
		to fit all of the datasets.  If type is local then a separate copy of this
		parameter will be created for each dataset.
		"""

		param = {}
		param["name"] = name

		if type.lower() == 'global':
			param["position"] = self._next_global_param()
		elif type.lower() == 'local':
			param["position"] = self._next_local_param()
		else:
			raise ValueError("type must be either global or local")

		param["type"] = type.lower()

		if scale.lower().startswith("log"):
			param["guess"] = np.log(guess)
			param["bounds"] = [np.log(x) for x in bounds]
			param['log']= True
		elif scale.lower() == 'linear':
			param["guess"] = guess
			param["bounds"] = [x for x in bounds]
			param['log'] = False
		else:
			raise ValueError("Only log and linear scales are supported for parameters")

		self.params[name] = param

	def _prepare_params(self):
		global_ordered = sorted(filter(lambda x: x["type"] == 'global', self.params.values()), key=lambda x: x['position'])
		local_ordered = sorted(filter(lambda x: x["type"] == 'local', self.params.values()), key=lambda x: x['position'])

		ordered = global_ordered + local_ordered

		for i in xrange(0, len(ordered)):
			v = ordered[i]
			dist = v['bounds'][1] - v['bounds'][0]

			if dist <= 0.0:
				raise ValueError("Upper bound is not greater than lower bound for parameter %s: %s" % (v['name'], str(v['bounds'])))

			v['scale'] = 1.0/dist
			v['offset'] = v['bounds'][0]
			v['scaled_guess'] = (v['guess'] - v['offset'])/dist

		self.param_list = ordered

	def convert_params(self, params):
		"""
		Given a set of parameters in scaled units, convert them to absolute units for use
		in calculations.
		"""

		p = params.copy()

		for i in xrange(0, len(p)):
			pdesc = self.param_list[i]
			p[i] = p[i]/pdesc['scale'] + pdesc['offset']
			if pdesc['log']:
				p[i] = np.exp(p[i])

		return p

	def num_params(self):
		return len(self.params)

	def set_model(self, model, gradient=None):
		self.model = model
		self.gradient = gradient

	def _callback(self, x, f, accept):
		self.run_log.append((self.convert_params(x), f, accept))

		if self.verbose:
			sys.stdout.write("Iteration %d: %f %s\n" % (len(self.run_log), f, str(self.convert_params(x))))
			sys.stdout.flush()

		#Do not stop the search
		return False

	def _split_params(self, params):
		"""
		Given a set of parameters that specify different carrier densities for different
		temperature series, return a list of parameter sets for each series.
		"""
		numsets = len(self.data)

		if numsets == 1:
			return [params]

		psets = [np.copy(params[:-(numsets-1)]) for i in xrange(0, numsets)]

		for i in xrange(0, numsets):
			psets[i][-1] = params[-(numsets-i)]

		return psets

	def _build_scaled_objective(self):
		"""
		Given the currently defined model, build an objective function
		that computes the least-squared error between the model and each
		data set
		"""

		def obj(params, *args):
			if len(params) != self.num_params():
				raise ValueError("Incorrect number of parameters passed to objective")

			scaled_params = self.convert_params(params)
			psets = self._split_params(scaled_params)

			val = 0
			for i in xrange(0, len(self.data)):
				pred = self.model(self.data[i][:,0], *psets[i])
				val += np.linalg.norm(pred - self.data[i][:,1])

			return val

		return obj

	def _convert_gradient(self, param, grad):
		for i in xrange(0, len(grad)):
			pdesc = self.param_list[i]
			if pdesc['log'] is True:
				grad[i] = param[i]*grad[i]
			
			grad[i] /= pdesc['scale']

	def _build_compute_object(self):
		"""
		Given the parameters defined in this Global Fit object, create an objective function suitable
		for global minimization that will find the optimal sets of parameters
		"""

		def compute_value(*args):
			params = self.convert_params(args[0])
			psets = self._split_params(params)
			vals = [objs[i](psets[i], *args[1:]) for i in xrange(0, len(objs))]
			
			return sum(vals)

		def compute_gradient(*args):
			params = self.convert_params(args[0])
			psets = self._split_params(params)

			comb_grad = np.zeros_like(params)
			gradvals = [grads[i](psets[i], *args[1:]) for i in xrange(0, len(grads))]

			#Compute the gradient in scaled coordinates
			numsets = len(self.data)
			if numsets == 1:
				self._convert_gradient(psets[0], gradvals[0])
				return gradvals[0]

			for i in xrange(0, len(grads)):
				comb_grad[:-(numsets-1)] += gradvals[i]

			#Correct the last values
			for i in xrange(0, len(grads)):
				comb_grad[-(numsets-i)] = gradvals[i][-1]

			self._convert_gradient(params, comb_grad)

			return comb_grad

		return compute_value, compute_gradient

	def _build_minimizer_args(self, maxiter=200):
		minargs = {}
		minargs['method'] = 'TNC'
		minargs['bounds'] = [(0,1) for x in self.param_list]
		minargs['options'] = {'maxiter': maxiter}

		return minargs

	def search_local(self, maxiter=200, verbose=True):
		self.verbose = verbose
		self._prepare_params()

		obj = self._build_scaled_objective()

		guess = np.array([x['scaled_guess'] for x in self.param_list])

		minargs = self._build_minimizer_args(maxiter)

		#Minimize, returning 
		res = minimize(obj, guess, **minargs)
		params = self.convert_params(res.x)
		return params, res

	def search_global(self, verbose=True, T=0.1, stepsize=0.05, clear=True, maxiter=1):
		self.verbose = verbose
		self._prepare_params()

		if clear:
			self.run_log = []		

		c = self._build_scaled_objective()
		guess = np.array([x['scaled_guess'] for x in self.param_list])

		minargs = self._build_minimizer_args()
		#minargs['jac'] = g

		res = basinhopping(c, guess, stepsize=stepsize, niter=maxiter, callback=self._callback, minimizer_kwargs=minargs, T=T)

		return res