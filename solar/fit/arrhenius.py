import numpy as np
from linear import linear_fit

kb = 8.6173324e-5

def arrhenius_fit(data):
	"""
	Given a temperature data series, perform an arrhenius fit
	to the data, returning the prefactor and activation energy
	in eV. 

	Returns:
	(activation energy (ev), prefactor, fit statistics)
	"""

	x = 1.0/(kb*data[:,0])
	y = np.log(data[:,1])

	act, pre, stats = linear_fit(x,y)

	return -act, np.exp(pre), stats
