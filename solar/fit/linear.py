import numpy as np
from scipy.optimize import curve_fit
from scipy.stats import linregress

def linear_fit(data, y=None):
	"""
	Compute a least-squares linear fit to the data
	"""

	if y is None:
		m, b, r, p, std_err = linregress(data[:,0], data[:,1])
	else:
		m, b, r, p, std_err = linregress(data, y)

	stats = {
		"r": r,
		"p": p,
		"std_err": std_err
		}

	return m, b, stats

def loglinear_fit(data, y=None, guess=[1., 0]):
	"""
	Compute a least-squares linear fit to the data on a loglog plot
	"""

	if y is None:
		x = data[:,0]
		y = data[:,1]
	else:
		x = data

	logy = np.log(y)

	def fit_func(x, m, b):
		return np.log(m*x + b)

	res = curve_fit(fit_func, x, logy, guess)
	return res.x[0], res.x[1]