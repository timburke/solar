from scipy.interpolate import UnivariateSpline
import numpy as np

def smooth(x,y, **kw):
	"""
	Return a spline that interpolates the the provided data.
	Any keyword arguments are passed on to the UnivariateSpline function
	"""

	return UnivariateSpline(x,y, **kw)

def resample_uniform_y(x,y, points=100, **kw):
	"""
	Given a dataset that is not uniformly sampled in the y direction,
	resample it with a given number of uniformly spaced points in y.
	"""

	if 's' not in kw:
		kw['s'] = 0

	interp = smooth(y,x, **kw)

	minv = np.min(y)
	maxv = np.max(y)

	res = np.ndarray([points, 2])
	res[:,1] = np.linspace(minv, maxv, points)
	res[:,0] = interp(res[:,1])

	return res

def resample_uniform_x(x,y, points=100, max_attempts=100, **kw):
	"""
	Given a dataset that is not uniformly sampled in the x direction,
	resample it with a given number of uniformly spaced points in x.
	"""

	if 's' not in kw:
		kw['s'] = 0
	

	minv = np.min(x)
	maxv = np.max(x)

	res = np.ndarray([points, 2])
	res[:,0] = np.linspace(minv, maxv, points)

	s = kw['s']
	kwargs = kw.copy()
	for i in xrange(0, max_attempts):
		kwargs['s'] = s
		interp = smooth(x, y, **kwargs)

		res[:,1] = interp(res[:,0])
		if not np.isnan(res[:,1]).any():
			break

		s += 0.1

	if s != kw['s']:
		print "WARNING: Could not spline-fit the data using the provided (or default) s value"
		print "Autofound an s value that worked: s=%s" % s
		print "Check and make sure that the fit is good"

	return res
