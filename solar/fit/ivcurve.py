from numpy.linalg import norm
import numpy as np
from scipy.optimize import curve_fit, minimize
from spline import resample_uniform_x
from scipy.weave import inline
from scipy.weave.converters import blitz

def simple_fitter(w, A, B, Jph):
    C = np.zeros_like(w)
    n = w.shape[0]
    A = float(A)
    B = float(B)
    Jph = float(Jph)
    
    code = \
    r"""
    #line 12

    double A2 = A*A;
    double B2 = B*B;
    for (int i=0; i<n; ++i)
    {
        double temp = w(i);
        C(i) = A2*temp - Jph*(temp*temp*temp) + B2;
    }
    """

    inline(code, ['w', 'A', 'B', 'Jph', 'C', 'n'], type_converters=blitz, compiler='gcc')
    return C
    #return A*A*w - Jph*(w**3) + B*B

def simple_fitter_noB(w, A, Jph):
    return A*A*w - Jph*(w**3)

def simple_fitter_noA(w, B, Jph):
    return -Jph*(w**3) + B*B

def full_fitter(v, y, A, B, Jph, Vbi, Ect_coeff, Rs):
    """
    Given a value for A, B, Jph, Vbi, Ect_coeff, Rs, calculate the 
    residual between the fit for that parameter set and the dataset x,y.

    The residual is calculated in a robust way by not dividing by x-Vbi
    so that we don't blow up when Vbi is chosen too low.
    """

    T = 300.
    k = 8.617e-5
    kT = k*T

    x = v - y*Rs

    w = Vbi - x

    #Ect_coeff = Ect_coeff*Ect_coeff

    try:
        t1 = np.exp((x-Ect_coeff)/kT)
    except FloatingPointError:
        print "Ect was", Ect_coeff
        print "Rs was", Rs
        print "Min x was", min(x)
        print "Max x was", max(x)
    
    f = simple_fitter(w, A, B, Jph)
    resid = norm((y - t1)*w*w*w - f)/len(f)
    return resid

def simple_fit(iv, Vbi, Rs, type='both'):
    if type == 'both':
        fitfun = simple_fitter
        guess = [-min(iv[:,1]), 0., 0.]
    elif type == 'no A':
        fitfun = simple_fitter_noA
        guess = [-min(iv[:,1]), 0.]
    elif type == 'no B':
        fitfun = simple_fitter_noB
        guess = [-min(iv[:,1]), 0.]
    else:
        raise ValueError("Unknown type: %s", type)
    
    w = Vbi - (iv[:,0] - iv[:,1]*Rs)
    y = iv[:,1]*(w**3)
    
    opt, cov = curve_fit(fitfun, w, y, guess)
    return opt

def get_A_from_voc(iv):
    j, v = find_voc(iv)
    T = 300.
    k = 8.617e-5
    kT = k*T
    
    return np.exp(-v/kT)

def find_j(iv, j):
    if j > iv[-1, 1]:
        return iv.shape[0]
    
    return np.argmax(iv[:,1]>j)

def find_voc(iv):
    i = find_j(iv, 0.0)
    j1 = iv[i-1, 1]
    j2 = iv[i, 1]
    
    dx = iv[i,0] - iv[i-1,0]
    dy = j2 - j1
    
    return i-1, iv[i-1,0] + (0. - j1)*dx/dy

def fit_iv_curve(orig_iv, Vbi_range=[0.5, 5.0], Rs_range=[0.0, 0.03], Voc_range=[0.5, 1.5], points=50, start=0, stop=-10, T=300., type='both'):
    k = 8.617e-5
    kT = k*T

    iv = resample_uniform_x(orig_iv[:,0], orig_iv[:,1])
    exp_corrected = iv.copy()

    vbis = np.linspace(Vbi_range[0], Vbi_range[1], points)
    Rss = np.linspace(Rs_range[0], Rs_range[1], points)
    Vocs = np.linspace(Voc_range[0], Voc_range[1], points)

    res = []
    paramsets = []
    for i in xrange(0, points):
        for j in xrange(0, points):
            for k in xrange(0, points):
                Vbi = vbis[i]
                Rs = Rss[j]
                Voc = Vocs[k]

                x = (iv[:,0] - iv[:,1]*Rs)
                t1 = np.exp((x-Voc)/kT)

                exp_corrected[:,1] = iv[:,1] - t1
                
                try:
                    params = simple_fit(exp_corrected[start:stop,:], Vbi, Rs, type=type)
                except:
                    continue

                w = Vbi - x

                f = simple_fitter(w, *params)
                resid = norm(iv[:,1] - t1 - f/w**3)/len(f)
                res.append(resid)
                paramsets.append([x for x in params] + [Vbi, Rs, Voc])
    
    info = {}
    info['residuals'] = res
    info['psets'] = paramsets
    info['series'] = Rss
    info['vbis'] = vbis
    info['resampled'] = iv
    
    best_i = np.argmin(res)
    best_p = paramsets[best_i]
    rs = best_p[-2]
    vbi = best_p[-3]
    voc = best_p[-1]
    
    #Save the best IV curve
    gen = iv.copy()
    gen[:,0] = iv[:,0] - rs*iv[:,1]
    
    x = (iv[:,0] - iv[:,1]*rs)
    t1 = np.exp((x-voc)/kT)

    w = vbi - x
    f = simple_fitter(w, *best_p[:-3])
    gen[:,1] = t1 + f/w**3
    info['generated'] = gen
    info['rs'] = rs
    info['rs_i'] = rs
    info['vbi_i'] = vbi
    info['voc_i'] = voc
    
    #Return the square of the parameters as is done in the fit function
    #to force them to be positive.
    full_pset = [best_p[2], best_p[0]**2, best_p[1]**2, voc, vbi, rs]
    info['best_resid'] = res[best_i]
    return full_pset, info

def iterated_fit(orig_iv, points=20, start=0, stop=-10, refine=1, Rs_range=[0.0, 0.03], Vbi_range=[0.5, 5.0], Voc_range=[0.5, 1.5]):
    for i in xrange(0, refine):
        full_pset, info = fit_iv_curve(orig_iv, Vbi_range=Vbi_range, Rs_range=Rs_range, Voc_range=Voc_range,
                     points=points, start=start, stop=stop)
        
        vbi_i = info['vbi_i']
        rs_i = info['rs_i']
        
        if (vbi_i == points-1) or (rs_i == points-1) or vbi_i == 0 or rs_i==0:
            break
        
        Vbi_range = [info['vbis'][vbi_i-1], info['vbis'][vbi_i+1]]
        Rs_range = [info['series'][rs_i-1], info['series'][rs_i+1]]
    
    return full_pset, info

def optimized_fit(orig_iv, method='TNC'):
    guess = [0., 0., 10, 1.0, 1.0, 0.0]
    bounds = [(0., 10), (0, 10.), (0.0, 20.0), (0.5, 5.0), (0.5, 5.0), (0., 0.03)]
    np.seterr(over='raise')

    local_fitter = lambda args: full_fitter(orig_iv[:,0], orig_iv[:,1], *args)
    return minimize(local_fitter, guess, bounds=bounds, method=method, options={'maxiter': 1000})

