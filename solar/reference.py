#reference.py
#Store and retrieve constant values of interest for solar research
#For example, the AM1.5 solar spectrum

import os.path
import numpy as np
import types
import math
from scipy import interpolate, fftpack

q = 1.60217646e-19
c = 2.99792e8
h = 6.626068e-34
canonical_start = 300.
canonical_end = 4000.
canonical_sampling = 5.0 #sample every 5 nm
canonical_wavecount = int((canonical_end - canonical_start)/canonical_sampling)

def ev_to_nm(ev):
	r"""
	Convert a photon energy in electron volts to the corresponding wavelength in nm according to the 
	standard formula:
	
	.. math::
	   \lambda = \frac{h c}{E}
	
	where h is planck's constant and c is the speed of light in appropriate units.
	
	:param ev: the energy to convert in electron volts
	:return: the corresponding wavelength in nanometers
	"""
	global h, c
	joules = ev_to_joules(ev)
	
	if joules > 0.0:
		return h*c/joules*(10.**9)
	else:
		return 0.0
		
#call with wavelength in nanometers
def wavelengthToJoules(wave):
	global h,c 
	
	return h*c/(wave*10.**-9)

def gaussian(x, center, width, max):
	r"""
	Returns the value of a gaussian distribution with the given parameters sampled at point x.  
	The returned value is calculated as:
	
	.. math::
		y = max * exp\left(-\frac{(x-center)^2}{2*{width}^2}\right)
		
	:param center: the gaussian peak location
	:param x: the value at which to evaluate the distribution
	:param width: the halfwidth of the gaussian at its inflection points
	:param max: the peak value of the gaussian
	"""
	return max * np.exp( -1.0 * np.square(x - center) / (2 * width*width) )

def unit_gaussian(x, center, width):
	r"""
	Returns the value of a gaussian distribution with unit area of a given width.  The parameters 
	are the same as :func:`gaussian`.  Internally, the gaussian's peak height is calculated as:
	
	.. math::
		max = \frac{1}{width*\sqrt{2 \pi}}
	
	so that the following is true:
	
	.. math::
		\int_{-\infty}^{\infty} \text{unit_gaussian}(x, center, width) \mathrm{dx} = 1
	
	:param center: the gaussian peak location
	:param x: the value at which to evaluate the distribution
	:param width: the halfwidth of the gaussian at its inflection points
	"""
	return gaussian(x, center, width, 1.0 / (width*math.sqrt(2*3.141592)))

#wavelength should be in nanometers
def powerToFlux(power, wavelength):
	global h,c
	
	if wavelength == 0.0:
		return 0.0
		
	photonEnergy = h*c/(wavelength*10.**-9)
	
	return power/photonEnergy

def vToFlux(light):
	global h,c
	#photonEnergy = h*c/(wavelength*10.**-9)
	
	arr = light.copy()
	arr[:,1] = arr[:,1] / (h*c) * arr[:,0]*10.**-9
	
	return arr

def vToFluxInPlace(arr):
	global h,c 
	hc = h*c
	nm = 10.**-9
	
	arr[:,1] = arr[:,1] / (hc) * arr[:,0]*nm

def vToPower(light):
	global h,c
	
	arr = light.copy()
	arr[:,1] = arr[:,1] * (h*c) / (arr[:,0]*10.**-9)
	
	return arr
	
def vToPowerInPlace(arr):
	global h,c
	
	hc = h*c
	nm = 10.**-9
	
	arr[:,1] = arr[:,1] * (hc) / (arr[:,0]*nm)
	
	return arr
	
#operates on a vector with two elements, the first being the wavelength and the second being the power
def toFlux(a):
	a[1] = powerToFlux(a[1], a[0])
	
	return a

def toPower(a):
	a[1] = fluxToPower(a[1], a[0])
	
	return a
	
def fluxToPower(flux, wavelength):
	global h, c
	
	photonEnergy = h*c/(wavelength*10.**-9)
	
	return flux*photonEnergy

def ev_to_joules(ev):
	r"""
	Convert the argument in electron volts to joules
	"""
	
	global q
	return ev*q
	
def canonical_wavelengths(array=False):
	r"""When some function needs to be canonically sampled over the wavelengths of interest to this module,
	we need to make a decision about how to do that and this function makes sure that decision is made uniformly.
	"""
	global canonical_start, canonical_end, canonical_wavecount
	
	waves = np.linspace(canonical_start, canonical_end, canonical_wavecount)

	if array:
		out = np.ndarray([canonical_wavecount, 2])
		out[:,0] = waves
		return out
	else:
		return waves

def am15_tilt():
	r"""
	Return the AM1.5G solar spectrum as a 2D array of samples, where each point is the wavelength at which
	the sample in nanometers was taken and the spectral power density in Watts/cm^2/nm.  
	"""
	
	tiltfile = os.path.join(os.path.dirname(__file__), "reference_data", "am15_tilt.csv")
	
	return np.genfromtxt(tiltfile, skip_header=1, delimiter=",")