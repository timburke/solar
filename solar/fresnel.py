# -*- coding: utf-8 -*-
"""

Created on Sun Feb 12 17:20:52 2012

@author: Steve Byrnes
"""

from numpy import cos, inf, zeros, array, exp, conj

import scipy as sp
import numpy as np

def snell(n_1,n_2,th_1):
    """
    return angle theta in layer 2 with refractive index n_2, assuming
    it has angle th_1 in layer with refractive index n_1. Use Snell's law. Note that
    "angles" may be complex!!
    """
    #Important that the arcsin here is scipy.arcsin, not numpy.arcsin!! (They
    #give different results e.g. for arcsin(2).)
    #Use real_if_close because e.g. arcsin(2 + 1e-17j) is very different from
    #arcsin(2) due to branch cut
    return sp.arcsin(np.real_if_close(n_1*np.sin(th_1) / n_2))

def list_snell(n_list,th_0):
    """
    return list of angle theta in each layer based on angle th_0 in layer 0,
    using Snell's law. n_list is index of refraction of each layer. Note that
    "angles" may be complex!!
    """
    #Important that the arcsin here is scipy.arcsin, not numpy.arcsin!! (They
    #give different results e.g. for arcsin(2).)
    #Use real_if_close because e.g. arcsin(2 + 1e-17j) is very different from
    #arcsin(2) due to branch cut
    return sp.arcsin(np.real_if_close(n_list[0]*np.sin(th_0) / n_list))

def interface_r(polarization, n_i, n_f, th_i, th_f):
    """
    reflection amplitude
    
    polarization is either "s" or "p" for polarization
    
    n_i, n_f are (complex) refractive index for incident and final
    
    th_i, th_f are (complex) propegation angle for incident and final
    (in radians, where 0=normal). "th" stands for "theta".
    """
    if polarization == 's':
        #return 2 * n_i * cos(th_i) / (n_i * cos(th_i) + n_f * cos(th_f))
        return ((n_i * cos(th_i) - n_f * cos(th_f)) /
                (n_i * cos(th_i) + n_f * cos(th_f)))
    elif polarization == 'p':
        return ((n_f * cos(th_i) - n_i * cos(th_f)) /
                (n_f * cos(th_i) + n_i * cos(th_f)))
    else:
        raise ValueError("Polarization must be 's' or 'p'")

def interface_t(polarization, n_i, n_f, th_i, th_f):
    """
    transmission amplitude

    polarization is either "s" or "p" for polarization

    n_i, n_f are (complex) refractive index for incident and final

    th_i, th_f are (complex) propegation angle for incident and final
    (in radians, where 0=normal). "th" stands for "theta".    
    """
    if polarization == 's':
        return 2 * n_i * cos(th_i) / (n_i * cos(th_i) + n_f * cos(th_f))
    elif polarization == 'p':
        return 2 * n_i * cos(th_i) / (n_f * cos(th_i) + n_i * cos(th_f))
    else:
        raise ValueError("Polarization must be 's' or 'p'")

def R_from_r(pol, r, n_i, th_i):
    """
    Calculate reflected power R, starting with reflection amplitude r.
    
    n_i is refractive index of incident medium.
    
    th_i is (complex) propegation angle through incident medium
    (in radians, where 0=normal). "th" stands for "theta".    

    In the case that n_i and th_i are real, formulas simplify to R=|r|^2.
    
    See manual for discussion of formulas
    """
    if(pol=='s'):
        return 1 - ((n_i*cos(th_i)*(1+conj(r))*(1-r)).real
                     / (n_i*cos(th_i)).real)
    elif(pol=='p'):
        return 1 - ((n_i*conj(cos(th_i))*(1+r)*(1-conj(r))).real
                      / (n_i*conj(cos(th_i))).real)
    else:
        raise ValueError("Polarization must be 's' or 'p'")

def T_from_t(pol, t, n_i, n_f, th_i, th_f):
    """
    Calculate transmitted power T, starting with transmission amplitude t.
    
    n_i,n_f are refractive indices of incident and final medium.
    
    th_i, th_f are (complex) propegation angles through incident & final medium
    (in radians, where 0=normal). "th" stands for "theta".

    In the case that n_i,n_f,th_i,th_f are real, formulas simplify to
    T=|t|^2 * (n_f cos(th_f)) / (n_i cos(th_i)).
    
    See manual for discussion of formulas
    """
    if(pol=='s'):
        return abs(t**2) * (((n_f*cos(th_f)).real) / (n_i*cos(th_i)).real)
    elif(pol=='p'):
        return abs(t**2) * (((n_f*conj(cos(th_f))).real) /
                                (n_i*conj(cos(th_i))).real)
    else:
        raise ValueError("Polarization must be 's' or 'p'")

def interface_T(polarization, n_i, n_f, th_i, th_f):
    """
    Fraction of light intensity transmitted at an interface.
    """
    t = interface_t(polarization,n_i,n_f,th_i,th_f)    
    return T_from_t(polarization,t,n_i,n_f,th_i,th_f)

def interface_R(polarization, n_i, n_f, th_i, th_f):
    """
    Fraction of light intensity reflected at an interface.
    """
    r = interface_r(polarization,n_i,n_f,th_i,th_f)    
    return R_from_r(polarization,r,n_i,th_i)

def fresnel_main(pol, n_list, d_list, th_0, lam_vac):
    """
    Main fresnel calc. Given parameters of a stack, calculates everything you
    could ever want to know about how light propagates in it. (If performance
    is an issue, you can delete some of the calculations without affecting the
    rest.)
    
    pol is light polarization, "s" or "p".
    
    n_list is the list of refractive indices, in the order that the light would
    pass through them. The 0'th element of the list should be the semi-infinite
    medium from which the light enters, the last element should be the semi-
    infinite medium to which the light exits (if any exits).
    
    th_0 is the angle of incidence 0 for normal, pi/2 for glancing.
    Remember, for a dissipative incoming medium (n_list[0] is not real), th_0
    should be complex so that n0 sin(th0) is real (intensity is constant as
    a function of lateral position).
    
    d_list is the list of layer thicknesses (front to back). Should correspond
    one-to-one with elements of n_list. First and last elements should be "inf".
    
    lam_vac is vacuum wavelength of the light.
    
    Outputs r, t, R, T, vw_list, kz_list, th_list, pol, n_list, d_list, th_0,
    lam_vac, as a dictionary.
    
    [To do: Save the info in a class instead of a dictionary.]
    
    """
    #convert lists to numpy arrays if they're not already. Make sure there are
    #no integers. (Adding 0.0 kills integer type for both real and complex.)
    n_list=array(n_list)+0.0
    d_list=array(d_list,dtype=float)
    
    #input tests
    if (n_list.ndim != 1) | (d_list.ndim != 1) | (n_list.size) != (d_list.size):
        raise ValueError("Problem with n_list or d_list!")
    if (d_list[0] != inf) | (d_list[-1] != inf):
        raise ValueError('d_list must start and end with inf!')
    if (np.real_if_close(n_list[0]*np.sin(th_0))).imag != 0:
        raise ValueError('Error in n0 or th0!')
    num_layers = n_list.size
    
    #th_list is a list with, for each layer, the angle that the light travels
    #through the layer. Computed with Snell's law. Note that the "angles" may be
    #complex!
    th_list = list_snell(n_list,th_0)
    
    #kz is the z-component of (complex) angular wavevector for forward-moving
    #wave.
    kz_list = 2 * np.pi * n_list * cos(th_list) / lam_vac
    
    #delta is the total phase accrued by traveling through a given layer.
    #ignore warning about inf multiplication
    olderr = sp.seterr(invalid= 'ignore')
    delta = kz_list * d_list
    sp.seterr(**olderr)
    #t_list[i,j] and r_list[i,j] are transmission and reflection amplitudes,
    #respectively, coming from i, going to j. Only need to calculate this when
    #j=i+1. (2D array is overkill but helps avoid confusion.)
    t_list = zeros((num_layers,num_layers),dtype=complex)
    r_list = zeros((num_layers,num_layers),dtype=complex)
    for i in xrange(num_layers-1):
        t_list[i,i+1] = interface_t(pol,n_list[i],n_list[i+1],
                                     th_list[i],th_list[i+1])
        r_list[i,i+1] = interface_r(pol,n_list[i],n_list[i+1],
                                     th_list[i],th_list[i+1])
    #At the interface between the (n-1)st and nth material, let v_n be the
    #amplitude of the wave on the nth side heading forwards (away from the
    #boundary), and let w_n be the amplitude on the nth side heading backwards
    #(towards the boundary). Then (v_n,w_n) = M_n (v_{n+1},w_{n+1}). M_n is
    #M_list[n]. M_0 and M_{num_layers-1} are not defined.
    #My M is a bit different than Sernelius's, but Mtilde is the same.
    M_list = zeros((num_layers,2,2),dtype=complex)
    for i in xrange(1,num_layers-1):
        M_list[i] = (1/t_list[i,i+1]) * np.dot(
            array([[exp(-1j*delta[i]),0],[0,exp(1j*delta[i])]]),
            array([[1,r_list[i,i+1]],[r_list[i,i+1],1]]))
                    
    Mtilde = array([[1,0],[0,1]], dtype=complex)
    for i in xrange(1,num_layers-1):
        Mtilde = np.dot(Mtilde,M_list[i])
        
    Mtilde = np.dot(array([[1,r_list[0,1]],[r_list[0,1],1]])/t_list[0,1],
                    Mtilde)

    #Net complex transmission and reflection amplitudes
    r=Mtilde[1,0]/Mtilde[0,0]
    t=1/Mtilde[0,0]
    
    #vw_list[n] = [v_n, w_n]. v_0 and w_0 are undefined because the 0th medium
    #has no left interface.
    vw_list=zeros((num_layers,2), dtype=complex)
    vw = array([[t],[0]])
    vw_list[-1,:] = np.transpose(vw)
    for i in xrange(num_layers-2,0,-1):
        vw = np.dot(M_list[i], vw)
        vw_list[i,:] = np.transpose(vw)
    
    #Net transmitted and reflected power, as a proportion of the incoming light
    #power.
    R = R_from_r(pol, r, n_list[0], th_0)
    T = T_from_t(pol, t, n_list[0], n_list[-1], th_0, th_list[-1])

    return {'r': r, 't': t, 'R': R, 'T': T, 'vw_list': vw_list,
            'kz_list': kz_list, 'th_list': th_list, 'pol': pol,
            'n_list': n_list, 'd_list': d_list, 'th_0': th_0,
            'lam_vac':lam_vac,'transfer':Mtilde}

def fresnel_reverse(pol, n_list, d_list, th_0, lam_vac):
    """
    Reverses the order of the stack then runs fresnel_main.
    """
    th_f = snell(n_list[0],n_list[-1],th_0)
    return fresnel_main(pol,n_list[::-1],d_list[::-1],th_f,lam_vac)

def ellips(n_list, d_list, th_0, lam_vac):
    """
    Calculates ellipsometric parameters, in radians.
    
    Warning: Conventions differ. You may need to subtract pi/2 or whatever.
    """
    
    s_data=fresnel_main('s',n_list, d_list, th_0, lam_vac)
    p_data=fresnel_main('p',n_list, d_list, th_0, lam_vac)
    rs = s_data['r']
    rp = p_data['r']
    return {'psi': np.arctan(abs(rp/rs)), 'Delta': np.angle(-rp/rs)}

def unpolarized_RT(n_list, d_list, th_0, lam_vac):
    """
    Calculates reflected and transmitted power for unpolarized light.
    """
    
    s_data = fresnel_main('s',n_list, d_list, th_0, lam_vac)
    p_data = fresnel_main('p',n_list, d_list, th_0, lam_vac)
    R = (s_data['R'] + p_data['R']) / 2.
    T = (s_data['T'] + p_data['T']) / 2.
    return {'R': R, 'T': T}

def position_resolved(layer, dist, fresnel_data):
    """
    Starting with output of fresnel_main(), calculate the Poynting vector
    and absorbed energy density a distance "dist" into layer number "layer"
    """
    vw = fresnel_data['vw_list'][layer]
    kz = fresnel_data['kz_list'][layer]
    th = fresnel_data['th_list'][layer]
    n = fresnel_data['n_list'][layer]
    n_0 = fresnel_data['n_list'][0]
    th_0 = fresnel_data['th_0']
    pol = fresnel_data['pol']
    
    #amplitude of forward-moving wave is Ef, backwards is Eb
    Ef = vw[0] * exp(1j * kz * dist)
    Eb = vw[1] * exp(-1j * kz * dist)
    
    #Poynting vector
    if(pol=='s'):
        poyn = ((n*cos(th)*conj(Ef+Eb)*(Ef-Eb)).real) / (n_0*cos(th_0)).real
    elif(pol=='p'):
        poyn = (((n*conj(cos(th))*(Ef+Eb)*conj(Ef-Eb)).real)
                    / (n_0*conj(cos(th_0))).real)
    
    #absorbed energy density
    if(pol=='s'):
        absor = (n*cos(th)*kz*abs(Ef+Eb)**2).imag / (n_0*cos(th_0)).real
    elif(pol=='p'):
        absor = (n*conj(cos(th))*
                 (kz*abs(Ef-Eb)**2-conj(kz)*abs(Ef+Eb)**2)
                ).imag / (n_0*conj(cos(th_0))).real
    
    return({'poyn':poyn, 'absor':absor})

def find_in_structure(d_list,dist):
    """
    d_list is list of thicknesses of layers, all of which are finite.
    
    dist is the distance from the front of the whole multilayer structure
    (i.e., from the start of layer 0.)
    
    Function returns [layer,z], where:
        
    layer is what number layer you're at.
    (For large enough dist, layer = len(d_list), even though d_list[layer]
    doesn't exist in that case.
    
    z is the distance into that layer.
    """
    if sum(d_list) == inf:
        raise ValueError('This function expects finite arguments')
    layer=0
    while (layer < len(d_list)) and (dist >= d_list[layer]):
        dist -= d_list[layer]
        layer += 1
    return [layer,dist]

def find_in_structure_with_inf(d_list,dist):
    """
    d_list is list of thicknesses of layers [inf, blah, blah, ..., blah, inf]
    
    dist is the distance from the front of the whole multilayer structure
    (i.e., frcom the start of layer 1.)
    
    Function returns [layer,z], where:
        
    layer is what number layer you're at,
    
    z is the distance into that layer.
    """
    [layer,dist] = find_in_structure(d_list[1:-1],dist)
    return [layer+1,dist]

def layer_starts(d_list):
    """
    Gives the location of the start of any given layer, relative to the front
    of the whole multilayer structure. (i.e. the start of layer 1)
    
    d_list is list of thicknesses of layers [inf, blah, blah, ..., blah, inf]

    """
    final_answer = zeros(len(d_list))
    final_answer[0] = -inf
    final_answer[1] = 0
    for i in xrange(2,len(d_list)):
        final_answer[i] = final_answer[i-1] + d_list[i-1]
    return final_answer

def find_absorp_analytic_fn(layer, fresnel_data):
    """
    Absorption in a given layer is a pretty simple analytical function:
    The sum of four exponentials. This function outputs parameters
    A1,A2,A3,a1,a3 with
    
    a(z) = A1*exp(a1*z) + A2*exp(-a1*z)
           + A3*exp(1j*a3*z) + conj(A3)*exp(-1j*a3*z)
    
    where a(z) is absorption at depth z, with z=0 being the start of the layer.
    
    fresnel_data is output of fresnel_main()
    """
    pol = fresnel_data['pol']
    v = fresnel_data['vw_list'][layer][0]
    w = fresnel_data['vw_list'][layer][1]
    kz = fresnel_data['kz_list'][layer]
    n = fresnel_data['n_list'][layer]
    n_0 = fresnel_data['n_list'][0]
    th_0 = fresnel_data['th_0']
    th = fresnel_data['th_list'][layer]
    
    a1 = 2*kz.imag
    a3 = 2*kz.real
    
    if pol=='s':
        temp = (n*cos(th)*kz).imag / (n_0*cos(th_0)).real
        A1 = temp * abs(w)**2
        A2 = temp * abs(v)**2
        A3 = temp * v * conj(w)
    elif pol=='p':
        temp = (2*(kz.imag)*(n*cos(conj(th))).real /
                  (n_0*conj(cos(th_0))).real)
        A1 = temp * abs(w)**2
        A2 = temp * abs(v)**2
        temp = (-2*(kz.real)*(n*cos(conj(th))).imag /
                 (n_0*conj(cos(th_0))).real)
        A3 = temp * v * conj(w)
        
    return {'A1':A1, 'A2':A2, 'A3':A3, 'a1':a1, 'a3':a3}

def run_absorp_analytic_fn(z,absorp_func_data):
    """
    Mainly for testing that find_absorp_analytic_fn is coded correctly.
    Calculates absorption at a given depth z, where z=0 is the start of the
    layer.
    
    absorp_func_data is output of find_absorp_analytic_fn()
    """
    A1 = absorp_func_data['A1']
    A2 = absorp_func_data['A2']
    A3 = absorp_func_data['A3']
    a1 = absorp_func_data['a1']
    a3 = absorp_func_data['a3']
    
    return (A1*exp(a1*z) + A2*exp(-a1*z)
             + A3*exp(1j*a3*z) + conj(A3)*exp(-1j*a3*z))

def flip_absorp_analytic_fn(d,absorp_func_data):
    """
    For a layer of thickness d, find new parameters A1,A2,A3,a1,a3, with
    
    a(d-z) = A1*exp(a1*z) + A2*exp(-a1*z)
           + A3*exp(1j*a3*z) + conj(A3)*exp(-1j*a3*z)
           
    i.e., flipping the absorption profile front-to-back.
    
    absorp_func_data is output of find_absorp_analytic_fn()
    """
    a1 = absorp_func_data['a1']
    a3 = absorp_func_data['a3']
    A1 = absorp_func_data['A2']*exp(-a1*d)
    A2 = absorp_func_data['A1']*exp(a1*d)
    A3 = conj(absorp_func_data['A3']*exp(1j*a3*d))
    return {'A1':A1, 'A2':A2, 'A3':A3, 'a1':a1, 'a3':a3}


def absorp_in_each_layer(fresnel_data):
    """
    An array listing what proportion of light is absorbed in each layer.
    
    Assumes the final layer eventually absorbs all transmitted light.
    
    Assumes the initial layer eventually absorbs all reflected light.
    
    Entries of array should sum to 1.
    
    fresnel_data is output of fresnel_main()
    """
    d_list = fresnel_data['d_list']
    num_layers = len(d_list)
    final_answer = zeros(num_layers)
    final_answer[0] = fresnel_data['R']
    final_answer[-1] = fresnel_data['T']
    for i in xrange(1,num_layers-1):
        final_answer[i] = (position_resolved(i,0,fresnel_data)['poyn'] -
                            position_resolved(i+1,0,fresnel_data)['poyn'])
    return final_answer

def incoherent_main(pol,n_list,d_list,th_0,lam_vac):
    """
    d_list is the thickness of each layer, but if a layer has incoherent
    propagation, put its thickness as "inf" (infinity). Losses in incoherent
    layers are ignored. See manual for more details.
    
    See fresnel_main for other definitions.
    
    The program runs fresnel_main for each group of one or more coherent
    (finite) layers surrounded by incoherent layers on both sides. fresnel_data
    returns the output of fresnel_main for each of these groups from front to
    back. fresnel_bdata[i] is the output of fresnel_main for the same stack as
    fresnel_data[i], but with the layers of the stack in reverse order.
    

    """
    #convert lists to numpy arrays if they're not already. Make sure there are
    #no integers. (Adding 0.0 kills integer type for both real and complex.)
    n_list=array(n_list)+0.0
    d_list=array(d_list,dtype=float)
    
    #input tests
    if (n_list.ndim != 1) | (d_list.ndim != 1) | (n_list.size) != (d_list.size):
        raise ValueError("Problem with n_list or d_list!")
    if (d_list[0] != inf) | (d_list[-1] != inf):
        raise ValueError('d_list must start and end with inf!')
    if (np.real_if_close(n_list[0]*np.sin(th_0))).imag != 0:
        raise ValueError('Error in n0 or th0!')
    num_layers = n_list.size
    
    #th_list is a list with, for each layer, the angle that the light travels
    #through the layer. Computed with Snell's law. Note that the "angles" may be
    #complex!
    th_list = list_snell(n_list,th_0)

    #R_list[i] is the reflection intensity of the interface (or stack) after
    #the i'th incoherent layer. RB_list[i] is the same stack but reflection of
    #light traveling backwards from the (i+1)th layer. T and TB are likewise
    #transmission.
    R_list = []
    RB_list =[]
    T_list = []
    TB_list = []
    #fresnel_data_list[i] is the output of fresnel_main for the i'th stack
    #("stack" means group of 1 or more adjacent coherent layers.)
    fresnel_data_list = []
    #fresnel_bdata_list[i] is the same stack as fresnel_data_list[i] but
    #with order of layers reversed
    fresnel_bdata_list = []
    #stack_place[i]=j means that the i'th stack comes after the j'th
    #incoherent layer.
    stack_place=[]

    stack_in_progress = False

    for i in xrange(1,num_layers):
        if d_list[i] != inf:
            if stack_in_progress is False:
                stack_start_index = i-1
                stack_in_progress = True
                stack_place.append(len(R_list))
            continue
        else: # This is when d_list[i] = inf
            if stack_in_progress is False:
                R_list.append(interface_R(pol,n_list[i-1],
                                n_list[i],th_list[i-1],th_list[i]))
                RB_list.append(interface_R(pol,n_list[i],
                                n_list[i-1],th_list[i],th_list[i-1]))
                T_list.append(1 - R_list[-1])
                TB_list.append(1 - RB_list[-1])
            else:
                stack_n_list = n_list[stack_start_index:i+1]
                stack_d_list = d_list[stack_start_index:i+1]
                stack_th0 = th_list[stack_start_index]
                fresnel_data = fresnel_main(pol,stack_n_list,
                                               stack_d_list,stack_th0,lam_vac)
                fresnel_data_list.append(fresnel_data)
                R_list.append(fresnel_data['R'])
                T_list.append(fresnel_data['T'])
                fresnel_bdata = fresnel_reverse(pol,stack_n_list,
                                               stack_d_list,stack_th0,lam_vac)
                fresnel_bdata_list.append(fresnel_bdata)
                RB_list.append(fresnel_bdata['R'])
                TB_list.append(fresnel_bdata['T'])
            stack_in_progress = False
    #L is the transfer matrix from the i'th to (i+1)st incoherent layer, see
    #manual
    L_list = []
    Ltilde = array([[1,0],[0,1]])
    for i in xrange(len(R_list)):
        L = (array([[1,-RB_list[i]],
                    [R_list[i],TB_list[i]*T_list[i] - RB_list[i]*R_list[i]]])
                    / T_list[i])
        L_list.append(L)
        Ltilde = np.dot(Ltilde,L)
    T = 1 / Ltilde[0,0]
    R = Ltilde[1,0] / Ltilde[0,0]
    #VW_list[n] = [V_n, W_n], the forward- and backward-moving intensities
    #in the n'th incoherent layer.
    VW_list=zeros((len(R_list)+1,2))
    VW = array([[T],[0]])
    VW_list[-1,:] = np.transpose(VW)
    for i in xrange(len(R_list)-1,-1,-1):
        VW = np.dot(L_list[i], VW)
        VW_list[i,:] = np.transpose(VW)
    #stackFB_list[n]=[F,B] means that F is light traveling forward towards n'th
    #stack and B is light traveling backwards towards n'th stack.
    stackFB_list=[]
    for i, stack_place_i in enumerate(stack_place):
        stackFB_list.append([VW_list[stack_place_i][0],
                             VW_list[stack_place_i+1][1]])
    return {'T':T, 'R':R, 'VW_list':VW_list,
            'fresnel_data_list':fresnel_data_list,
            'fresnel_bdata_list':fresnel_bdata_list,
            'stack_place':stack_place,
            'stackFB_list':stackFB_list}
    
def inc_absorp_in_each_layer(inc_data):
    """
    A list saying what proportion of light is absorbed in each layer.
    
    [reflected, [stack0layer1,stack0layer2,...],
     [stack1layer1,stack1layer2,...], ... , transmitted]
    
    (where "stack" refers to a coherent multilayer stack)
        
    Entries should sum to 1.
    
    Leaves out incoherent layers in the middle because I assume those don't
    absorb.
    
    inc_data is output of incoherent_main()
    """
    stackFB_list = inc_data['stackFB_list']
    final_list = []
    #first entry is the reflected light
    final_list.append(inc_data['R'])
    for i,fresnel_data in enumerate(inc_data['fresnel_data_list']):
        fresnel_bdata = inc_data['fresnel_bdata_list'][i]
        stack_absorp = ((stackFB_list[i][0] *
                        absorp_in_each_layer(fresnel_data))[1:-1]
                       + (stackFB_list[i][1] *
                        absorp_in_each_layer(fresnel_bdata))[-2:0:-1])
        final_list.append(stack_absorp)
    #last entry is the transmitted light
    final_list.append(inc_data['T'])
    return final_list
