#fitdos.py
#Routines for fitting an analytical DOS model to a Voc vs temperature
#data series

import numpy as np

def build_dos_objective(series, predfun):
	"""
	Given an experimental data series of Voc vs temperature and a dos model
	for predicting the temperature series, return an objective function describing
	the error between the prediction and the data and the gradient of the objective 
	function with respect to all of the model parameters.
	"""

	def build_args(params):
		shape = list([series.shape[0], params.shape[0]])
		arr = np.zeros(shape)
		arr[:,:] = params
		return arr

	def objective_function(*args):
		arg = build_args(args[0])
		pred = predfun(arg, series[:,0], *args[1:])
		errs = series[:,1] - pred
		l2norm = np.sum(errs*errs)

		return l2norm

	def objective_grad(*args):
		arg = build_args(args[0])
		pred = predfun(arg, series[:,0], *args[1:])

		arg[:,-1] = pred
		grad = predfun.grad(arg, series[:,0], *args[1:])
		total = (2.0*(pred - series[:,1])).reshape(grad.shape[0],1)*grad
		return np.sum(total, axis=0)
	
	return objective_function, objective_grad

