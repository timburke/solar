import numpy as np 

def fermi_dirac(x, mu, T):
	"""
	Return the fermi dirac function evaluated at x 
	given that the fermi level is mu and the temperature is T.

	T should be in Kelvin and x and mu in eV.
	"""

	k = 8.6173324e-5
	return 1.0 / (np.exp((x-mu)/(k*T)) + 1.0)

def fermi_dirac_grad_mu(x, mu, T):
	k = 8.6173324e-5
	kT = k*T
	f = np.exp((x-mu)/(kT))

	return f/((1+f)*(1+f)*kT)

def boltz(x, mu, T):
	k = 8.6173324e-5

	return np.exp((mu-x)/(k*T))

def sloped_fermi(x, Ef, Vbi, Voc, T):
	"""
	Given a solar cell with linearly sloped bands and the fermi level
	position at one of the contacts, return the average occupation
	probability at a given energy x.  Vbi must be in electron volts.
	Ef and x must be measured relative to the same reference point,
	e.g. the center of one of the energy levels.
	"""

	V = Vbi - Voc
	V2 = V/2.0
	E0 = Ef - V2
	kT = 8.6173324e-5*T

	pre = kT/V

	farg = ((x - E0) - V2)/(2.0*kT)
	sarg = ((x - E0) + V2)/(2.0*kT)

	fterm = pre*np.log(np.cosh(farg))
	sterm = pre*np.log(np.cosh(sarg))

	return 0.5 + fterm - sterm

def sloped_fermi_grad_v(x, Ef, Vbi, Voc, T):
	V = Vbi - Voc
	kT2 = 2*8.6173324e-5*T


	first = kT2*np.log(np.cosh((x-Ef)/kT2)/np.cosh((x-Ef+V)/kT2))
	second = V*np.tanh((x-Ef+V)/kT2)

	return (first + second)/(2*V*V)

def sloped_fermi_grad_ef(x, Ef, Vbi, Voc, T):
	V = Vbi - Voc
	kT2 = 2*8.6173324e-5*T

	return (np.tanh((x-Ef)/kT2)+np.tanh((Ef-x-V)/kT2))/(-2.0*V)

def sloped_fermi_grad(x, Ef, Vbi, Voc, T):
	shape = [i for i in x.shape]
	shape.append(3)

	grad_v = sloped_fermi_grad_v(x, Ef, Vbi, Voc, T)
	grad_ef = sloped_fermi_grad_ef(x, Ef, Vbi, Voc, T)

	res = np.zeros(shape)
	res[...,0] = grad_ef
	res[...,1] = -1.0*grad_v
	res[...,2] = grad_v

	return res

sloped_fermi.grad = sloped_fermi_grad

def trunc_boltz(x, mu, T):
	if x < mu:
		return 1.0

	return boltz(x, mu, T)

def constant(x):
	return 1.0
