#voc.py
#Given a model for the DOS of an organic solar cell, calculate what Voc should
#be for a given carrier density

import fermi
from scipy.optimize import brentq, curve_fit, bisect
from solar.math import tensor_eval
import numpy as np
from dos import GaussianDOS
from solar.math.tensor import enforce_1d, prepend_array
from scipy.weave.converters import blitz
from scipy.weave import inline

def voc_temperature(params, T, dos_builder):
	"""
	For an organic solar cell with linearly sloped energy levels, calculate the temperature dependent
	open circuit voltage given a model for its density of states and parameters
	characterizing the fermi level alignment at the contacts and a specified carrier density in
	the solar cell.

	Parameter order:
	T, sigma, Ef, Vbi, n
	"""

	T = enforce_1d(T)
	dos, align_params = _split_params(params, dos_builder)
	dos_vals = dos.safe_eval(points=1000)

	#Calculate all of the occupation probabilities
	def compute_n(Voc0, Ef0, Vbi0, T0, n0):
		aparams = np.array([Ef0, Vbi0, Voc0])
		ncalc = compiled_n_1T(dos_vals, T0, aparams)
		return ncalc - n0

	V = np.zeros_like(T)
	pset = build_T_params(align_params, T)

	for i in xrange(0, T.shape[0]):
		Vmax = pset[i,2] + 0.5
		Vmin = -1.0
		nmin = compute_n(Vmin, pset[i,1], pset[i,2], pset[i,0], pset[i,3])+pset[i,3]
		nmax = compute_n(Vmax, pset[i,1], pset[i,2], pset[i,0], pset[i,3])+pset[i,3]
		
		#Extrapolate for low V with a simple exponential so that the search routine
		#doesn't die if we go too far in one direction.
		if pset[i,3] < nmin:
			V[i] = 0.1*np.log(pset[i,3]/nmin) + Vmin
		else:
			try:
				V[i] = brentq(compute_n, Vmin, Vmax, args=(pset[i,1], pset[i,2], pset[i,0], pset[i,3]), xtol=1e-5)
			except ValueError as err:
				print "voc out of range."
				print "Ef", pset[i,1]
				print "Vbi", pset[i,2]
				print "T", pset[i,0]
				print "n", pset[i,3]

				print "n @ voc min", compute_n(Vmin, pset[i,1], pset[i,2], pset[i,0], pset[i,3])
				print "n @ voc max", compute_n(Vmax, pset[i,1], pset[i,2], pset[i,0], pset[i,3])

				raise err

	return V

def voc_temperature_grad(params, T, dos_builder):
	"""
	Compute the gradient of Voc vs all of the used model parameters.
	NB: you must pass in Voc vs all of the model parameters from a 
	previous call to voc_temperature.
	"""

	T = enforce_1d(T)

	ngrad = n_temperature_grad(params, T, dos_builder)
	dvdn = 1.0/ngrad[..., -1]

	grad = np.zeros_like(ngrad)
	grad = -1.0 * ngrad * dvdn.reshape([dvdn.shape[0], 1])
	grad[..., -1] = dvdn

	return grad.reshape([T.shape[0], dos_builder.num_params+3])

voc_temperature.grad = voc_temperature_grad

def _split_params(params, dos_builder):
	#Build the DOS
	num_model = dos_builder.num_params

	if len(params.shape) == 2:
		model_params = params[0, :num_model]
		align_params = params[:, num_model:]
	else:
		model_params = params[:num_model]
		align_params = params[num_model:]

	if hasattr(dos_builder, 'Build'):
		dos = dos_builder.Build(model_params)
	else:
		dos = dos_builder(model_params)

	return dos, align_params

def build_T_params(params, T):
	"""
	Given a set of parameters (either a 1D list that is the same for all T
	or a 2D array (len(T), N) with different parameters for each T, concatenate
	the parameters with the tempera ture to produce an array (len(T), N+1) with
	the temperature prepended.
	"""

	if len(params.shape) == 1:
		res = np.zeros([T.shape[0], params.shape[0]+1])
	elif len(params.shape) == 2:
		res = np.zeros([T.shape[0], params.shape[1]+1])
	else:
		raise ValueError("You must pass in a parameter array that is either 1D or 2D with the first dimension equal to len(T)")
	
	res[:,0] = T
	res[:,1:] = params

	return res

def compiled_n_1T(dosvals, temp, args):
	"""
	Compute the total number of carriers in a device given the sloped_fermi occupation function, a single
	temperature and an evaluated density of states.
	"""

	assert(type(temp) == np.float64)
	assert(len(args.shape) == 1)
	assert(len(dosvals.shape) == 2)
	assert(dosvals.shape[1] == 2)
	assert(args.shape[0] == 3)

	M = dosvals.shape[0]
	dist = dosvals[-1,0] - dosvals[0,0]

	out = np.zeros([1])

	code = \
	r"""
	#line 66
	double Ef = args(0);
	double Vbi = args(1);
	double Voc = args(2);

	double V = Vbi - Voc;
	double V2 = V*0.5;
	double E0 = Ef - V2;
	double scale = (double)dist/(2.0*(M-1));

	double kT  = 8.6173324e-5*(double)temp;
	double pre = kT/V;

	//Runing counters for computing f(x,*args)*g(x)
	double last = 0.0; //double count all values in the interior of the list without branching to do trapz rule
	double tot = 0.0;

	for (int i=0; i<M; ++i)
	{
		double x 	= dosvals(i,0);
		
		double farg = ((x - E0) - V2)/(2.0*kT);
		double sarg = ((x - E0) + V2)/(2.0*kT);

		double occ = 0.5 + pre*log(cosh(farg)/cosh(sarg));
		double dn = occ*dosvals(i,1);

		tot += last + dn;
		last = dn;
	}

	out(0) = tot*scale;
	"""

	inline(code, ['dist', 'args', 'temp',  'dosvals', 'out', 'M'], type_converters=blitz, compiler='gcc')
	return out[0]

def compiled_n(dosvals, temp, args):
	"""
	Compute the total number of carriers in a device given the sloped_fermi occupation function, a 1D array of temperatures
	and an evaluated density of states.
	"""

	assert(len(temp.shape) == 1)
	assert(len(args.shape) == 1)
	assert(len(dosvals.shape) == 2)
	assert(dosvals.shape[1] == 2)
	assert(args.shape[0] == 3)

	M = dosvals.shape[0]
	N = temp.shape[0]
	dist = dosvals[-1,0] - dosvals[0,0]

	out = np.zeros([N])

	code = \
	r"""
	#line 66
	double Ef = args(0);
	double Vbi = args(1);
	double Voc = args(2);

	double V = Vbi - Voc;
	double V2 = V*0.5;
	double E0 = Ef - V2;
	double scale = (double)dist/(2.0*(M-1));

	for (int j=0;j<N;++j)
	{
		double kT  = 8.6173324e-5*temp(j);
		double pre = kT/V;

		//Runing counters for computing f(x,*args)*g(x)
		double last = 0.0; //double count all values in the interior of the list without branching to do trapz rule
		double tot = 0.0;

		for (int i=0;i<M;++i)
		{
			double x 	= dosvals(i,0);
			
			double farg = ((x - E0) - V2)/(2.0*kT);
			double sarg = ((x - E0) + V2)/(2.0*kT);

			double occ = 0.5 + pre*log(cosh(farg)/cosh(sarg));
			double dn = occ*dosvals(i,1);

			tot += last + dn;
			last = dn;
		}

		out(j) = tot*scale;
	}
	"""

	inline(code, ['dist', 'args', 'temp',  'dosvals', 'out', 'M', 'N'], type_converters=blitz, compiler='gcc')
	return out

def n_temperature(params, T, dos_builder):
	"""
	Given a DOS model, fermi level alignment at the contacts and a built-in potential
	calculate the carrier density as a function of temperature given the voltage as a
	function of temperature.
	"""

	T = enforce_1d(T)
	dos, align_params = _split_params(params, dos_builder)
	dos_vals = dos.safe_eval(points=1000)

	return compiled_n(dos_vals, T, align_params)

def n_temperature_grad(params, T, dos_builder):
	"""
	Compute the derivative of the carrier density with respect to all of the DOS
	model parameters, Ef, Vbi and Voc for all of the parameter values specified.
	"""

	T = enforce_1d(T)
	dos, align_params = _split_params(params, dos_builder)
	dos_vals = dos.safe_eval(points=1000)
	dos_grad = dos.gradient(dos_vals[:,0])
	dos_grad = dos_grad.reshape([dos_grad.shape[0], dos.num_params])

	align_params = build_T_params(align_params, T)
	args = prepend_array(dos_vals[:,0], align_params)
	res = fermi.sloped_fermi(args[...,0], args[...,2], args[...,3], args[...,4], args[...,1])
	occ_prob = res.reshape([dos_vals.shape[0], T.shape[0]])

	res = fermi.sloped_fermi_grad(args[...,0], args[...,2], args[...,3], args[...,4], args[...,1])
	occ_grad = res.reshape([dos_vals.shape[0], T.shape[0], 3])

	shape = [1 for x in occ_prob.shape]
	occ_prob = occ_prob.reshape([dos_vals.shape[0], T.shape[0], 1])

	d = dos_grad.reshape([dos_vals.shape[0], 1, dos_grad.shape[-1]])
	occ = occ_prob*d

	model_grad = np.trapz(occ, x=dos_vals[:,0], axis=0)

	shape = [1 for x in occ_grad.shape]
	d = np.swapaxes(np.tile(dos_vals[:,1], shape), -1, 0)
	occ = occ_grad*d

	align_grad = np.trapz(occ, x=dos_vals[:,0], axis=0)

	shape = [T.shape[0], dos_builder.num_params+3]

	res = np.zeros(shape)
	res[...,0:-3] = model_grad.reshape([T.shape[0], dos_builder.num_params])
	res[...,-3:] = align_grad

	return res

n_temperature.grad = n_temperature_grad