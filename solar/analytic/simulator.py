#Voc Simulator class

import numpy as np
from scipy.optimize import basinhopping, minimize
from scipy.linalg import norm
from voc import *
from fitdos import build_dos_objective
import sys

class VocSimulator:
	"""
	This object encapsulates routines for simulating the open-circuit
	voltage of an OPV device given a model of its density of states.
	It provides routines for computing model parameters that optimally
	describe experimental data.
	"""

	def __init__(self, dos_model):
		"""
		Create a VocSimulator given a callable that generates a DOS object
		based on passed in parameters.
		"""

		self.model = dos_model
		self.params = {}

	def load_temperature_data(self, *args):
		"""
		Given a series of Voc vs temperature curves, each of which
		should correspond to the same density of states but under
		different light intensities, prepare the simulator for parameter
		estimation.
		"""

		for i in xrange(0, len(args)):
			if args[i].shape[1] != 2:
				raise ValueError("You must pass in a series of Nx2 numpy arrays with T, Voc values.")

		self.data = args

	def set_param(self, name, position, bounds, guess, scale='linear'):
		"""
		Add a parameter with the given name and bounds at the specified position in
		the parameter array.
		"""

		param = {}
		param["name"] = name
		param["position"] = position

		if scale.lower().startswith("log"):
			param["guess"] = np.log(guess)
			param["bounds"] = [np.log(x) for x in bounds]
			param['log']= True
		else:
			param["guess"] = guess
			param["bounds"] = [x for x in bounds]
			param['log'] = False

		self.params[name] = param

	def _prepare_params(self):
		ordered = sorted(self.params.values(), key=lambda x: x['position'])

		for i in xrange(0, len(ordered)):
			v = ordered[i]
			dist = v['bounds'][1] - v['bounds'][0]

			if v['position'] != i:
				raise ValueError("Parameter %d does not exist.  All parameters must be defined." % i)

			if dist <= 0.0:
				raise ValueError("Upper bound is not greater than lower bound for parameter %s: %s" % (v['name'], str(v['bounds'])))

			v['scale'] = 1.0/dist
			v['offset'] = v['bounds'][0]
			v['scaled_guess'] = (v['guess'] - v['offset'])/dist

		self.param_list = ordered

	def convert_params(self, params):
		"""
		Given a set of parameters in scaled units, convert them to absolute units for use
		in calculations.
		"""

		p = params.copy()

		for i in xrange(0, len(p)):
			pdesc = self.param_list[i]
			p[i] = p[i]/pdesc['scale'] + pdesc['offset']
			if pdesc['log']:
				p[i] = np.exp(p[i])

		return p

	def num_params(self):
		return len(self.params)

	def _callback(self, x, f, accept):
		self.run_log.append((self.convert_params(x), f, accept))

		if self.verbose:
			sys.stdout.write("Iteration %d: %f %s\n" % (len(self.run_log), f, str(self.convert_params(x))))
			sys.stdout.flush()

		#Do not stop the search
		return False

	def _split_params(self, params):
		"""
		Given a set of parameters that specify different carrier densities for different
		temperature series, return a list of parameter sets for each series.
		"""
		numsets = len(self.data)

		if numsets == 1:
			return [params]

		psets = [np.copy(params[:-(numsets-1)]) for i in xrange(0, numsets)]

		for i in xrange(0, numsets):
			psets[i][-1] = params[-(numsets-i)]

		return psets

	def _convert_gradient(self, param, grad):
		for i in xrange(0, len(grad)):
			pdesc = self.param_list[i]
			if pdesc['log'] is True:
				grad[i] = param[i]*grad[i]
			
			grad[i] /= pdesc['scale']

	def _build_compute_object(self):
		"""
		Given the parameters defined in this VocSimulator, create an objective function suitable
		for global minimization that will find the optimal sets of parameters given the Voc vs.
		temperature data series are passed in to this simulator.
		"""

		objs = []
		grads = []

		for i in xrange(0, len(self.data)):
			obj,grad = build_dos_objective(self.data[i], voc_temperature)
			objs.append(obj)
			grads.append(grad)

		def compute_value(*args):
			params = self.convert_params(args[0])
			psets = self._split_params(params)
			vals = [objs[i](psets[i], *args[1:]) for i in xrange(0, len(objs))]
			
			return sum(vals)

		def compute_gradient(*args):
			params = self.convert_params(args[0])
			psets = self._split_params(params)

			comb_grad = np.zeros_like(params)
			gradvals = [grads[i](psets[i], *args[1:]) for i in xrange(0, len(grads))]

			#Compute the gradient in scaled coordinates
			numsets = len(self.data)
			if numsets == 1:
				self._convert_gradient(psets[0], gradvals[0])
				return gradvals[0]

			for i in xrange(0, len(grads)):
				comb_grad[:-(numsets-1)] += gradvals[i]

			#Correct the last values
			for i in xrange(0, len(grads)):
				comb_grad[-(numsets-i)] = gradvals[i][-1]

			self._convert_gradient(params, comb_grad)

			return comb_grad

		return compute_value, compute_gradient

	def _build_minimizer_args(self, maxiter=200):
		minargs = {}
		minargs['method'] = 'TNC'
		minargs['bounds'] = [(0,1) for x in self.param_list]
		minargs['options'] = {'maxiter': maxiter}

		return minargs

	def search_local(self, maxiter=200, verbose=True):
		self.verbose = verbose
		self._prepare_params()

		obj,grad = self._build_compute_object()

		guess = np.array([x['scaled_guess'] for x in self.param_list])

		minargs = self._build_minimizer_args(maxiters)
		minargs['args'] = (self.model,)

		#Minimize, returning 
		res = minimize(obj, guess, jac=grad, **minargs)
		params = self.convert_params(res.x)
		return params, res

	def search_global(self, verbose=True, T=0.1, stepsize=0.05, clear=True, maxiter=1):
		self.verbose = verbose
		self._prepare_params()

		if clear:
			self.run_log = []		

		c,g = self._build_compute_object()
		guess = np.array([x['scaled_guess'] for x in self.param_list])

		minargs = self._build_minimizer_args()
		minargs['args'] = (self.model,)
		minargs['jac'] = g

		res = basinhopping(c, guess, stepsize=stepsize, niter=maxiter, callback=self._callback, minimizer_kwargs=minargs, T=T)

		return res