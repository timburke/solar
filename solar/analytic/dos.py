#dos.py

import numpy as np
import numbers
from solar import utilities
from scipy.stats import linregress
from scipy.special import erfc

class DOS:
	def occupancy(self, start, end, occ_func, *occ_args, **kw):
		"""
		Given a function that takes as its first parameter an energy
		and returns the probability that a state at that energy is filled,
		determine the total number of states occupied given this DOS.
		
		additional arguments passed to this function are passed on to the
		occupancy function occ_func.
		"""

		points = 1000
		if 'points' in kw:
			points = int(kw['points'])

			if points % 2 != 0:
				raise ValueError("You must specify an even number of points.")

		dos = np.zeros([points, 2])

		r = self.range[1] - self.range[0]
		center = (self.range[0] + self.range[1])/2.0

		dos[:,0] = np.linspace(center - r, center+r, points)
		cumfun = self.cumfun()
		dos[:,1] = cumfun(dos[:,0])

		#resample to avoid numerical issues with delta functions
		dos[1:,1] = np.diff(dos[:,1])
		dos[0, 1] = 0.0

		if 'prefactor' in kw:
			dos[:,1] *= kw['prefactor'](dos[:,0])

		occ = np.zeros([points, 2])
		occ[:,0] = np.linspace(-r, r, points)

		for i in xrange(0, points):
			occ[i,1] = occ_func(occ[i,0], *occ_args)

		#Get the result, interpolate with extending with the end values and
		#resample to the desired range
		corr = utilities.correlate(dos, occ)[points/4:3*points/4,:]
		interpolator = utilities.interpolate_array(corr, extend=True)

		res = np.zeros([points, 2])
		res[:,0] = np.linspace(start, end, points)
		res[:,1] = interpolator(res[:,0])
		return res

	def safe_eval(self, **kw):
		"""
		Safely evaluate this density of states by using the cumulative distribution
		and numerically differentiating it in order to get the density of states to
		avoid numerical issues with sampling the delta function directly.
		"""

		points = 1000
		if 'points' in kw:
			points = int(kw['points'])

		dos = np.zeros([points, 2])
		dos[:,0] = np.linspace(self.range[0], self.range[1], points)
		
		#If this DOS has numerical instabilities like delta functions, use
		#the cumulative distribution function and differentiate it to get the
		#DOS
		if not hasattr(self, 'safe') or self.safe == False:
			cumfun = self.cumfun()
			dos[:,1] = cumfun(dos[:,0])
			dos[1:,1] = np.diff(dos[:,1])/(self.range[1]-self.range[0])*points
			dos[0, 1] = 0.0
		else:
			dos[:,1] = self.eval(dos[:,0])

		return dos

	def total_n(self, occ_func, *occ_args, **kw):
		"""
		Evaluates occ_func(en, *occ_args, **kw) across the range of this DOS
		and finds the total number of occupied states.  This is different
		from occupancy above since we only do it once.  This will work for 
		occupancy functions where occupancy for all arguments is not a simple 
		cross-correlation between the occupancy function and DOS, like when the
		bands of an organic semiconductor are tilted.
		"""

		points = 1000
		if 'points' in kw:
			points = int(kw['points'])

		dos = np.zeros([points, 2])

		r = self.range[1] - self.range[0]
		center = (self.range[0] + self.range[1])/2.0

		dos[:,0] = np.linspace(center - r, center+r, 1000)
		cumfun = self.cumfun()
		dos[:,1] = cumfun(dos[:,0])

		#resample to avoid numerical issues with delta functions
		dos[1:,1] = np.diff(dos[:,1])
		dos[0, 1] = 0.0

		dos[:, 1]*= occ_func(dos[:,0], *occ_args)
		return np.sum(dos[:, 1])

	def occupancy_fun(self, occ_func, *occ_args, **kw):
		"""
		Return an interpolator object that interpolates self.occupancy, extending it
		to all values.
		"""

		occ = self.occupancy(self.range[0], self.range[1], occ_func, *occ_args, **kw)
		return utilities.interpolate_array(occ, extend=True)

	def linearize(self, min, max, occ_func, *occ_args, **kw):
		"""
		Return the best linear fit to log(occupancy) betweeb min and max.
		"""

		occ = self.occupancy_fun(occ_func, *occ_args, **kw)

		x = np.linspace(min, max, 200)
		y = np.log(occ(x))

		slope, intercept, r_value, p_value, std_err = linregress(x, y)

		neff = np.exp(intercept)
		kTeff = 1.0/slope

		return (neff, kTeff)

	def eval(self, x):
		if isinstance(x, numbers.Number):
			return self.get(x)
		elif hasattr(x, 'shape'):
			res = np.zeros_like(x)

			for i in xrange(0, x.shape[0]):
				res[i] = self.get(x[i])

			return res

		raise ValueError("Invalid type of object passed to DOS.eval")

	def cumulative(self, points = 1000):
		"""
		Return the total number of states filled up to x.  
		"""

		x = np.linspace(self.range[0], self.range[1], points)

		vals = self.get(x)
		return np.cumsum(vals)*(x[1]-x[0])

	def cumfun(self, points=1000):
		"""
		Build an interpolator function that returns the cumulative number 
		of states up to an energy E
		"""

		vals = np.ndarray([points, 2])
		vals[:,0] = np.linspace(self.range[0], self.range[1], points)
		vals[:,1] = self.cumulative(points)

		interpolator = utilities.interpolate_array(vals, extend=True)
		return interpolator

class GaussianDOS (DOS):
	def __init__(self, sigma, center = 0.0, n0=1.0):
		"""
		Create a Gaussian Density of States model centered at center with total
		number of states n0 and standard deviation sigma
		"""

		self.sigma = sigma
		self.center = center
		self.n0 = n0

		self.range = [center - 1.0, center + 1.0]
		self.safe = True

	def cumulative(self, points = 1000):
		"""
		Return the total number of states filled up to x.  
		"""

		x = np.linspace(self.range[0], self.range[1], points)
		vals = 0.5*erfc((self.center-x)/(np.sqrt(2)*self.sigma))*self.n0
		return vals

	def get(self, x):
		"""
		Get the value of the DOS at a given position
		"""

		return self.n0 / (self.sigma * np.sqrt(2*3.1415926535))*np.exp(-1.0*(x-self.center)**2 / (2.0*self.sigma**2))

	def gradient(self, x):
		"""
		Get the gradient of this DOS vith respect to the std. deviation.
		"""

		s2 = self.sigma*self.sigma
		arg = x - self.center
		pre = (arg*arg - s2)/(s2*self.sigma)

		return pre*self.get(x)

	def gradient_center(self, x):
		s2 = self.sigma*self.sigma
		pre = (x - self.center)/s2

		return pre*self.get(x)

	@classmethod
	def Build(cls, params):
		if params.shape[0] != 1:
			raise ValueError("You need to pass 4 parameters to build a DualGaussianDOS, arguments were: %s" % (str(params),))

		return GaussianDOS(params[0])
	
	num_params = 1


class DualGaussianDOS (DOS):
	def __init__(self, sigma1, sigma2, fraction2, offset2, center=0.0, n0=1.0):
		self.dos1 = GaussianDOS(sigma1, center, (1.0-fraction2)*n0)
		self.dos2 = GaussianDOS(sigma2, center+offset2, fraction2*n0)
		self.fraction2 = fraction2
		self.range = [min(self.dos1.range[0], self.dos2.range[0]), max(self.dos1.range[1], self.dos2.range[1])]

	def get(self, x):
		return self.dos1.get(x) + self.dos2.get(x)

	def gradient(self, x):
		"""
		Return the gradient of this dos at x wrt to sigma1, sigma2, fraction2 and offset2
		"""

		res = np.zeros([x.shape[0], 4])
		res[:,0] = self.dos1.gradient(x)
		res[:,1] = self.dos2.gradient(x)
		res[:,2] = self.dos2.get(x) - self.dos1.get(x)
		res[:,3] = self.dos2.gradient_center(x)

		return res

	@classmethod
	def Build(cls, params):
		if params.shape[0] != 4:
			raise ValueError("You need to pass 4 parameters to build a DualGaussianDOS, arguments were: %s" % (str(params),))

		return DualGaussianDOS(params[0], params[1], params[2], params[3])
	
	num_params = 4

class LumpedDOS (DOS):
	def __init__(self, band_edge, n0):
		"""
		Create a lumped DOS where the states are assumed to be a delta function
		at band_edge.
		"""

		self.loc = band_edge
		self.n0 = n0

		self.range = [self.loc - 1.0, self.loc + 1.0]

	def get(self, x):
		"""
		Get the value of the DOS at a given position
		"""

		dist = np.abs(self.loc - x)

		if dist < 0.001:
			return self.n0 / (2.0*0.001)

		return 0.0

	def cumulative(self, points=100):
		"""
		override because of numerical issues dealing with sampling the delta function
		"""

		x = np.linspace(self.range[0], self.range[1], points)

		res = np.zeros_like(x)
		res[x > self.loc] = self.n0

		return res

class CombinedDOS (DOS):
	"""
	A DOS created from the sum of multiple other DOS objects
	"""

	def __init__(self, *doslist):
		"""
		Given a list of DOS objects, create a combined DOS object.
		"""

		self.dos_list = doslist

		minr = min([x.range[0] for x in doslist])
		maxr = max([x.range[1] for x in doslist])

		self.range = [minr, maxr]

	def get(self, x):
		d = 0.0

		for dos in self.dos_list:
			d += dos.get(x)

		return d

	def cumulative(self, points=100):
		"""
		Override in case the constituent DOSes have an overrriden cumulative function
		"""

		x = np.linspace(self.range[0], self.range[1], points)
		res = np.zeros_like(x)

		interps = [a.cumfun(points) for a in self.dos_list]
		for interp in interps:
			res += interp(x)

		return res
