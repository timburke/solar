#characterization.py
#These classes allow one to easier characterize the performance of a solar cell and
#correspond to figures of merit.  The allow you to easily set what you care about
#in a given device and track how that changes as you vary the device architecture

from solar.types import Spectrum
from solar.sim.simplesim import photocurrent
from matplotlib import pyplot as plt
from solar import reference, utilities
import numpy as np

class FigureOfMerit:
	r"""
	An object that calculates some quantity for a given solar cell.  In general
	if can calculate a scalar, vector, or matrix.
	"""
	
	Scalar = 0
	Vector = 1
	SpectralResponse = 2
	
	def __init__(self, name, type):
		self.name = name
		self.type = type
		self.value = None
		self.stamp = -1
		self.depends = []
		self.hidden = False
	
	def hide(self):
		self.hidden = True
		
	def show(self):
		self.hidden = False

	def update(self, device, figures):
		if self.stamp == device.stamp:
			return
			
		if not hasattr(self, "_calculate"):
			raise Exception("Figure of Merit does not have a _calculate method. Aborting.")
		
		deps = {}
		
		#TODO add dependency tracking
		for dep in self.depends:
			if dep not in figures:
				raise Exception("Necessary dependency not found", dep, self)
			
			depFOM = figures[dep]
			depFOM.update(device, figures)
			
			deps[dep] = depFOM.value
		
		self._calculate(device, deps)
		self.stamp = device.stamp
	
	def display(self):
		r"""
		Print this value in a way depending on the type of the figure of merit.
		""" 
		
		raise Exception("FigureOfMerit subclass does not define a display method.")

class ScalarFOM (FigureOfMerit):
	def __init__(self, name, units):
		FigureOfMerit.__init__(self, name, FigureOfMerit.Scalar)
		self.units = units
	def display(self):
		print "%s: %.4f %s" % (self.name, self.value, self.units)

class SpectralResponseFOM (FigureOfMerit):
	def __init__(self, name, yaxis, units):
		FigureOfMerit.__init__(self, name, FigureOfMerit.SpectralResponse)
		self.y_axis = yaxis
		self.units = units
	
	def display(self):
		figure = plt.figure()
		plt.plot(self.value[:,0], self.value[:,1])
		ylab = "%s (%s)" % (self.y_axis, self.units)
		plt.ylabel(ylab)
		plt.xlabel("Wavelength (nm)")
		plt.title(self.name)

#Common Figures of Merit
class ShortCircuitCurrentFOM (ScalarFOM):
	r"""
	Returns the jsc of this device at one sun illumination from the front with
	no additional effects except those included in the device's given EQE.
	"""
	
	def __init__(self):
		ScalarFOM.__init__(self, "Short Circuit Current", "mA/cm^2")
		self.light = Spectrum.AM15G()
	
	def _calculate(self, device, dependencies):
		self.value = photocurrent(self.light, device.front_eqe())*1000

class TransmittedLightFOM (SpectralResponseFOM):
	def __init__(self):
		SpectralResponseFOM.__init__(self, "Transmitted Light Spectrum", "Light transmitted through device", "photons/s/cm^2")
		self.light = Spectrum.AM15G()
		reference.vToFluxInPlace(self.light)
		
	def _calculate(self, device, deps):
		self.value = self.light.copy()
		self.value[:,1] *= device.transmitted_light(self.value[:,0])

class ReflectedLightFOM (SpectralResponseFOM):
	def __init__(self):
		SpectralResponseFOM.__init__(self, "Reflected Light Spectrum", "Light reflected from the device surface", "photons/s/cm^2")
		self.light = Spectrum.AM15G()
		reference.vToFluxInPlace(self.light)
		
	def _calculate(self, device, deps):
		self.value = self.light.copy()
		self.value[:,1] *= device.front_coupling(self.value[:,0])[:,2]
		
class NormalizedReflectedLightFOM (SpectralResponseFOM):
	def __init__(self):
		SpectralResponseFOM.__init__(self, "Reflected Light Spectrum", "Normalized reflection from the device surface", "a.u.")

	def _calculate(self, device, deps):
		self.value = Spectrum.AM15G()
		self.value[:,1] = device.front_coupling(self.value[:,0])[:,2]

class TransmittedLightFractionFOM (ScalarFOM):
	r"""
	Returns the fraction of photons that are transmitted from the AM1.5G Solar Spectrum.
	"""
	
	def __init__(self):
		ScalarFOM.__init__(self, "Transmitted Light Fraction", "")

		self.depends.append("TransmittedLightFOM")

		light = Spectrum.AM15G()
		reference.vToFluxInPlace(light)
		self.total = light.totalpower()
		
	def _calculate(self, device, deps):		
		self.value = deps["TransmittedLightFOM"].totalpower()/self.total