Simulation Module
=====================
.. module:: solar.sim

The Simulation Module is designed to simulate both crystaline and thin-film devices using
PC1D, WxAmps, or empircal EQE data.  It also allows the calculation of light transmission
through device stacks using a thin-film optics package.

.. autosummary::
   :toctree: sim
   
   SimulatedDevice
   SimpleDevice
   OpaqueDevice
   EncasedDevice