Basic Object Types
=====================
.. module:: solar.types

In order to easily simulate a variety of solar cell devices, some basic building
blocks common to many different situations are defined in this module.


.. autosummary::
   :toctree: types
	
   OpticalSolution
   OpticalFilm
   OpticalStack
   OpticalMaterial