.. PyaSol documentation master file, created by
   sphinx-quickstart on Sat Mar 17 19:59:11 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyaSol's documentation!
==================================

PyaSol is a python package meant to help solar cell researchers easily model 
a wide variety of common solar cell materials and designs.  It internally interfaces
with the standard solar cell software packages PC1D and WxAmps to simulate both
crystalline and thin-film solar cells.  

PyaSol is designed to allow users to easily model solar cells and easily interact
with the resulting information (like quantum efficiencies, etc).  It was initially
designed to simulate combined solar cell and up-converter devices, however, it
can also so a number of other things.

PyaSol also has an integrated database for easily storing and processing experimental data.
Handlers are provided for common file types that allow you to easily import data from your
experiments and use the analysis routines available in PyaSol to wring conclusions out of it.

Modules
===========
.. toctree::
   :maxdepth: 2
   
   upconversion
   utilities
   reference
   types
   sim
   data

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

