Upconversion Module
=====================
.. module:: solar.upconversion

The Upconversion Module simulates both idealized and empirical upconverters.


The various upconverter models that are available in PyaSol are listed below.

Upconverter Models
---------------------

.. autosummary::
   :toctree: upconversion
	
   GaussianUpconverter
   SquareUpconverter
   EmpiricalUpconverter


Base Classes
--------------------

.. autosummary::
	:toctree: upconversion
	
	UpconverterBase
	MobileUpconverter