Utility Functions
=====================
.. module:: solar.utilities

PyaSol depends on various mathematical utility functions that are defined in this
module.  


.. autosummary::
   :toctree: utilities
	
   correlate
   definite_integral
   sample_uniformly
   max_index