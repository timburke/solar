Reference Functions
=====================
.. module:: solar.reference

PyaSol depends on various mathematical utility functions that are defined in this
module.  


.. autosummary::
   :toctree: reference
	
   ev_to_nm
   ev_to_joules
   am15_tilt
   gaussian
   unit_gaussian
   