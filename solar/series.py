import os.path
import glob
import numpy as np

def load_series(folder, type, filter=None, sortby=None, split=None, tags=None):
	"""
	Given a series of data files with a common naming scheme in a single folder,
	load all of the files in that folder meeting the filter. Optionally tag all
	of the objects in the series with metadata pulled from the filenames of
	each object.
	"""

	if filter is None:
		filter = "*"

	#Get all of the matching files
	path = os.path.join(folder, filter)
	files = glob.glob(path)

	series = []

	for file in files:
		if hasattr(type, 'Load'):
			obj = type.Load(file)
		else:
			obj = type(file)

		series.append(obj)

		if split is None:
			continue

		#Try to pull information from the filename	
		fname = os.path.splitext(os.path.basename(file))[0]
		if callable(split):
			fields = split(fname)
		elif isinstance(split, basestring):
			fields = fname.split(split)
		else:
			raise ValueError("Unknown parameter passed for split, should be a delimiter string or callable")

		if len(fields) != len(tags):
			print "Ignoring file %s because %d fields were extracted and there are %d tags" % (fname, len(fields), len(tags))
			continue

		tagged = lambda x: x
		obj.tags = tagged

		for field,tag in zip(fields, tags):
			name = tag[0]
			if name is None:
				continue

			conv = tag[1]

			val = conv(field)
			setattr(obj.tags, name, val)

	if sortby is not None:
		if callable(sortby):
			series.sort(sortby)
		elif isinstance(sortby, basestring):
			sortkey = lambda x: getattr(x.tags, sortby)
			series.sort(key=sortkey)
		else:
			raise ValueError("Unknown sortby parameter should be tag name string or callable key extraction function: %s" % str(sortby))

	return series

def extract_series(data, tag, attr, filter_func=None, normalize=False):
	"""
	Given a list of tagged data, produced, e.g. from the load_series function,
	extract a 2xN numpy array with the first column being the specified tag
	value and the second column being the specified attribute value.
	"""

	if filter_func is not None:
		data = filter(filter_func, data)

	out = np.zeros([len(data), 2])

	for i in xrange(0, len(data)):
		obj = data[i]
		out[i,0] = getattr(obj.tags, tag)
		out[i,1] = getattr(obj, attr)

	if normalize:
		out[:,1] /= max(out[:,1])

	return out

def filter_series(data, filter_func=None, sortby=None):
	if filter_func is not None:
		data = filter(filter_func, data)

	if sortby is not None:
		if callable(sortby):
			data.sort(sortby)
		elif isinstance(sortby, basestring):
			sortkey = lambda x: getattr(x.tags, sortby)
			data.sort(key=sortkey)
		else:
			raise ValueError("Unknown sortby parameter should be tag name string or callable key extraction function: %s" % str(sortby))

	return data

	return data.sort(key=sort)