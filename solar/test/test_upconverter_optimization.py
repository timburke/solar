#Unit test for testing upconverter optimization code path by comparing it to a known good reference
#result

import unittest
from solar.upconversion.plots import *
from utils import compare
import numpy as np
import os.path

@unittest.skip("Takes too long to run and does not test anything useful.")
class TestSequenceFunctions(unittest.TestCase):
	def optimalLocation(self, width):
		(a,b) = IdealLocationVsBG(1.0, 2.0, width, points=2000, max=2000.)
		
		arr = np.ndarray((len(a), 2))
		arr[:,0] = a
		arr[:,1] = b
		
		return arr
	
	def test_location_50(self):
		test = self.optimalLocation(50)
		compare.CompareArrayWithReference(self, test, "50_nm_optimal_loc_2000.csv")
	
	def test_location_100(self):
		test = self.optimalLocation(100)		
		compare.CompareArrayWithReference(self, test, "100_nm_optimal_loc_2000.csv")
	
	def test_location_150(self):
		test = self.optimalLocation(100)
		compare.CompareArrayWithReference(self, test, "100_nm_optimal_loc_2000.csv")

if __name__ == '__main__':
    unittest.main()
