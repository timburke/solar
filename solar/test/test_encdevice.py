from solar.sim import SimpleDevice, EncasedDevice
from solar.types import *
from solar.upconversion import GaussianUpconverter
import unittest
from utils import compare, persist
from numpy import pi, linspace, inf
import scipy
import os.path
import numpy as np
from solar.sim import *
from solar import reference

class TestEncasedDevice(unittest.TestCase):
	def setUp(self):
		#Load Optical Materials
		ito_material 	= OpticalMaterial.FromReference("ITO")
		benzene_material= OpticalMaterial(1.501, 0.0)
		si_material 	= OpticalMaterial(3.47, 0)
		
		ito_film 		= OpticalFilm(ito_material, 200)
		si 				= OpticalFilm(si_material, inf)
		benzene 		= OpticalFilm(benzene_material, inf)
		
		#Create a stack of films
		back_stack = OpticalStack()
		back_stack.add_layer(si)
		back_stack.add_layer(ito_film)
		back_stack.add_layer(benzene)
		front_stack = OpticalStack()

		#Create a device using one of the available simulators
		self.dev = SimpleDevice(QuantumEfficiency.IdealQE(1.1), QuantumEfficiency.IdealQE(1.1))
		self.enc = EncasedDevice(self.dev, front_stack, back_stack)
		self.upconverter = GaussianUpconverter(1200, 1.0, 150, 300)
		
		dev = SimpleDevice.FromWxAmpsDevice(os.path.join(os.path.dirname(__file__), "data", "CdTe_Gloeckler.dev"))
		self.combined = CombinedDevice(dev)
		
		np.seterr(all='ignore')

	def test_encased_device(self):
		(resp, total) = self.upconverter.ideal_benefit(device=self.enc)
		
		#persist.persist_array(resp, "enc_dev_ideal_100.csv")
		compare.CompareArrayWithReference(self, resp, "enc_dev_ideal_100.csv")
	
	def test_simple_device(self):
		(respI, totalI) = self.upconverter.ideal_benefit(device=self.dev)
		
		compare.CompareArrayWithReference(self, respI, "simple_dev_ideal_100.csv")
		
	def test_coherent_coupling(self):
		res = self.combined.front_coupling(reference.canonical_wavelengths())
		
		trans = np.ndarray([res.shape[0], 2])
		trans[:,0] = res[:,0]
		trans[:,1] = res[:,3]
		
		refl = trans.copy()
		refl[:,1] = res[:,2]
		
		abs = trans.copy()
		abs[:,1] = res[:,1]
		
		#persist.persist_array(trans,  "cdte-optical-coh-trans.csv")
		#persist.persist_array(refl,  "cdte-optical-coh-refl.csv")
		#persist.persist_array(abs,  "cdte-optical-coh-abs.csv")
		
		compare.CompareArrayWithReference(self, trans,  "cdte-optical-coh-trans.csv")
		compare.CompareArrayWithReference(self, refl, "cdte-optical-coh-refl.csv")
		compare.CompareArrayWithReference(self, abs, "cdte-optical-coh-abs.csv")
		
	def test_incoherent_coupling(self):
		self.combined._optical_stack.make_incoherent(1)
		res = self.combined.front_coupling(reference.canonical_wavelengths())
		
		trans = np.ndarray([res.shape[0], 2])
		trans[:,0] = res[:,0]
		trans[:,1] = res[:,3]
		
		refl = trans.copy()
		refl[:,1] = res[:,2]
		
		abs = trans.copy()
		abs[:,1] = res[:,1]
		
		#persist.persist_array(trans,  "cdte-optical-incoh-trans.csv")
		#persist.persist_array(refl,  "cdte-optical-incoh-refl.csv")
		#persist.persist_array(abs,  "cdte-optical-incoh-abs.csv")
		
		compare.CompareArrayWithReference(self, trans,  "cdte-optical-incoh-trans.csv")
		compare.CompareArrayWithReference(self, refl, "cdte-optical-incoh-refl.csv")
		compare.CompareArrayWithReference(self, abs, "cdte-optical-incoh-abs.csv")
		