from solar.sim import SimpleDevice, EncasedDevice
from solar.types import *
from solar.upconversion import GaussianUpconverter
import unittest
from utils import compare, persist
from numpy import pi, linspace, inf
import scipy
import os.path
import numpy as np
from solar.sim import *
from solar import reference
from solar.characterization import *

class TestFiguresOfMerit(unittest.TestCase):
	def setUp(self):
		dev = SimpleDevice.FromWxAmpsDevice(os.path.join(os.path.dirname(__file__), "data", "CdTe_Gloeckler.dev"))
		self.comb = CombinedDevice(dev)
		
	def test_depinjection(self):
		self.comb.track_figure(TransmittedLightFractionFOM())
		
		self.assertEqual(len(self.comb.figures), 2) #Verify automatic dependency injection
		self.assertIn("TransmittedLightFOM", self.comb.figures) #verify dependency injection