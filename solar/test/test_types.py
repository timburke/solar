#Unit test for testing the basic types in the solar package
#result

import unittest
from upconverter_harness import UpconverterHarness
from solar.upconversion import gaussian, square, empirical
from solar.types import Spectrum, QuantumEfficiency, OpticalMaterial
from solar import reference,utilities
from utils import compare
import numpy as np
import os.path
import scipy
from scipy import fftpack, signal

class TestBasicTypes(unittest.TestCase):
	def setUp(self):
		self.gaussian = gaussian.GaussianUpconverter(1000., 1.0, 100, 400.0)
		self.square = square.SquareUpconverter(1000, 1.0, 100, 400.0)
		self.empirical = empirical.EmpiricalUpconverter.FromReference('PtTPBP', 200)
		self.empirical.absorber.concentration = 10.**-3
		self.empirical.absorber.pathLength = 20 * 10.**-4
		self.spectrum = Spectrum.AM15G()
		self.spectrumU = utilities.sample_uniformly(self.spectrum, 0., 4096., 4096).view(Spectrum)
		self.eqe = QuantumEfficiency.Load(os.path.join(os.path.dirname(__file__), "data", "dssc_world_record_12_2011.csv"))
	
	def test_reference_material(self):
		"""
		Test basic functionality of reference material class.
		"""
		
		mat1 = OpticalMaterial.FromReference("ito")
		mat2 = OpticalMaterial.FromReference("ITO")
		mat3 = OpticalMaterial(1.0, 2.0)
		
		self.assertAlmostEqual(mat3.k(5), 2.0)
		self.assertAlmostEqual(mat3.n(10), 1.0)
		
	def test_optical_stack(self):
		"""
		Test basic functionality in the OpticalStack class
		"""
		
		from solar.types import OpticalStack,OpticalMaterial,OpticalFilm

		ito = OpticalMaterial.FromReference('ITO')
		
		stack = OpticalStack()
		stack.add_layer('air')
		stack.add_layer(OpticalFilm(ito, 100))
		stack.add_layer('air')
		
		waves = np.linspace(400,1200, 200)
		
		trans = stack.transmission(waves)
		refl = stack.reflection(waves)
		
		abs = stack.absorption_in_layers(waves)
		
		diff = 1.0 - (abs[:,1]+abs[:,2]+abs[:,3]) #should sum to one
		
		zero = np.zeros_like(diff)
		
		compare.Compare1DArrays(self, diff, zero)
		
		self.assertAlmostEqual(trans[5], 0.85003256094622137)
		self.assertAlmostEqual(refl[10], 0.037012463736304957)
	
	def test_spectrum_power(self):
		"""
		Compare total power density in the solar spectrum to known good values.
		"""
		
		self.assertAlmostEqual(self.spectrum.totalpower(), 0.10003706555734437)
		self.assertAlmostEqual(self.spectrumU.totalpower(), 0.10003681970820363)
	
	def test_eqe_class(self):
		"""
		Test basic functionality in the QuantumEfficiency class
		"""
		
		resp = self.eqe.response(self.spectrum)
		respU = self.eqe.response(self.spectrumU)
		
		jsc = scipy.trapz(resp[:,1], resp[:,0]) * reference.q
		jscU = scipy.trapz(respU[:,1], respU[:,0]) * reference.q
		
		self.assertAlmostEqual(jsc, 0.017005875177211447)
		self.assertAlmostEqual(jscU, 0.017006707702892795)
		
	def test_ideal_emission(self):
		self.gaussian.emissionHalfwidth = 50
		loc50 = self.gaussian.ideal_shift(eqe=self.eqe)[1]
		
		self.gaussian.emissionHalfwidth = 10
		loc10 = self.gaussian.ideal_shift(eqe=self.eqe)[1]
		
		self.assertAlmostEqual(loc10, 652.15921855921852)
		self.assertAlmostEqual(loc50, 554.1352869352869)
		
		