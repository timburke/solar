#Utility function for persisting data for testing
import os.path
import numpy as np

def persist_array(arr, name):
	pathname = os.path.join(os.path.dirname(__file__), "..", "data", name)
	
	np.savetxt(pathname, arr, delimiter=',')
