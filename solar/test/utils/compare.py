#Utilities for comparing two np arrays for basic equality
import unittest
import numpy as np
from scipy import interpolate
import os.path

#Arrays must be two dimensional
def CompareArrays(case, test, ref):
	case.assertTupleEqual(test.shape, ref.shape, "Two arrays are not the same size")
	
	for i in xrange(0, test.shape[0]):
		for j in xrange(0, test.shape[1]):
			case.assertAlmostEqual(test[i,j], ref[i,j])

def Compare1DArrays(case, test, ref):
	case.assertTupleEqual(test.shape, ref.shape, "Two arrays are not the same size")
	
	for i in xrange(0, test.shape[0]):
		case.assertAlmostEqual(test[i], ref[i])

def CompareArrayWithReference(case, test, refFile):
	pathname = os.path.join(os.path.dirname(__file__), "..", "data", refFile)
	ref = np.genfromtxt(pathname, delimiter=',')
	
	CompareArrays(case, test, ref)

#arr1 must have a domain that is a subset of arr2
def CompareSampledFunctions(case, arr1, arr2):
	func = interpolate.interp1d(arr2[:,0], arr2[:,1], kind="cubic")
	
	for i in xrange(0, arr1.shape[0]):
		x = arr1[i,0]
		y1 = arr1[i,1]
		y2 = func(x)
				
		case.assertAlmostEqual(y1, y2)
