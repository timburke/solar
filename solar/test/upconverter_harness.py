#TestUpconverter.py
#An ideal gaussian upconverter with controllable absorption peak location and size

import math
from solar.upconversion import base
import numpy as np

class UpconverterHarness (base.MobileUpconverter):
	#peak is the location of gaussian peak in nm
	#max is the maximum absorbance at that wavelength
	#halfwidth is the full width at half-maximum
	#shift is the antistokes shift of the upconverter in nm (relative to peak) and positive
	def __init__(self, peak, max, width, shift):
		base.UpconverterBase.__init__(self)
		
		self.peak = peak
		self.max = max
		self.width = width
		self.shift = peak-shift

	#return a constant value to test integration scheme
	#for emission
	def total_absorption(self, light):
		return 2.0
	