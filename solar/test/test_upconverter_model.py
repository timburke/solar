#Unit test for testing upconverter optimization code path by comparing it to a known good reference
#result

import unittest
from upconverter_harness import UpconverterHarness
from solar.upconversion import gaussian, square, empirical
from solar.types import Spectrum
from solar import reference, utilities
from utils import compare
import numpy as np
import os.path
import scipy
from scipy import fftpack, signal

class TestUpconverterModel(unittest.TestCase):
	def setUp(self):
		self.gaussian = gaussian.GaussianUpconverter(1000., 1.0, 100, 400.0)
		self.square = square.SquareUpconverter(1000, 1.0, 100, 400.0)
		self.empirical = empirical.EmpiricalUpconverter.FromReference('PtTPBP', 200)
		self.empirical.absorber.concentration = 10.**-3
		self.empirical.absorber.pathLength = 20 * 10.**-4
		self.spectrum = Spectrum.AM15G()
		self.test = UpconverterHarness(1000, 1.0, 100.0, 400.0)
		
	def checkLinear(self, fun):
		a = fun(1.0)
		b = fun(0.5)
		c = fun(1.0/3.0)
		
		#normalize so we can check sigfigs using python's functions (which checks 7 digits after
		#decimal place)
		c /= a
		b /= a
		a /= a
		
		self.assertAlmostEqual(a/2.0, b)
		self.assertAlmostEqual(a/3.0, c)
		
	def parameterizedAbsorption(self, conv, max):
		oldMax = conv.max
		
		conv.max = max
		
		absorbed = conv.total_absorption(self.spectrum)
		conv.max = oldMax
		
		return absorbed
		
	#Test Cases
	def testFFTIdealBenefitCorrectness(self):
		light = Spectrum.AM15G()
		lightU = utilities.sample_uniformly(light, 0., 8192., 8192)
		reference.vToFluxInPlace(lightU)

		specF = self.empirical.prepare_absorption_spectrum()
		specC = fftpack.ifftshift(specF)
		
		respC = signal.correlate(lightU[:,1], specC, mode="same")
		respF = np.real(fftpack.ifft( fftpack.fft(lightU[:,1]) * np.conj(fftpack.fft(specF))))
		respC *= reference.q / 2.0
		respF *= reference.q / 2.0

		(resp, totalF) = self.empirical.ideal_benefit(10.0)
		
		#They differ at low wavelengths since the ideal_benefit call has a cutoff, but should be in agreement
		#above 500 nm.
		compare.Compare1DArrays(self, respC[500:4096], resp[500:,1])
		compare.Compare1DArrays(self, respF[500:4096], resp[500:,1])
	
	@unittest.skip("This test is not expected to work because of the different sampling points used in the reference and fast code.")
	def testFFTIdealBenefitAgainstReference(self):
		(upGF, totalGF) = self.gaussian.ideal_benefit(1.5)
		(upGX, upGY, totalG) = self.gaussian.idealbenefit_reference(1.5, points=4000, absolute=True)
		upG = np.ndarray([upGX.shape[0], 2])
		upG[:,0] = upGX
		upG[:,1] = upGY

		(upSF, totalSF) = self.square.ideal_benefit(1.5)
		#(upSX, upSY, totalS) = self.square.idealbenefit_reference(1.5, points=4000, absolute=True)
		#upS = np.ndarray([upGX.shape[0], 2])
		#upS[:,0] = upSX
		#upS[:,1] = upSY
	
		(upEF, totalEF) = self.empirical.ideal_benefit(1.5)
		#(upEX, upEY, totalE) = self.empirical.idealbenefit_reference(1.5, points=4000, absolute=True)
		#upE = np.ndarray([upGX.shape[0], 2])
		#upE[:,0] = upEX
		#upE[:,1] = upEY
		
		#self.assertAlmostEqual(totalGF, totalG)
		#self.assertAlmostEqual(totalSF, totalS)
		#self.assertAlmostEqual(totalEF, totalE)
		
		compare.CompareSampledFunctions(self, upG, upGF)
		#compare.CompareSampledFunctions(self, upS, upSF)
		#compare.CompareSampledFunctions(self, upE, upEF)

	#Make sure the unscaled emission function has unit area
	def testEmissionUnity(self):
		emission = self.gaussian.emission_gaussian()
		total = scipy.trapz(emission[:,1], emission[:,0])
		
		#Make sure the output gaussian has unit area
		self.assertAlmostEqual(total, 1.0)
		
	#Make sure the scaled emission function has the correct number of photons
	def testEmissionScaling(self):
		emission = self.test.upconverted_light(self.spectrum)
		emission = reference.vToFlux(emission)
		
		totalFlux = scipy.trapz(emission[:,1], emission[:,0])
		
		#Upconverted light should have a total flux of 1.0 since the test harness has an absorbed
		#flux of 2.0
		self.assertAlmostEqual(totalFlux, 1.0)
		
	#Make sure the absorption functions are linear in its maximum, as it should be
	def testGaussianAbsorptionLinearity(self):
		self.checkLinear(lambda x: self.parameterizedAbsorption(self.gaussian, x))
		
	def testSquareAbsorptionLinearity(self):
		self.checkLinear(lambda x: self.parameterizedAbsorption(self.square, x))

	
if __name__ == '__main__':
    unittest.main()
