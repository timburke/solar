import unittest
from solar.types import Spectrum
from solar import reference,types
from utils import compare
import numpy as np
import scipy

class TestUpconverterModel(unittest.TestCase):
	#make sure that the in place and reference conversion functions
	#behave identically
	def test_inplace_functions(self):
		s1 = types.Spectrum.AM15G()
		#s2 = types.Spectrum.AM15G()
		
		fSlow = reference.vToFlux(s1)
		reference.vToFluxInPlace(s1)
		
		s2 = fSlow.copy()
		
		pSlow = reference.vToPower(s2)
		reference.vToPowerInPlace(s2)
		
		#normalize
		max1 = np.max(fSlow[:,1])
		fSlow[:,1] /= max1
		s1[:,1] /= max1
		
		max2 = np.max(pSlow[:,1])
		pSlow[:,1] /= max2
		s2[:,1] /= max2
		
		compare.CompareArrays(self, s1, fSlow)
		compare.CompareArrays(self, s2, pSlow)