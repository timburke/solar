#caching.py
#Adds module level caching abilities to entire package using joblib

from joblib import Memory
import numpy as np
from solar import reference
from scipy import interpolate

import tempfile

mem = Memory(cachedir=tempfile.mkdtemp())

def canonical_spectral_method(method):
	r"""
	A spectral method that computes a function once for the canonical wavelengths used
	in this library and then interpolates it for future requests
	"""
	
	name = method.__name__
	cached_name = "_canon_" + name
	cached_stamp = cached_name + "_stamp"
	cached_dim = cached_name + "_dim"
	
	def wrapper(self, *args, **kwargs):
		if hasattr(self, cached_stamp) and getattr(self, cached_stamp) == self.stamp:
			out = np.ndarray([args[0].shape[0], getattr(self, cached_dim)])
			out[:,0] = args[0]
			out[:,1:] = getattr(self, cached_name)(args[0])
			
			#print "Returning interpolated values for", name
			
			return out
		
		#print "Computing canonical values for", name
		waves = reference.canonical_wavelengths()
		
		res = method(self, waves, **kwargs)
		func = interpolate.interp1d(waves, res[:,1:],axis=0)
		#print "Done computing"
		
		setattr(self, cached_name, func)
		setattr(self, cached_stamp, self.stamp)
		setattr(self, cached_dim, res.shape[1])
				
		out = np.ndarray([args[0].shape[0], getattr(self, cached_dim)])
		out[:,0] = args[0]
		out[:,1:] = getattr(self, cached_name)(args[0])
		return out
		
	
	return wrapper

def cached_spectral_method(method):
	r"""
	A method decorator that ensures results are only calculated when the go stale
	by looking at the stamp on the object and only recomputing something if its stamp
	is newer than the stamp of the result.  This works only if the function takes in 
	a range of wavelengths and computes a value for each wavelength which is stored
	in an ndarray with the first column being those wavelengths.
	"""
	
	#this only works np arrays and copy them
	
	name = method.__name__
	cached_name = "_cached_" + name
	cached_stamp = cached_name + "_stamp"
	
	def wrapper(self, *args, **kwargs):
		if hasattr(self, cached_stamp) and getattr(self, cached_stamp) == self.stamp:
			if np.allclose(getattr(self, cached_name)[:,0], args[0]):
				#print "Returning cached from: %s" % name	
				return getattr(self, cached_name).copy()
		
		
		res = method(self, *args, **kwargs)
		
		setattr(self, cached_name, res.copy())
		setattr(self, cached_stamp, self.stamp)
		
		#print "Caching %s" % name
		
		return res
		
	return wrapper