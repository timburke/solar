﻿#utilities.py
#Utility functions that are used in the solar module

from scipy import fftpack, interpolate
import numpy as np
import subprocess
from subprocess import call, check_output
import paramiko
from getpass import getpass
import uuid
import math
import shutil
import os.path
import os

def correlate(function, response):
	r"""
	Compute the full discrete cross-correlation between two sampled functions using fourier transforms.
	It is up to the caller to ensure that there is sufficient zero-padding to ensure that wrap-around
	effects do not influence the results.  The functions must be uniformly sampled with the same spacing 
	for this to work.  You can use SampleUniformly to accomlish this.
	
	:param function: The stationary function that is to be correlated with response.  This should be a 2D array whose first column is a list of sampling points and second column is a list of values
	:type function: numpy.ndarray
	:param response: The response function that will be used for the correlation.  This should be centered around the middle of the array.  So, if the length of response is 4096, then response[2048] is response(0).  
	
	:returns: The cross correlation of the two inputs.  This has the same size as the inputs and the first column is identically equal to function[:,0]
	:rtype: numpy.ndarray
	"""
	
	if function.shape[0] != response.shape[0] or function.shape[1] != 2 or response.shape[1] != 2:
		raise Exception("The two arguments to correlate must 2d arrays with the same length.")
	
	firstF = fftpack.fft(function[:,1])
	secondF = fftpack.fft(fftpack.fftshift(response[:,1]))
	
	resp = function.copy()
	resp[:,1] = np.real(fftpack.ifft(firstF*np.conj(secondF)))
	
	return resp
	
def definite_integral(arr, waveMin, waveMax):
	r"""
	Returns the discrete definite integral of the function sampled in arr from waveMin to waveMax.
	The integral is calculated using a `Newton-Cotes <http://en.wikipedia.org/wiki/Newton–Cotes_formulas>`_ method of degree one (Trapezoidal Integration).
	
	waveMin and waveMax are not required to be explicitly listed in arr.  If they are not explicitly listed,
	the function's value at them is linearly interpolated.
	
	.. note::
		If waveMin is less than :math:`\min(arr[:,0])` or waveMax is greater than :math:`\max(arr[:,0])` 
		the results should not trusted.  It is the caller's responsibility to assure that this is okay.
		
	:param waveMin: the lower limit of integration 
	:param waveMax: the upper limit of integration 
	:param arr: the 2D array of (x,y) points representing the function to be integrated.
	"""
	
	length = arr.shape[0]
	
	counting = False
	sum = 0.0
	
	func = interpolate.interp1d(arr[:,0], arr[:,1])
	
	for i in xrange(0,length):
		#if we're already in the range of [waveMin, waveMax]
		if counting is True:
			#if we're below wavemax, add up a whole trapezoid contribution
			if arr[i,0] <= waveMax:
				if i>0:
					sum += (arr[i,1]+arr[i-1,1])/2.0*(arr[i,0]-arr[i-1,0])
			else:
				#this is the first one above wavemax, so add up a trapezoid contribution up to waveMax
				#we know i>0 because we had to set counting true by passing at least one row
				sum += (arr[i-1,1]+func(waveMax))/2.0*(waveMax - arr[i-1,0])
				break
	
		#if this is the first entry above the cutoff wavelength
		if counting is False and arr[i,0] >= waveMin:
			if arr[i,0] > waveMax:
				#The entire limit is contained in this one entry
				#if it's below the first entry, we say 0
				#otherwise it's the value of the spectrum*(wavemax - wavemin)
				if i == 0:
					return 0.0
				else:
					return (func(waveMax)+func(waveMin))/2.0*(waveMax-waveMin)
			
			#otherwise we need to add a partial contribution from waveMin to this wavelength
			sum += arr[i,1]*(arr[i,0]-waveMin)
			
			counting = True
	
	return sum
	
def sample_uniformly(arr, min, max, points):
	r"""
	Resample a discretely sampled function given in arr, such that the spacing between sampled points is
	uniform.  This is important, for example, when taking the discrete fourier transform or convolution, since
	those functions assume a uniform sample spacing.  
	
	The function is sampled points times uniformly between min and max.  If either min or max is out of the 
	domain of the function given in arr, it is padded with zeros.  The samples are computed by linearly
	interpolating arr.
	
	:param min: the first point to sample
	:param max: the last point to sample
	:param points: the total number of samples
	:param arr: a 2D array of (x,y) values representing the function to be resampled
	"""
	
	func = interpolate.interp1d(arr[:,0], arr[:,1], bounds_error=False, fill_value=0.0)
	
	results = np.ndarray([points, 2])
	results[:,0] = np.linspace(min, max, points)
	
	results[:,1] = func(results[:,0])

	return results
	
def max_index(arr):
	r"""
	Given 2D array which is interpreted as a list of (x,y) coordinates, this function returns the
	index of the pair with the greatest y value.  This is useful for finding the maxima of sampled
	functions, for example.
	
	:param arr: a 2D array of points which is interpreted as a list of (x,y) points
	:return: the index of the coordinate pair with the greatest y value.
	"""
	
	max = arr[0,1] - 1.0
	foundI = -1
	for i in xrange(0, arr.shape[0]):
		if arr[i, 1] >= max:
			foundI = i
			max = arr[i,1]
	
	return foundI

def extended_interpolator(x, interpolator, lower, upper):
	if x < lower[0]:
		return lower[1]
	
	if x > upper[0]:
		return upper[1]
	
	return interpolator(x)

def flatten(l):
	out = []
	for item in l:
		if isinstance(item, (list, tuple)):
			out.extend(flatten(item))
		elif isinstance(item, np.ndarray) and np.ndim(item) > 0:
			for elem in item:
				out.append(elem)
		else:
			out.append(item)
			
	return out

def interpolate_array(arr, **kw):
	r"""Interpolate an array into a function using various keywords to control the behavior.
	:param arr: should be a 2d array listing (x,y) samples from the function to be interpolated
	:param kind: what type of interpolation to do: can be any valid scipy.interp1d value.  If kind is not
	defined, linear interpolation is used.
	:param fill: if defined, disable bounds checking and return this value for all parameters
	outside the domain of arr.
	"""
	
	#make sure the array is 2D
	shape = arr.shape
	if len(shape) != 2:
		raise Exception("Array to be interpolated must be an (x,y) list.  Shape was:" + str(shape))
	if shape[1] != 2:
		raise Exception("Array to be interpolated must be an (x,y) list.  There were " + str(shape[1]) + " columns.")
	
	kind = 'linear'
	check = True
	extend = False
	fill = 0.0
	
	if "kind" in kw:
		kind = kw["kind"]
	
	if "fill" in kw:
		fill = kw["fill"]
		check = False

	if "extend" in kw:
		extend = True
	
	interpolator = interpolate.interp1d(arr[:,0], arr[:,1], kind=kind, bounds_error=check, fill_value=fill)
	
	if extend:
		#Extend array by using the first and last values of the array as the values on either side of the domain
		lower = (arr[0,0], arr[0,1])
		upper = (arr[-1,0], arr[-1,1])
		
		return np.vectorize(lambda x: extended_interpolator(x, interpolator, lower, upper))
	else:
		return interpolator

class SSHUtility:
	def connect(self):
		"""
		Connect to corn servers with paramiko prompting for a password since we can't support kerberos.
		Subsequent calls will reconnect to corn.stanford.edu to get a new low-load server
		"""
		
		#close old ssh if we need to
		if hasattr(self, "_ssh"):
			self._ssh.close()
		
		ssh = paramiko.SSHClient()
		ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
		
		#get password from interactive user since pki is not supported and kerberos doesn't work
		passw = getpass('Enter corn password: ')
		ssh.connect('corn.stanford.edu', username='timburke', password=passw, look_for_keys=False)
		
		self._ssh = ssh				
		
		print "Connected to:", self._get_host()
		
	def _get_host(self):
		self._ensure_connected()
		
		val = self._exec_remote("cat /etc/hostname")
		
		val = val.rstrip()
		
		return val.split('.')[0]
	
	def _exec_remote(self, cmd, quiet=True):
		self._ensure_connected()
		
		infile, outfile, errfile = self._ssh.exec_command(cmd)

		output = outfile.read()
		infile.close()
		outfile.close()
		errfile.close()
		
		if not quiet:
			print output
			
		return output
	
	def _put(self, local, remote=None):
		self._ensure_connected()
		
		if remote is None:
			remote = os.path.join("/tmp", uuid.uuid4().hex)
		
		ftp = self._ssh.open_sftp()
		
		ftp.put(local, remote)
		
		ftp.close()
		
		return remote
	
	def _get(self, local, remote):
		self._ensure_connected()
		
		ftp = self._ssh.open_sftp()
		ftp.get(remote, local)
		ftp.close()
		
	def _get_directory(self, local_dir, remote_dir):
		self._ensure_connected()
		
		ftp = self._ssh.open_sftp()
		ftp.chdir(remote_dir)
		files = ftp.listdir()
		
		print "Starting download"
		
		for file in files:
			local_path = os.path.join(local_dir, file)
			ftp.get(file, local_path)
		
		print "Download finished"
	
	def _create_remote(self, contents, remote=None):
		if remote is None:
			remote = os.path.join("/tmp", uuid.uuid4().hex)
			
		self._ensure_connected()
				
		ftp = self._ssh.open_sftp()
		r = ftp.file(remote, 'w')
		r.write(contents)
		r.close()
		ftp.close()
		
		return remote
	
	def _exec_commands(self, cmds, shell="/bin/bash"):
		data = "\n".join(cmds)
		script = self._create_remote(contents=data)
		
		return self._exec_remote(shell + " " + script)
	
	def _get_shell(self):
		self._ensure_connected()
		
		chan = self._ssh.invoke_shell()
		
		stdin = chan.makefile('wb')
		stdout = chan.makefile('rb')
		
		return (stdin, stdout, chan)
	
	def _ensure_connected(self):
		if not self._connected():
			self.connect()
		
	def _connected(self):
		return hasattr(self, '_ssh') and self._ssh is not None