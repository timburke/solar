#!/usr/bin/python

from setuptools import setup, find_packages


setup(name='solar',
      version='1.0',
      description='PyaSol solar research package with upconversion, montecarlo and inorganic device simulation modules.',
      author='Tim Burke',
      author_email='timburke@stanford.edu',
      packages=find_packages(),
      package_data={'solar': ['reference_data/*.csv', 'reference_data/optical/*.csv', 'slice/pc1d/*.ice', 'slice/wxamps/*.ice']}
      )
